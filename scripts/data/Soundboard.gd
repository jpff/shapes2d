extends Node

const c_db_inc_amount = 0.08
const c_db_normal_volume_amount = -3
const c_db_low_volume_amount = -9

var g_set_to_lower_volume = true
var g_db_value = -9			# Start as low volume when the game starts

var g_track_index = 0
var g_track_indexes_randomzied = []
var g_music_enabled = false

func _ready():
	disable_game_music()
	get_node("Track1").set_loop(true)
	get_node("Track1").set_volume_db(c_db_low_volume_amount)
	set_process(true)

func play_click():
	get_node("Click").play("switch27")

func play_found_object():
	get_node("Found Shape").play("pop2")

func play_dot_hover():
	get_node("Dot Hover").play("pop1")

func play_beep():
	get_node("Beep").play("tone1")

func play_levelup():
	get_node("LevelUp").play("tone-beep")



# Game music tracks

func enable_game_music():
	if !g_music_enabled:
		get_node("Track1").play()
		g_music_enabled = true

func disable_game_music():
	if g_music_enabled:
		get_node("Track1").stop()
		g_music_enabled = false

func lower_music_volume(l_state):
	if l_state:
		g_set_to_lower_volume = true
	else:
		g_set_to_lower_volume = false

func _process(delta):
	if g_set_to_lower_volume && g_db_value > c_db_low_volume_amount:
		g_db_value -= c_db_inc_amount
		get_node("Track1").set_volume_db(g_db_value)
	elif !g_set_to_lower_volume && g_db_value < c_db_normal_volume_amount:
		g_db_value += c_db_inc_amount
		get_node("Track1").set_volume_db(g_db_value)
