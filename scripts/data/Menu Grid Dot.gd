extends Node2D

const c_checkmark_start_scale = Vector2(1.2,1.2)
const c_checkmark_final_scale = Vector2(0.76,0.76)
const c_checkmark_scale_increment = ( c_checkmark_start_scale / c_checkmark_final_scale ) * 0.008

var checkmark_timer = null

func _ready():
	set_process(false)
	get_node("Check Mark").set_scale(c_checkmark_start_scale)
	get_node("Check Mark").set_hidden(true)

func _init_checkmark_timer():
	checkmark_timer = Timer.new()
	checkmark_timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	checkmark_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	checkmark_timer.set_wait_time(1.5)
	checkmark_timer.connect("timeout", self, "on_checkmark_timeout")
	add_child(checkmark_timer)

func on_checkmark_timeout():
	get_node("Check Mark").set_hidden(true)
	checkmark_timer.stop()

func set_dot_modulate(l_colour):
	if(!get_node("/root/global").g_theme):
		get_node("Dot").set_modulate(l_colour.inverted())
		get_node("Dot/Label").add_color_override("font_color", l_colour)
	else:
		get_node("Dot").set_modulate(l_colour)
		get_node("Dot/Label").add_color_override("font_color", l_colour.inverted())
	
	get_node("Check Mark").set_modulate(get_node("Dot").get_modulate().inverted())

func set_dot_scale(l_scale):
	get_node("Dot").set_scale(l_scale)

func set_text(l_text):
	get_node("Dot/Label").set_text(l_text)

func play_completed_animation():
	get_node("Check Mark").set_hidden(false)
	_init_checkmark_timer()
	set_process(true)

func _process(delta):
	if get_node("Check Mark").get_scale().x > c_checkmark_final_scale.x:
		get_node("Check Mark").set_scale( get_node("Check Mark").get_scale() - c_checkmark_scale_increment )
	else:
		checkmark_timer.start()		# Start timer and wait 2 seconds before hidding the checkmark away
		set_process(false)
