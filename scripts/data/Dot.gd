extends Node2D

const c_default_dot_size = Vector2(50,50)
var g_dot_default_colour = Color(0,0,0,1)

const c_destroyed_size_scaler = 4			# The multiplier that scales the maximum size the dots inflates to before being deleted off the root tree

var g_bloating_increment_amount
var g_explode_while_destroying_amount

var g_dot_grid_index
var g_dot_pos
var g_dot_size
var g_dot_base_size
var g_dot_scaler
var g_dot_colour = g_dot_default_colour

var g_bloated_dot_increment = ( c_default_dot_size / 8 )
var g_dot_next_position

var g_destroyed_size = ( c_default_dot_size * c_destroyed_size_scaler )

var g_destroy_dot		# Used to visualy destroy/shrink and remove the dot from the tree
var g_is_alive			# Latch that holds the value if it's destroyed or not

func _ready():
	g_destroy_dot = false
	g_dot_next_position = null
	g_is_alive = true
	
	g_bloating_increment_amount = get_node("/root/viewport_data").g_dot_bloating_increment_amount
	g_explode_while_destroying_amount = g_bloating_increment_amount * 8
	
	set_process(true)

func _debug_dot():
	set_dot_pos(Vector2(200,200))

func _process(delta):
	if NOTIFICATION_DRAW:
		update()

func set_dot_pos(l_pos):
	g_dot_pos = l_pos
	set_pos(l_pos)

func set_dot_z(l_z):
	set_z(l_z)

func set_dot_scale(l_scale):
	g_dot_scaler = l_scale
	g_dot_base_size = ( g_dot_scaler * c_default_dot_size )
	g_dot_size = ( g_dot_scaler * c_default_dot_size )
	g_bloated_dot_increment = ( g_dot_base_size / 8 )
	g_destroyed_size = ( g_dot_base_size * c_destroyed_size_scaler )

func get_dot_scale():
	return g_dot_scaler

func get_dot_pos():
	return g_dot_pos

func get_dot_real_pos():
	return get_pos()

func set_dot_colour(l_colour):
	g_dot_colour = l_colour

func get_dot_colour():
	return g_dot_colour

func set_dot_index(l_dot_grid_index):
	g_dot_grid_index = l_dot_grid_index

func get_dot_index():
	return g_dot_grid_index

func get_dot_size():
	return g_dot_base_size

func bloat_dot():
	if(!g_destroy_dot):
		if ( g_dot_size < ( g_dot_base_size + g_bloated_dot_increment )):
			g_dot_size += g_bloating_increment_amount
	else:
		update()

func unbloat_dot():
	if(!g_destroy_dot):
		if ( g_dot_size > g_dot_base_size ):
			g_dot_size -= g_bloating_increment_amount
	else:
		update()

func queue_free_dot():
	queue_free()

func set_dot_next_position(l_new_dot_position, l_new_dot_index):
	if (g_dot_next_position == null):
		g_dot_pos = l_new_dot_position
		g_dot_next_position = l_new_dot_position
		g_dot_grid_index = l_new_dot_index
		
		return true
	
	return false

func is_moving():
	var l_next_incremented_position
	
	if(g_dot_next_position != null):
		l_next_incremented_position = get_pos() + get_node("/root/viewport_data").g_dot_movement_increment
		
		if(l_next_incremented_position.y < g_dot_next_position.y):			# Dot only moves in the y axis
			set_pos(l_next_incremented_position)
			update()
			
			return true
		elif ( l_next_incremented_position.y >= g_dot_next_position.y ):
			set_pos(g_dot_pos)
			update()
	
	if (get_pos() == g_dot_pos ):
		# When dot reaches it's predifined new position, set g_dot_next_position to null
		g_dot_next_position = null
	
	return false

func destroying_dot():
	var l_next_dot_size
	
	if g_is_alive:
		if ( g_dot_size.x < g_destroyed_size.x):
			l_next_dot_size = g_dot_size + g_explode_while_destroying_amount
			if ( l_next_dot_size.x < g_destroyed_size.y):
				g_dot_colour.a = float( g_dot_base_size.x / g_dot_size.x )
				g_dot_size = l_next_dot_size
				update()
			elif (l_next_dot_size.x >= g_destroyed_size.y):
				g_dot_size = g_destroyed_size
				g_dot_colour.a = 0
				update()
				g_is_alive = false
				
				return false
			
			return true
	else:
		return false

func _draw():
	if g_is_alive:
		draw_circle(Vector2(0,0), g_dot_size.x / 2, g_dot_colour)
	else:
		pass
