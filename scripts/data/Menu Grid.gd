extends TextureButton

onready var g_board_max_levels
var g_grid_level = 1

func _ready():
	if(!get_node("/root/global").g_theme):
		get_node("Grid Header").set_modulate(Color(0,0,0,1))
	else:
		get_node("Grid Header").set_modulate(Color(255,255,255,1))
	
	g_board_max_levels = get_node("Grid Dots").get_child_count()
	_setup_dots()

func _setup_dots():
	var l_name
	
	for i in range(get_node("Grid Dots").get_child_count()):
		l_name = String(i + 1)
		get_node("Grid Dots").get_child(i).set_name(l_name)
		get_node("Grid Dots").get_child(i).set_dot_modulate(Color(255,255,255,1))
		get_node("Grid Dots").get_child(i).set_text(l_name)

# Colorizes the dot objects according to the attained user level
func set_grid_user_level(l_level):
	if l_level <= g_board_max_levels:
		g_grid_level = l_level
		for i in range(l_level):
			get_node("Grid Dots").get_child(i).set_dot_modulate( get_node("/root/colours").dot_colours[randi() % get_node("/root/colours").dot_colours.size()] )

func check_if_grid_is_complete():
	if g_grid_level == g_board_max_levels:
		return true
	else:
		return false

func grid_play_won_level_animation(l_level):
	get_node("Grid Dots").get_child(l_level - 1).play_completed_animation()

# Creates an animation of a newly achieved user level on the grid
func set_grid_new_user_level(l_level):
	if l_level <= g_board_max_levels:
		get_node("Grid Dots").get_child(l_level).set_dot_modulate( get_node("/root/colours").dot_colours[randi() % get_node("/root/colours").dot_colours.size()] )
		get_node("Grid Dots").get_child(l_level).play_achived_animation()
