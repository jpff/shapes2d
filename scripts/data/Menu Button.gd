extends TextureButton

const DEBUG = false

var timer = null
var g_on_timer_wait = false

onready var g_debug_pos = self.get_viewport().get_rect().size / 2

# Button characteristics
const c_default_size = Vector2(400,400)
const c_default_center = ( c_default_size / 2 )
const c_default_pressed_scale = Vector2(0.5, 0.5)
const c_default_spawn_scale = Vector2(0.96, 0.96)
const c_tagged_destroyed_scale_value = 0.005

var g_with_animated_spawn = false
var g_button_scale = Vector2()
var g_circle_centre_pos = Vector2(0,0)

# PID
var Kp = 0
var Ki = 0
var Kd = 0
const bias = Vector2(0.00001,0.00001)
const c_time_multiplier = 250

# The PID algorithm was conceived to automatically compensate for system errors.
# Therefore, a blackbox function that wrought's a simulated frictioned or delayed output off an input is required.
const c_value_friction = Vector2(0.005,0.005)
var g_bloat_integrator = Vector2(0,0)
var g_previous_error = Vector2(0,0)
var g_integral = Vector2(0,0)
var g_previous_time = 0

var g_is_alive = true
var g_is_being_destroyed = false

func _ready():
	if DEBUG:
		connect("pressed", self, "_on_Menu_Button_pressed")
		load_PID_for_scale_0_625()
		#setup(g_debug_pos, Vector2(0.625,0.625), Color(0,0,0,1), 0.0001)			# No delay
		setup(g_debug_pos, Vector2(0.625,0.625), Color(0,0,0,1), 0.5)		# Half a second of delay before spawning
		set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_play_description, 0, Color(255,0,0,1), "texture 1")

func _init_timer(l_delay_S):
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(l_delay_S)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

func on_timeout():
	timer.stop()
	set_hidden(false)
	set_disabled(false)
	g_on_timer_wait = false

func setup(l_pos, l_scale, l_modulate, l_delayed_spawn_S, l_animated_spawn):
	set_disabled(true)
	set_hidden(true)
	set_button_pos(l_pos)
	set_button_scale(l_scale)
	set_modulate(l_modulate)
	
	if l_animated_spawn:
		_set_pid_scale( l_scale * c_default_spawn_scale )
	else:
		_set_pid_scale( l_scale )
	
	g_on_timer_wait = true
	_init_timer(l_delayed_spawn_S)
	timer.start()
	set_process(true)
	g_previous_time = _get_time_with_multiplier()

func play_click_animation():
	g_bloat_integrator = g_button_scale * c_default_pressed_scale
	_set_pid_scale( g_button_scale * c_default_pressed_scale )

func play_destroy_animation():
	Kp = 50000
	Ki = 0
	Kd = 0
	set_button_scale(Vector2(0.001,0.001))
	
	g_is_being_destroyed = true

func _process(delta):
	var l_derivative = Vector2()
	var l_pid_scale = Vector2()
	var l_current_time
	var dt
	var l_error
	
	if !g_on_timer_wait:
		l_current_time = _get_time_with_multiplier()
		dt = l_current_time - g_previous_time
		
		if dt != 0:
			l_error = g_button_scale - get_scale()
			
			g_integral = g_integral + ( l_error * dt )
			l_derivative = ( l_error - g_previous_error ) / dt
			
			l_pid_scale = ( Kp * l_error ) + ( Ki * g_integral ) + ( Kd * l_derivative ) + bias
			
			_set_pid_scale(_calc_scale_with_frictioned_value(l_pid_scale))
			
			g_previous_error = l_error
			g_previous_time = l_current_time
	else:
		g_previous_time = _get_time_with_multiplier()
	
	if g_is_being_destroyed && ( get_scale().x < c_tagged_destroyed_scale_value ):
		g_is_alive = false
		set_process(false)

func _calc_scale_with_frictioned_value(l_pid_scale):
	g_bloat_integrator += ( l_pid_scale * ( g_button_scale * Vector2(0.005,0.005) ) )
	return g_bloat_integrator



# --------------------------------------------------
# --------------- Debugging signal -----------------
# --------------------------------------------------

# This signal function is placed outside the node when it's being actively used in a project
func _on_Menu_Button_pressed():
	play_click_animation()
	invert_colours()
	#play_destroy_animation()



# --------------------------------------------------
# ------------- BUTTON API FUNCTIONS ---------------
# --------------------------------------------------

func check_if_alive():
	return g_is_alive

func invert_colours():
	set_modulate(get_modulate().inverted())
	
	for i in range(get_child_count()):
		if get_child(i).get_type() == "Sprite":
			get_child(i).set_modulate(get_child(i).get_modulate().inverted())

func set_button_overlay_texture(l_texture, l_z, l_modulate, l_child_name):
	var l_new_overlay_texture
	
	if l_texture.get_type() == "ImageTexture":
		l_new_overlay_texture = Sprite.new()
		l_new_overlay_texture.set_name(l_child_name)
		l_new_overlay_texture.set_texture( l_texture )
		l_new_overlay_texture.set_centered( false )
		l_new_overlay_texture.set_z(l_z)
		l_new_overlay_texture.set_z_as_relative(true)
		l_new_overlay_texture.set_modulate( l_modulate )
		l_new_overlay_texture.set_scale( c_default_size / l_new_overlay_texture.get_texture().get_size() )
		l_new_overlay_texture.set_pos( c_default_center - ( ( l_new_overlay_texture.get_texture().get_size() * l_new_overlay_texture.get_scale() ) / 2 ) )
		add_child( l_new_overlay_texture )
		return true
	else:
		return false

func set_child_hidden(l_state, l_child_name):
	for i in range(get_child_count()):
		if get_child(i).get_name() == l_child_name:
			get_child(i).set_hidden(l_state)
			return true
	return false

func get_child_hidden_status(l_child_name):
	for i in range(get_child_count()):
		if get_child(i).get_name() == l_child_name:
			return get_child(i).is_hidden()
	return null

func set_child_modulate(l_modulate, l_child_name):
	for i in range(get_child_count()):
		if get_child(i).get_name() == l_child_name:
			get_child(i).set_modulate(l_modulate)
			return true
	return false

func get_child_size(l_child_name):
	for i in range(get_child_count()):
		if get_child(i).get_name() == l_child_name:
			return ( get_child(i).get_texture().get_size() * get_child(i).get_scale() )

func set_child_overscale(l_overscale, l_child_name):
	var l_child_offset = 0
	
	for i in range(get_child_count()):
		if get_child(i).get_name() == l_child_name:
			get_child(i).set_scale( get_child(i).get_scale() + l_overscale )
			get_child(i).set_pos( ( get_normal_texture().get_size() / 2 ) - ( get_child_size(l_child_name) / 2 ) )
			
			return true
	return false

func set_button_scale(l_scale):
	g_button_scale = l_scale
	
	set_scale(l_scale)
	set_pos( g_circle_centre_pos - ( get_button_size() / 2 ) )

func _set_pid_scale(l_scale):
	set_scale(l_scale)
	set_pos( g_circle_centre_pos - ( get_button_size() / 2 ) )

func get_button_size():
	return ( c_default_size * get_scale() )

func set_button_pos(l_pos):
	g_circle_centre_pos = l_pos
	set_pos( g_circle_centre_pos - ( get_button_size() / 2 ) )

func get_button_center_pos():
	return ( g_circle_centre_pos - ( get_button_size() / 2 ) )

func get_button_pos():
	return get_pos()

func _get_time():
	return( float(OS.get_ticks_msec()) / 1000 )

func _get_time_with_multiplier():
	return ( _get_time() * c_time_multiplier )

func load_config_for_scale_0_4():
	Kp = 80
	Ki = 4
	Kd = 50
	
	set_button_scale(Vector2(0.4, 0.4))

func load_config_for_scale_0_625():
	Kp = 50
	Ki = 4
	Kd = 50
	
	set_button_scale(Vector2(0.625,0.625))

func load_PID_for_scale_0_4():
	Kp = 80
	Ki = 4
	Kd = 50

func load_PID_for_scale_0_625():
	Kp = 50
	Ki = 4
	Kd = 50

func set_custom_PID(l_Kp, l_Ki, l_Kd):
	Kp = l_Kp
	Ki = l_Ki
	Kd = l_Kd
