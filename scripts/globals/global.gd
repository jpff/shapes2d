extends Node

##################################
# ON RELEASE SET DEBUG  TO FALSE #
##################################

const DEBUG = true						# WARNING: Setting to false will make the app use production ads
const DEBUG_BOARD = false
var free_to_play_version = true

const c_hearts_for_premium = 5
var g_hearts_found = 0

const c_googleplay_app_page = "market://details?id=apps.kloudpixel.shapes2d"
const c_privacy_policy_url = "http://shapes2d-game-privacy.hasherkeen.com/"

# File names and directories
const CONFIG_FILE = "user://settings.cfg"
const SAVE_FILE = "user://data.bin"
const CHECKSUM_FILE = "user://check.bin"

var g_first_time_app_launch = true
onready var g_accepted_tos = false

const timer_timeout_secs = 0.4
const menu_button_timeout_secs = 0.07

var current_scene = null

# Global menu scene nodes
const Mainmenu = "res://scenes/menu/Mainmenu.tscn"					# Scene 1
const Pre_Board_Menu = "res://scenes/menu/Pre_Board_Menu.tscn"		# Scene 2
const Settings = "res://scenes/menu/Settings.tscn"					# Scene 4
const About = "res://scenes/menu/About.tscn"						# Scene 5
const GamePlayUI = "res://scenes/gameplay/GamePlay UI.tscn"			# Scene 6
const Help = "res://scenes/menu/Help.tscn"							# Scene 7
const LoadingScreen = "res://scenes/menu/Loading Screen.tscn"		# Scene 8
const StockAdvert = "res://scenes/menu/Stock Advert.tscn"			# Scene 9

# Global settings variables
const c_cleartheme_colour = "white"
const c_darktheme_colour = "black"

# true -> enabled, false -> disabled
var g_toggle_timer = true
var g_toggle_music = true
# 0 -> theme white, 1 -> theme dark
var g_theme = 0

# Variable used for a self implemented debug print function
var g_print_line = []
var g_print_buffered = []

var g_password = null

# Gameplay related

const c_user_level_template = [{grid_index = 0, grid_user_level = 1, grid_max_user_level = 16}, {grid_index = 1, grid_user_level = 1, grid_max_user_level = 25}, {grid_index = 2, grid_user_level = 1, grid_max_user_level = 36}, {grid_index = 3, grid_user_level = 1, grid_max_user_level = 49}, {grid_index = 4, grid_user_level = 1, grid_max_user_level = 64}, {grid_index = 5, grid_user_level = 1, grid_max_user_level = 81}]
onready var g_grid_userlevels = c_user_level_template

var g_board_type
var g_board_user_level
var g_board_newly_won_level = -1

func _ready():
	seed(OS.get_unix_time())
	
	_determine_password()
	_parse_read_savefile()
	_parse_read_config()
	parse_premium_status()
	
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1 )
	self.set_process(true)



func found_heart():
	g_hearts_found += 1
	parse_premium_status()
	_parse_write_savefile()

func parse_premium_status():
	if g_hearts_found >= c_hearts_for_premium:
		free_to_play_version = false
	else:
		free_to_play_version = true

func start_board_game():
	var s = ResourceLoader.load(GamePlayUI)
	current_scene.queue_free()
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	current_scene.start_board_game(g_board_user_level, g_board_type)
	get_tree().set_current_scene(current_scene)

func set_board(l_board_type):
	g_board_type = l_board_type
	g_board_user_level = g_grid_userlevels[g_board_type].grid_user_level

func reset_board_controls():
	g_board_type = -1
	g_board_user_level = 0
	g_board_newly_won_level = -1

func board_won():
	if ( g_board_user_level + 1 ) <= c_user_level_template[g_board_type].grid_max_user_level:
		save_user_score( g_board_user_level + 1 )
		g_board_newly_won_level = g_board_user_level + 1
	
	parse_premium_status()
	
	get_node("/root/admob_global").display_interstitial_toggler_on_off = !get_node("/root/admob_global").display_interstitial_toggler_on_off
	if get_node("/root/admob_global").display_interstitial_toggler_on_off:
		if get_node("/root/admob_global").g_interstitialLoaded:
			get_node("/root/admob_global").display_interstitial()
			goto_scene(LoadingScreen)
		else:
			goto_scene(StockAdvert)
	else:
			goto_scene(Pre_Board_Menu)

func board_lost():
	get_node("/root/admob_global").display_interstitial_toggler_on_off = !get_node("/root/admob_global").display_interstitial_toggler_on_off
	if get_node("/root/admob_global").display_interstitial_toggler_on_off:
		if get_node("/root/admob_global").g_interstitialLoaded:
			get_node("/root/admob_global").display_interstitial()
			goto_scene(LoadingScreen)
		else:
			goto_scene(StockAdvert)
	else:
			goto_scene(Pre_Board_Menu)

func save_user_score(l_grid_userlevel):
	if l_grid_userlevel > g_grid_userlevels[g_board_type].grid_user_level:
		g_grid_userlevels[g_board_type].grid_user_level = l_grid_userlevel
		_parse_write_savefile()

func board_retry():
	start_board_game()



func exit_game():
	get_tree().quit()

# Append data to be printed
func PrintAppend(l_data):
	g_print_line.append(l_data)

# Print all the data in a single line
func PrintLine():
	print(g_print_line)
	g_print_line = []		# Reset string array

# used when console text overflow occurs constantly
func PrintBufferedAppend(l_data):
	g_print_buffered.append(l_data)

func PrintBuffered():
	if g_print_buffered.size() > 0:
		print(g_print_buffered[0])
		g_print_buffered.pop_front()

func _process(delta):
	PrintBuffered()		# Print anything that's been buffered

func new_settings(l_toggle_music, l_toggle_timer, l_theme):
	g_toggle_music = l_toggle_music
	g_toggle_timer = l_toggle_timer
	g_theme = l_theme
	_parce_write_config()

func goto_scene(path):
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path):
	var s = ResourceLoader.load(path)
	current_scene.free()
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene( current_scene )

func _parse_read_config():
	# Config file is saved at the end
	
	var config = ConfigFile.new()
	var file = File.new()
	var dir = Directory.new()
	
	var err = config.load(CONFIG_FILE)
	
	if err == OK: # if not, something went wrong with the file loading
		# Read sound settings
		if not config.has_section_key("audio", "mute"):		# If entry does not exists then create it
			config.set_value("audio", "mute", false)		# And assign default values
		else:
			# Otherwise read values
			g_toggle_music = !config.get_value("audio", "mute")		# g_toggle_music = 1 -> enabled sounds
		
		# Read timer settings
		if not config.has_section_key("game_timer", "on"):		# If entry does not exists then create it
			config.set_value("game_timer", "on", true)		# And assign default values
		else:
			# Otherwise read values
			g_toggle_timer = config.get_value("game_timer", "on")
		
		# Read theme settings
		if not config.has_section_key("game_theme", "style"):		# If entry does not exists then create it
			config.set_value("game_theme", "style", 0)		# And assign default values
		else:
			# Otherwise read values
			g_theme = config.get_value("game_theme", "style")
		
		# Proceed now to filter out the read values off the config file
		if(g_toggle_music != true && g_toggle_music != false):
			config.set_value("audio", "mute", false)
			g_toggle_music = true
		
		if(g_toggle_timer != true && g_toggle_timer != false):
			config.set_value("game_timer", "on", true)
			g_toggle_timer = true
		
		if(g_theme != 0 && g_theme != 1):
			config.set_value("game_theme", "style", 0)
			g_theme = 0
		
		# If the game is the free to play version and the config file has been changed to disable the timer
		# then proceed to overwrite with the default values of timer enabled
		if(free_to_play_version && g_toggle_timer == false):
			config.set_value("game_timer", "on", true)
			g_toggle_timer = true
	else:
		# If the config file's loading failed in some manner then,
		# and firstly, delete the file and generate a new one...
		if(file.file_exists(CONFIG_FILE)):
			err = dir.remove(CONFIG_FILE)	# Delete the existing file
			err = file.open(CONFIG_FILE, file.WRITE_READ)	# Create a new file
		else:
			err = file.open(CONFIG_FILE, file.WRITE_READ)
		
		# Reload the file again
		var err = config.load(CONFIG_FILE)
		
		# If no errors were found then...
		if err == OK:
			# Then set its values to the default ones
			config.set_value("audio", "mute", false)
			config.set_value("game_timer", "on", true)
			config.set_value("game_theme", "style", 0)
			
			var g_toggle_timer = true
			var g_toggle_music = true
			var g_theme = 0
	
	err = config.save(CONFIG_FILE)

func _parce_write_config():
	var config = ConfigFile.new()
	
	var err = config.load(CONFIG_FILE)
	
	if err == OK:
		config.set_value("audio", "mute", !g_toggle_music)
		
		if(free_to_play_version):
			config.set_value("game_timer", "on", true)
		else:
			config.set_value("game_timer", "on", g_toggle_timer)
		
		config.set_value("game_theme", "style", g_theme)
		err = config.save(CONFIG_FILE)

func _determine_password():
	var l_password_array = []
	
	# Wait if variables aren't available yet
	while ( get_node("/root/viewport_data").g_viewport_rect_area == Vector2(0,0) || get_node("/root/viewport_data").g_viewport_rect_area == null ):
		pass
	
	# Assemble the password array first
	l_password_array.append(OS.get_unique_ID())
	l_password_array.append(OS.get_name())
	l_password_array.append(get_node("/root/viewport_data").g_viewport_rect_area)
	g_password = String(l_password_array.hash())

func _reset_savefile():
	var file = File.new()
	var dir = Directory.new()
	var l_tempdata
	var err
	
	if file.file_exists(SAVE_FILE):
		err = dir.remove(SAVE_FILE)
	
	l_tempdata = {first_time_app_launch = true, os_name = OS.get_name(), device_res = get_node("/root/viewport_data").g_viewport_rect_area, grid_userlevel = c_user_level_template, accepted_tos = false, hearts_found = 0}
	
	err = file.open_encrypted_with_pass(SAVE_FILE, File.WRITE, g_password)		# Create a new file
	err = file.store_var(l_tempdata)
	err = file.close()
	
	_register_md5_cheksum()

# Funcion registers the files checksums in order to validate them upon loading
func _register_md5_cheksum():
	var filetype_savefile = File.new()
	var filetype_checksumfile = File.new()
	var dir = Directory.new()
	
	if filetype_savefile.file_exists(SAVE_FILE):
		if filetype_checksumfile.file_exists(CHECKSUM_FILE):
			dir.remove(CHECKSUM_FILE)
		
		filetype_checksumfile.open(CHECKSUM_FILE, File.WRITE)
		filetype_checksumfile.store_line(filetype_savefile.get_md5(SAVE_FILE))
		filetype_checksumfile.close()
		
		return 0
	else:	# If no data file to create a checksum of exists, return -1 as error value
		return -1

func _read_md5_checksum():
	var filetype_checksumfile = File.new()
	var l_checksum
	
	if filetype_checksumfile.file_exists(CHECKSUM_FILE):
		filetype_checksumfile.open(CHECKSUM_FILE, File.READ)
		l_checksum = filetype_checksumfile.get_line()
		filetype_checksumfile.close()
		
		return l_checksum
	else:
		return null

func _parse_read_savefile():
	var file = File.new()
	var dir = Directory.new()
	var l_tempdata
	var err
	
	if ( OS.get_name() == "Android" || OS.get_name() == "iOS" || DEBUG == true):	# If debugging, then allow all OS's
		err = file.open_encrypted_with_pass(SAVE_FILE, File.READ, g_password)
		
		if err == OK:																# If no errors occured
			l_tempdata = file.get_var()
			
			if l_tempdata.size() == 6:
				if ( _read_md5_checksum() == file.get_md5(SAVE_FILE) ):
					g_hearts_found = l_tempdata.hearts_found
					g_first_time_app_launch = l_tempdata.first_time_app_launch
					g_grid_userlevels = l_tempdata.grid_userlevel
					g_accepted_tos = l_tempdata.accepted_tos
					err = file.close()
				else:																	# If the previous verifications failed, then reset the savefile
					if file.is_open():
						file.close()
					_reset_savefile()
					g_grid_userlevels = c_user_level_template
			else:																	# If the previous verifications failed, then reset the savefile
				if file.is_open():
					file.close()
				_reset_savefile()
				g_grid_userlevels = c_user_level_template
		else:		# Else, if an error occurred, reset file and set user values to default ones
			if file.is_open():
				file.close()
			_reset_savefile()
			g_grid_userlevels = c_user_level_template
	else:			# If OS's aren't either Android nor iOS, nor debugging the game, then force exit the application
		get_tree().quit()

func _parse_write_savefile():
	var file = File.new()
	var dir = Directory.new()
	var l_tempdata
	var err
	
	if ( OS.get_name() == "Android" || OS.get_name() == "iOS" || DEBUG == true):			# If OS's are Android or iOS, then use encryption for file protection
		l_tempdata = {first_time_app_launch = g_first_time_app_launch, os_name = OS.get_name(), device_res = get_node("/root/viewport_data").g_viewport_rect_area, grid_userlevel = g_grid_userlevels, accepted_tos = g_accepted_tos, hearts_found = g_hearts_found}
		
		if file.file_exists(SAVE_FILE):		# If file already exist
			err = dir.remove(SAVE_FILE)		# Then delete it
		
		err = file.open_encrypted_with_pass(SAVE_FILE, File.WRITE, g_password)
		
		if err == OK:		# If no errors occured
			err = file.store_var(l_tempdata)		# Append data to it
			err = file.close()						# Close it
			_register_md5_cheksum()					# Generate the checksums for the file
	else:		# If OS's aren't either Android nor iOS, nor debugging the game, the exit the application
		get_tree().quit()

func save_user_data():
	_parse_write_savefile()

func reset_levels():
	g_grid_userlevels = c_user_level_template
	_parse_write_savefile()
