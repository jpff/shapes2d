extends Node2D

onready var t_gameoverlay_description = preload("res://assets/textures/menu button/menu button textures/game overlay/Game Overlay Label.tex")

onready var t_mainmenu_about_description = preload("res://assets/textures/menu button/menu button textures/mainmenu/ABOUT.tex")
onready var t_mainmenu_exit_description = preload("res://assets/textures/menu button/menu button textures/mainmenu/EXIT.tex")
onready var t_mainmenu_play_description = preload("res://assets/textures/menu button/menu button textures/mainmenu/PLAY.tex")
onready var t_mainmenu_settings_description = preload("res://assets/textures/menu button/menu button textures/mainmenu/SETTINGS LABEL.tex")
onready var t_mainmenu_heart_texture = preload("res://assets/textures/menu button/menu button textures/mainmenu/SHIMMERING HEART.tex")
onready var t_mainmenu_help_texture = preload("res://assets/textures/menu button/menu button textures/mainmenu/HELP.tex")

onready var t_pausemenu_return_description = preload("res://assets/textures/menu button/menu button textures/pause menu/RETURN.tex")
onready var t_pausemenu_save_and_exit_description = preload("res://assets/textures/menu button/menu button textures/pause menu/SAVE AND.tex")

onready var t_preboardmenu_back_description = preload("res://assets/textures/menu button/menu button textures/pre_board menu/BACK.tex")
onready var t_preboardmenu_play_description = preload("res://assets/textures/menu button/menu button textures/pre_board menu/PLAY.tex")

onready var t_resultsmenu_save_and_exit_description = preload("res://assets/textures/menu button/menu button textures/results menu/SAVE AND.tex")
onready var t_resultsmenu_try_again_description = preload("res://assets/textures/menu button/menu button textures/results menu/Try Again Description.tex")

onready var t_settingsmenu_back_description = preload("res://assets/textures/menu button/menu button textures/settings/BACK.tex")
onready var t_settingsmenu_lock = preload("res://assets/textures/menu button/menu button textures/settings/Lock (1).tex")
onready var t_settingsmenu_pocket_clock = preload("res://assets/textures/menu button/menu button textures/settings/Pocket Clock.tex")
onready var t_settingsmenu_save_description = preload("res://assets/textures/menu button/menu button textures/settings/SAVE.tex")
onready var t_settingsmenu_musical_note = preload("res://assets/textures/menu button/menu button textures/settings/musical-note-400px resized.tex")
onready var t_settingsmenu_themeclear_description = preload("res://assets/textures/menu button/menu button textures/settings/THEME CLEAR #1.tex")
onready var t_settingsmenu_themeclear_boundary = preload("res://assets/textures/menu button/menu button textures/settings/THEME CLEAR BOUNDARY.tex")
onready var t_settingsmenu_themeclear_disabled = preload("res://assets/textures/menu button/menu button textures/settings/THEME CLEAR TEXTURE INTERIOR DISABLED.tex")
onready var t_settingsmenu_themedark_boundary = preload("res://assets/textures/menu button/menu button textures/settings/THEME DARK BOUNDARY.tex")
onready var t_settingsmenu_themedark_disabled = preload("res://assets/textures/menu button/menu button textures/settings/THEME DARK TEXTURE INTERIOR DISABLED.tex")
onready var t_settingsmenu_themedark_description = preload("res://assets/textures/menu button/menu button textures/settings/THEME DARK.tex")
onready var t_settingsmenu_music_boundary = preload("res://assets/textures/menu button/menu button textures/settings/TOGGLE MUSIC BOUNDARY.tex")
onready var t_settingsmenu_music_disabled = preload("res://assets/textures/menu button/menu button textures/settings/TOGGLE MUSIC INTERIOR DISABLED.tex")
onready var t_settingsmenu_timer_boundary = preload("res://assets/textures/menu button/menu button textures/settings/TOGGLE TIMER BOUNDARY.tex")
onready var t_settingsmenu_timer_disabled = preload("res://assets/textures/menu button/menu button textures/settings/TOGGLE TIMER INTERIOR DISABLED.tex")

onready var t_menu_button_circle = preload("res://assets/textures/menu button/circular button 400x400.tex")

onready var t_likemenu_star = preload("res://assets/textures/menu button/menu button textures/like/Star.tex")
onready var t_likemenu_rateapp_desc = preload("res://assets/textures/menu button/menu button textures/like/RATE APP.tex")
onready var t_likemenu_watchvideo = preload("res://assets/textures/menu button/menu button textures/like/CLICK TO WATCH.tex")
onready var t_likemenu_watchvideoicon = preload("res://assets/textures/menu button/menu button textures/like/WATCH ICON.tex")

onready var menu_button = preload("res://scenes/data/Menu Button.tscn")

const c_button_default_scale = Vector2(0.625,0.625)
onready var g_button_scale = Vector2(0,0)

func _ready():
	g_button_scale = c_button_default_scale
