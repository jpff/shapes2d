extends Node2D

var display_interstitial_toggler_on_off = true		# used as a latch to not having to display an ad everytime, instead it's set on and off

const c_bannerEnabled = true
const c_interstitialEnabled = true
const c_rewardedEnabled = false

const c_googleTestBannerId = "ca-app-pub-3940256099942544/6300978111"
const c_googleTestInterstitialId = "ca-app-pub-3940256099942544/1033173712"
const c_googleTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917"

# Edit with production/live admob id
const c_productionBannerId = "ca-app-pub-7691445420692174/1926229575"
const c_productionInterstitialId = "ca-app-pub-7691445420692174/1351213184"
const c_productionRewardedVideoId = ""

var admob = null
var isTop = true
onready var adBannerId
onready var adInterstitialId
onready var adRewardedId

# Control check variables
var g_bannerLoaded = false
var g_interstitialLoaded = false
var g_rewardedLoaded = false

func _ready():
	if(Globals.has_singleton("AdMob")):
		admob = Globals.get_singleton("AdMob")
		
		if get_node("/root/global").DEBUG:
			adBannerId = c_googleTestBannerId
			adInterstitialId = c_googleTestInterstitialId
			adRewardedId = c_googleTestRewardedVideoId
			admob.init(false, get_instance_ID())
		else:
			adBannerId = c_productionBannerId
			adInterstitialId = c_productionInterstitialId
			adRewardedId = c_productionRewardedVideoId
			admob.init(true, get_instance_ID())
		
		if c_bannerEnabled:
			loadBanner()
		if c_interstitialEnabled:
			loadInterstitial()
		if c_rewardedEnabled:
			loadRewardedVideo()
	get_tree().connect("screen_resized", self, "onResize")

func onResize():
	if admob != null:
		admob.resize()

func loadBanner():
	if admob != null:
		admob.loadBanner(adBannerId, isTop)

func loadInterstitial():
	if admob != null:
		admob.loadInterstitial(adInterstitialId)

func loadRewardedVideo():
	if admob != null:
		admob.loadRewardedVideo(adRewardedId)

func display_banner():
	if admob != null:
		if g_bannerLoaded:
			admob.showBanner()

func hide_banner():
	if admob != null:
		if g_bannerLoaded:
			admob.hideBanner()

func display_interstitial():
	if admob != null:
		if g_interstitialLoaded:
			admob.showInterstitial()

func display_rewardedvideo():
	if admob != null:
		if g_rewardedLoaded:
			admob.showRewardedVideo()

func _on_interstitial_close():
	get_node("/root/global").goto_scene(get_node("/root/global").Pre_Board_Menu)

func _on_admob_ad_loaded():
	g_bannerLoaded = true
	hide_banner()

func _on_interstitial_loaded():
	g_interstitialLoaded = true

func _on_rewarded_video_ad_loaded():
	g_rewardedLoaded = true

func _on_admob_network_error():
	pass

func _on_interstitial_not_loaded():
	g_interstitialLoaded = false

func _on_rewarded_video_ad_closed():
	pass

func _on_rewarded(currency, amount):
	pass
