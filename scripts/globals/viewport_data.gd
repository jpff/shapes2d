extends Node2D

# Base references
const c_default_window_size = Vector2(600,1024)
const c_default_rect_area = Rect2(Vector2(0,0), Vector2(600,1024))
const c_default_base_area_pos_increment = Vector2(50,50)
const c_default_base_scaler = c_default_window_size / c_default_base_area_pos_increment
const c_board_scale = Vector2(1,1)
const c_board_center = ( c_default_window_size / 2 ) * c_board_scale	# Game was developed for res 600x1024

onready var g_board_center_scaled
onready var g_viewport_scaler

# Global - general variables
var g_viewport_rect
var g_viewport_rect_pos
var g_viewport_rect_area

# Global - menu dot variables
var g_viewport_base_area_scaler

const c_dot1_index_reference = Vector2(3,2)
const c_dot2_index_reference = Vector2(9,2)
const c_dot3_index_reference = Vector2(3,8)
const c_dot4_index_reference = Vector2(9,8)
const c_dot5_index_reference = Vector2(3,14)
const c_dot6_index_reference = Vector2(9,14)
const c_dot7_index_reference = Vector2(3,20)
const c_dot8_index_reference = Vector2(9,20)

var g_dot1_pos
var g_dot2_pos
var g_dot3_pos
var g_dot4_pos
var g_dot5_pos
var g_dot6_pos
var g_dot7_pos
var g_dot8_pos
onready var g_dot_pos = []

var g_menu_dot_diameter

const c_bloating_increment_amount_reference = Vector2(1,1)
var g_dot_bloating_increment_amount

const c_dot_movement_multiplier = 14
const c_dot_movement_amount = ( Vector2(0,1) * c_dot_movement_multiplier )	# Dots are only translated in the x axis
onready var g_dot_movement_increment

func _ready():
	g_viewport_rect = get_viewport().get_rect()
	g_viewport_rect_pos = g_viewport_rect.pos
	g_viewport_rect_area = g_viewport_rect.end
	g_viewport_base_area_scaler = g_viewport_rect_area / c_default_base_scaler
	g_viewport_scaler = g_viewport_rect_area / c_default_window_size
	g_dot_bloating_increment_amount = ( ( c_bloating_increment_amount_reference * g_viewport_rect_area.y ) / c_default_window_size.y )
	g_dot_movement_increment = Vector2( 0, ( ( c_dot_movement_amount.y * g_viewport_rect_area.y ) / c_default_window_size.y ) )
	g_board_center_scaled = c_board_center * g_viewport_scaler
	_set_dot_pos_references()

func _set_dot_pos_references():
	g_dot1_pos = c_default_base_area_pos_increment * c_dot1_index_reference
	g_dot2_pos = c_default_base_area_pos_increment * c_dot2_index_reference
	g_dot3_pos = c_default_base_area_pos_increment * c_dot3_index_reference
	g_dot4_pos = c_default_base_area_pos_increment * c_dot4_index_reference
	g_dot5_pos = c_default_base_area_pos_increment * c_dot5_index_reference
	g_dot6_pos = c_default_base_area_pos_increment * c_dot6_index_reference
	g_dot7_pos = c_default_base_area_pos_increment * c_dot7_index_reference
	g_dot8_pos = c_default_base_area_pos_increment * c_dot8_index_reference
	
	g_dot_pos.append(g_dot2_pos)
	g_dot_pos.append(g_dot1_pos)
	g_dot_pos.append(g_dot4_pos)
	g_dot_pos.append(g_dot3_pos)
	g_dot_pos.append(g_dot6_pos)
	g_dot_pos.append(g_dot5_pos)
	g_dot_pos.append(g_dot8_pos)
	g_dot_pos.append(g_dot7_pos)
