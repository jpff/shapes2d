extends Node

const exclude_first_set_of_colours = 4

var g_colours_node

# Datastruct for colour type
#var <colour> = {name = "colour_name", value = Color(colour_value)}

var colours = []
var used_colours_log = []
var dot_colours = []

func _ready():
	# Instance a new 2D colour munsell canvas node
	g_colours_node = preload("res://scenes/data/colours.tscn").instance()
	
	rand_seed(OS.get_unix_time())
	
	# Clear arrays
	colours.clear()
	used_colours_log.clear()
	
	# Fill in colour array values
	_fetch_munsell_and_assign_colours()
	
	for i in range(get_node("/root/colours").colours.size() - exclude_first_set_of_colours):
		dot_colours.append(colours[i + exclude_first_set_of_colours].value)

# Fetches the pre-established munsell colours off the 2D node and assigns them to a colour array
func _fetch_munsell_and_assign_colours():
	var l_colour
	
	for i in range(g_colours_node.get_child_count()):
		l_colour = 0
		l_colour = {name = g_colours_node.get_child(i).get_name(), value = g_colours_node.get_child(i).get_modulate()}
		colours.append(l_colour)

# Fetches colour's rgb value
func get_colour(l_colour_name):
	for i in range(colours.size()):
		if colours[i].name == l_colour_name:
			return colours[i].value
	# If no value was found then return null
	return null

# Returns a colour used for the menu
func get_random_menu_colour():
	var l_rand_index = ( randi() % colours.size())
	
	while(colours[l_rand_index].name == "white" || colours[l_rand_index].name == "black"):
		l_rand_index = ( randi() % colours.size())
	
	return colours[l_rand_index].value
