extends Node2D

const DEBUG = false

var shape_processor = preload("res://scenes/gameplay/Shape Processor.tscn")

onready var g_shapes_array
var g_identified_shape = null
onready var g_shapes_reference = []			# Array of reference arrays
onready var g_shape_size_reference = []

func _ready():
	add_to_group("board")
	set_hidden(true)
	_setup()

func _setup():
	g_shapes_array = get_node("Shapes Reference Sheet").get_shapes()
	var l_temp_shapes = g_shapes_array
	
	for i in range(l_temp_shapes.size()):
		if typeof(l_temp_shapes[i].shape[0]) == TYPE_ARRAY:
			for j in range(l_temp_shapes[i].shape.size()):
				g_shapes_reference.append( l_temp_shapes[i].shape[j] )
				g_shape_size_reference.append( _calc_shape_size( l_temp_shapes[i].shape[j] ) )
		else:
			g_shapes_reference.append( l_temp_shapes[i].shape )
			g_shape_size_reference.append( _calc_shape_size( l_temp_shapes[i].shape ) )
	
	if DEBUG:
		get_node("Shape Viewer").set_hidden(false)
	else:
		get_node("Shape Viewer").queue_free()
		get_node("Shape Viewer").remove_and_skip()



# -----------------------/////////////////--------------------------
# 				Shapes Engine process functions						
# -----------------------/////////////////--------------------------

func validate_shape(l_shape_to_verify):
	# Determine which shapes have the same number of indexes to trim down possibilities and speed up processing
	for i in range(g_shapes_reference.size()):
		if g_shapes_reference[i].size() == l_shape_to_verify.size():
			if _check_shape(l_shape_to_verify, i):
				if g_identified_shape.size() == 8:
					if _validate_if_heart(g_identified_shape):
						get_node("/root/global").found_heart()
						get_tree().call_group(0, "board", "user_found_heart")
				return true
		
		#if l_shape_to_verify.size() == 4:
		#	if _check_for_trapezoid_shape(l_shape_to_verify):
		#		return true
	
	return false		# By default return false if no shapes were validated



func _validate_if_heart(l_shape):
	for i in range(g_shapes_array.size()):
		if g_shapes_array[i].name == "heart":
			for j in range(g_shapes_array[i].shape.size()):
				if g_shapes_array[i].shape[j] == l_shape:
					return true
	return false



# -----------------------/////////////////--------------------------
# 				Functions to check for a trapezoid					
# -----------------------/////////////////--------------------------

func _check_for_trapezoid_shape( a_trapezoid_to_verify ):
	var l_shape_processor = shape_processor.instance()
	var l_filtered_shape = _filter_shape_rotation( a_trapezoid_to_verify )
	var l_shape_to_verify = []
	
	print("&")
	print(l_filtered_shape)
	
	if DEBUG:
		get_node("Shape Viewer").set_shape(l_filtered_shape)
		get_node("Shape Viewer").show_shape_window()
	
	l_shape_processor.instance_shape( l_filtered_shape )
	#l_shape_processor.instance_shape( a_trapezoid_to_verify )		# The shape_processor node will autocenter the shape, so it's used here to save on code space
	l_shape_to_verify = l_shape_processor.get_shape()
	
	# Auxiliary quadrant array containing point P1, P2, P3 and P4 in respective order, P1 is contained within the first quadrant, P2 within the second, and so on
	var QuandrantArray = []
	QuandrantArray.resize(4)
	var QuadrantArray_trapezoid_assigned_index = []
	QuadrantArray_trapezoid_assigned_index.resize(4)
	var l_quadrant_index
	
	for i in range(l_shape_to_verify.size()):
		l_quadrant_index = _determine_point_quadrant(l_shape_to_verify[i]) - 1
		QuandrantArray[l_quadrant_index] = l_shape_to_verify[i]
		QuadrantArray_trapezoid_assigned_index[l_quadrant_index] = i
	
	var l_found_trapezoid_shape = _check_if_shape_is_a_trapezoid(QuandrantArray)
	var l_new_unitary_trapezoid = []
	l_new_unitary_trapezoid.resize(4)
	
	if l_found_trapezoid_shape == 1:
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[0]] = Vector2(2,1)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[1]] = Vector2(1,1)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[2]] = Vector2(0,0)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[3]] = Vector2(3,0)
	elif l_found_trapezoid_shape == 2:
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[0]] = Vector2(3,1)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[1]] = Vector2(0,1)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[2]] = Vector2(1,0)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[3]] = Vector2(2,0)
	elif l_found_trapezoid_shape == 3:
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[0]] = Vector2(1,3)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[1]] = Vector2(0,2)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[2]] = Vector2(0,1)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[3]] = Vector2(1,0)
	elif l_found_trapezoid_shape == 4:
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[0]] = Vector2(1,2)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[1]] = Vector2(0,3)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[2]] = Vector2(0,0)
		l_new_unitary_trapezoid[QuadrantArray_trapezoid_assigned_index[3]] = Vector2(1,1)
	else:
		return false
	
	for i in range(g_shapes_reference.size()):
		if g_shapes_reference[i].size() == l_new_unitary_trapezoid.size():
			if _check_shape(l_new_unitary_trapezoid, i):
				return true
	
	# Return false by default, unless a shape was found
	return false

func _determine_point_quadrant(l_point):
	if l_point.x > 0 && l_point.y > 0:		# First quadrant
		return 1
	elif l_point.x < 0 && l_point.y > 0:	# Second quadrant
		return 2
	elif l_point.x < 0 && l_point.y < 0:	# Third quadrant
		return 3
	elif l_point.x > 0 && l_point.y < 0:	#Fourth quadrant
		return 4

func _check_for_trapezoid_centered_with_no_rotation(P1, P2, P3, P4):
	return ( P2.y == P1.y && P3.y == P4.y && P3.x < P2.x && P4.x > P1.x )

func _check_for_trapezoid_centered_90_degrees_rotation(P1, P2, P3, P4):
	return ( P2.y == P1.y && P3.y == P4.y && P2.x < P3.x && P1.x > P4.x )

func _check_for_trapezoid_centered_180_degrees_rotation(P1, P2, P3, P4):
	return ( P1.x == P4.x && P2.x == P3.x && P1.y > P2.y && P4.y < P3.y )

func _check_for_trapezoid_centered_270_degrees_rotation(P1, P2, P3, P4):
	return ( P1.x == P4.x && P2.x == P3.x && P2.y > P1.y && P3.y < P4.y )

func _check_if_shape_is_a_trapezoid(l_trapezoid_to_verify):
	if _check_array_for_null_values(l_trapezoid_to_verify) == false:
		if _check_for_trapezoid_centered_with_no_rotation(l_trapezoid_to_verify[0], l_trapezoid_to_verify[1], l_trapezoid_to_verify[2], l_trapezoid_to_verify[3]):
			return 1
		elif _check_for_trapezoid_centered_90_degrees_rotation(l_trapezoid_to_verify[0], l_trapezoid_to_verify[1], l_trapezoid_to_verify[2], l_trapezoid_to_verify[3]):
			return 2
		elif _check_for_trapezoid_centered_180_degrees_rotation(l_trapezoid_to_verify[0], l_trapezoid_to_verify[1], l_trapezoid_to_verify[2], l_trapezoid_to_verify[3]):
			return 3
		elif _check_for_trapezoid_centered_270_degrees_rotation(l_trapezoid_to_verify[0], l_trapezoid_to_verify[1], l_trapezoid_to_verify[2], l_trapezoid_to_verify[3]):
			return 4
	
	return -1



# -----------------------/////////////////--------------------------
# 			Verify shapes directly against reference shapes			
# -----------------------/////////////////--------------------------

func _check_shape(arg_shape_to_check, i):
	var l_shape_to_indentify
	var l_offset
	var l_scale = Vector2(1,1)
	var l_shape_processor = shape_processor.instance()
	
	g_identified_shape = null
	l_shape_processor.instance_shape(arg_shape_to_check)
	l_scale = ( g_shape_size_reference[i] / l_shape_processor.get_shape_size() )
	l_shape_processor.scale_shape(l_scale)
	l_shape_to_indentify = l_shape_processor.get_shape()
	l_shape_processor.queue_free()
	
	if _compare_vector2_arrays_with_value_tolerance(l_shape_to_indentify, g_shapes_reference[i]):			# If the shape was immediately found then break cycle here
		g_identified_shape = l_shape_to_indentify
		return true
	else:
		l_offset = _find_in_vector2_array_with_value_tolerance(l_shape_to_indentify, g_shapes_reference[i][0])
		
		if l_offset != null:
			l_shape_to_indentify = _shift_array_left(l_shape_to_indentify, l_offset)
			
			if _compare_vector2_arrays_with_value_tolerance(l_shape_to_indentify, g_shapes_reference[i]):
				g_identified_shape = l_shape_to_indentify
				return true
			else:
				# If still has not found then invert the array and compare again
				l_shape_to_indentify.invert()
				
				l_offset = _find_in_vector2_array_with_value_tolerance(l_shape_to_indentify, g_shapes_reference[i][0])
				if l_offset != null:
					l_shape_to_indentify = _shift_array_left(l_shape_to_indentify, l_offset)
				
				if _compare_vector2_arrays_with_value_tolerance(l_shape_to_indentify, g_shapes_reference[i]):
					g_identified_shape = l_shape_to_indentify
					return true
	return false



# -----------------------/////////////////--------------------------
# 					Private auxiliary functions						
# -----------------------/////////////////--------------------------

func _calc_angle_between_points(l_point1, l_point2):
	var length = l_point2 - l_point1
	var alpha = rad2deg( atan2( length.x, length.y ) )
	
	return _normalize_angle( _digest_angle( length, alpha ) )

func _digest_angle(l_vector, l_angle):		# Transform the relative angle between two dots into an absolute angle in relation to the cartesian axis
	var l_angle_calc
	
	if ( l_vector.x >= 0 && l_vector.y >= 0 ):		# 1º Quadrant
		l_angle_calc = 360 + l_angle
	elif ( l_vector.x < 0 && l_vector.y >= 0 ):		# 2º Quadrant
		l_angle_calc = l_angle
	elif ( l_vector.x < 0 && l_vector.y < 0 ):		# 3º Quadrant
		l_angle_calc = 360 + l_angle
	elif ( l_vector.x >= 0 && l_vector.y < 0 ):		# 4º Quadrant
		l_angle_calc = l_angle
	
	return l_angle_calc

func _normalize_angle(l_angle_to_normalize):
	if( l_angle_to_normalize < 0 ):
		while ( l_angle_to_normalize < 0 ):
			l_angle_to_normalize += 360
	elif ( l_angle_to_normalize >= 360 ):
		while ( l_angle_to_normalize >= 360 ):
			l_angle_to_normalize -= 360
	
	return l_angle_to_normalize

func _filter_shape_rotation(l_shape):
	var l_longest_distance_between_points = 0
	var l_angle_between_points
	var l_distance_aux
	var l_phasors = []
	var l_phasors_angle = []
	var l_filtered_shape = []
	
	if DEBUG:
		print("*")
		print(l_shape)
	
	# First, determine which points hold the longest range between themselves
	for i in range(l_shape.size() - 1):
		l_distance_aux = _distance_between_points(l_shape[i], l_shape[i + 1])
		if l_distance_aux > l_longest_distance_between_points:
			l_longest_distance_between_points = l_distance_aux
			l_angle_between_points = _calc_angle_between_points(l_shape[i], l_shape[i + 1])
	
	if DEBUG:
		print("**")
		print(l_longest_distance_between_points)
		print(l_angle_between_points)
	
	for i in range(l_shape.size()):
		l_phasors.append( _distance_between_points( Vector2(0,0), l_shape[i] ) )
		l_phasors_angle.append( _calc_angle_between_points( Vector2(0,0), l_shape[i] ) )
	
	if DEBUG:
		print("***")
		print(l_phasors)
		print(l_phasors_angle)
	
	for i in range(l_phasors.size()):
		l_phasors_angle[i] = -l_phasors_angle[i] + l_angle_between_points
		l_phasors[i] *= sin( l_phasors_angle[i] )
		l_filtered_shape.append( Vector2( l_phasors[i]*cos( l_phasors_angle[i] ), l_phasors[i]*sin( l_phasors_angle[i] ) ) )
	
	if DEBUG:
		print("****")
		print(l_phasors)
		print(l_phasors_angle)
		print(l_filtered_shape)
	
	return l_filtered_shape

func _distance_between_points(l_point1, l_point2):
	var l_delta_coord = l_point2 - l_point1
	return ( sqrt( l_delta_coord.x * l_delta_coord.x + l_delta_coord.y * l_delta_coord.y ) )

func _shift_array_left(l_array_to_shift, l_offset):
	var l_buffer_array = []
	var l_index_counter = l_offset
	var l_array_size = l_array_to_shift.size()
	
	for i in range(l_array_size):
		l_buffer_array.append(l_array_to_shift[l_index_counter])
		l_index_counter +=1
		
		if(l_index_counter >= l_array_size):	# If the counter is lower than the minimum array size
			l_index_counter = 0					# Then set it at the end
	
	return l_buffer_array

# Find the index on the specified array to find a specified value at. Returns -1 if couldn't have value
func _find_in_vector2_array(l_array_to_find_at, l_value_to_find):
	for i in range(l_array_to_find_at.size()):
		if ( l_array_to_find_at[i] == l_value_to_find ):
			return i
	return null

func _find_in_vector2_array_with_value_tolerance(l_array_to_find_at, l_value_to_find):
	var l_tolerance = Vector2(0,0)
	
	for i in range(l_array_to_find_at.size()):
		l_tolerance = _calc_tolerance(l_array_to_find_at[i])
		
		if ( ( l_array_to_find_at[i].x - l_tolerance.x ) <= l_value_to_find.x ) && ( ( l_array_to_find_at[i].y - l_tolerance.y ) <= l_value_to_find.y ):
			if ( l_value_to_find.x <= ( l_array_to_find_at[i].x + l_tolerance.x ) ) && ( l_value_to_find.y <= ( l_array_to_find_at[i].y + l_tolerance.y ) ) :
				return i
	return null

func _compare_vector2_arrays_with_value_tolerance(l_array1, l_array2):
	var l_tolerance
	
	if l_array1.size() == l_array2.size():
		for i in range(l_array1.size()):
			l_tolerance = _calc_tolerance(l_array2[i])
			
			# If the x value of any index...
			if ( l_array1[i].x < ( l_array2[i].x - l_tolerance.x ) ):
				return false
			
			if ( l_array1[i].y < ( l_array2[i].y - l_tolerance.y ) ):
				return false
			
			if ( ( l_array2[i].y + l_tolerance.y ) < l_array1[i].y ):
				return false
			
			# Or the y value of any index differ, then return false
			if ( ( l_array2[i].x + l_tolerance.x ) < l_array1[i].x ):
				return false
	else:
		return false
	return true

func _calc_shape_size(l_shape):
	var l_max_index_found = Vector2(-10000, -10000)
	
	if (l_shape.size() != 0):
		for i in range(l_shape.size()):
			if(l_shape[i].x > l_max_index_found.x):
				l_max_index_found.x = l_shape[i].x
			if(l_shape[i].y > l_max_index_found.y):
				l_max_index_found.y = l_shape[i].y
		
		return l_max_index_found
	else:
		return null

func _calc_tolerance(l_value):
	var l_tolerance = l_value * Vector2(0.05, 0.05)
	
	if l_tolerance.x < 0:
		l_tolerance.x *= (-1)
	if l_tolerance.y < 0:
		l_tolerance.y *= (-1)
	
	return l_tolerance

func _check_array_for_null_values(l_array):
	for i in range(l_array.size()):
		if l_array[i] == null:
			return true
	return false
