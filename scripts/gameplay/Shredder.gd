# This node is used as a "graveyard"/shredder for removed dot nodes to be tossed at
# Each node plays its destroyed animation here and is free'd off the tree

extends GridContainer

func _ready():
	set_process(false)
	add_to_group("board")

func start_shredder():
	set_process(true)

func _process(delta):
	var l_child_counter = 0
	var l_aux
	
	if get_child_count() > 0:
		while l_child_counter < get_child_count():
			if(get_child(l_child_counter) != null):
				l_aux = get_child(l_child_counter).destroying_dot()
				
				if (!l_aux):
					get_child(l_child_counter).queue_free()
					remove_child(get_child(l_child_counter))
				
			l_child_counter += 1
	else:
		set_process(false)
		get_tree().call_group(0, "controls", "no_action_on_board")
