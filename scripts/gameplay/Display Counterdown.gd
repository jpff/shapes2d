extends Node2D

const c_scale_default_shrink_value = 0.02

var g_scale = 1
var g_digit = 0

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	set_process(false)
	get_node("1").set_modulate(get_node("/root/colours").get_colour("green"))
	get_node("2").set_modulate(get_node("/root/colours").get_colour("yellow"))
	get_node("3").set_modulate(get_node("/root/colours").get_colour("red"))

func set_digit(l_digit):
	if l_digit == 1:
		get_node("1").set_hidden(false)
		get_node("2").set_hidden(true)
		get_node("3").set_hidden(true)
		
		get_node("1").set_scale(Vector2(1,1))
		
		g_scale = 1
		g_digit = 1
		
		set_hidden(false)
		set_process(true)
	elif l_digit == 2:
		get_node("1").set_hidden(true)
		get_node("2").set_hidden(false)
		get_node("3").set_hidden(true)
		
		get_node("2").set_scale(Vector2(1,1))
		
		g_scale = 1
		g_digit = 2
		
		set_hidden(false)
		set_process(true)
	elif l_digit == 3:
		get_node("1").set_hidden(true)
		get_node("2").set_hidden(true)
		get_node("3").set_hidden(false)
		
		get_node("3").set_scale(Vector2(1,1))
		
		g_scale = 1
		g_digit = 3
		
		set_hidden(false)
		set_process(true)

func _process(delta):
	g_scale -= c_scale_default_shrink_value
	
	if g_digit == 1:
		get_node("1").set_scale(Vector2(g_scale, g_scale))
	elif g_digit == 2:
		get_node("2").set_scale(Vector2(g_scale, g_scale))
	elif g_digit == 3:
		get_node("3").set_scale(Vector2(g_scale, g_scale))
	
	if g_scale <= 0.5:		# Shrink the digit no more than half of its original size
		g_digit = 0
		set_process(false)
