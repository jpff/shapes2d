extends Control

const c_font_colours = ["aqua", "black", "blue", "fuchsia", "gray", "green", "lime", "maroon", "navy", "purple", "red", "silver", "teal", "white", "yellow"]
const c_font_colours_bbcodes = ["[color=aqua]", "[color=black]", "[color=blue]", "[color=fuchsia]", "[color=gray]", "[color=green]", "[color=lime]", "[color=maroon]", "[color=navy]", "[color=purple]", "[color=red]", "[color=silver]", "[color=teal]", "[color=white]", "[color=yellow]"]

const c_clear_font = "[color=white]"
const c_dark_font = "[color=black]"

const c_minimum_shrink_scale = Vector2(2,2)
const c_max_bloat_scale = Vector2(2.5,2.5)
const c_bloat_increment = Vector2(0.07, 0.07)

var g_has_fully_bloated = false
var g_label_original_pos

var g_setting_theme = true
var g_font_colour = ""

func _ready():
	get_node("RichTextLabel").set_theme(get_node("/root/themes").asimov_theme)
	g_label_original_pos = get_pos()
	
	if (get_node("/root/global").g_theme == 0):
		g_font_colour = c_dark_font
	else:
		g_font_colour = c_clear_font
	
	set_process(true)

func set_font_colour_override(l_colour):
	var l_colour_index = c_font_colours.find(l_colour)
	
	if l_colour_index != -1:
		g_font_colour = c_font_colours_bbcodes[l_colour_index]

func clear():
	get_node("RichTextLabel").clear()

func set_text(l_text):
	get_node("RichTextLabel").clear()
	get_node("RichTextLabel").append_bbcode("[center]")
	get_node("RichTextLabel").append_bbcode(g_font_colour)
	get_node("RichTextLabel").append_bbcode(String(l_text))

func get_text():
	return get_node("RichTextLabel").get_text()

func play_scale_up_animation():
	g_has_fully_bloated = false
	set_process(true)

func _process(delta):
	if ( get_scale().x >= c_max_bloat_scale.x ) :
		g_has_fully_bloated = true
	
	if !g_has_fully_bloated:
		set_scale(get_scale() + c_bloat_increment)
	else:
		set_scale(get_scale() - c_bloat_increment)
	
	if get_scale().x <= c_minimum_shrink_scale.x:
		set_scale(c_minimum_shrink_scale)
		set_pos(g_label_original_pos)
		set_process(false)
	
	# Check for the right theme
	if g_setting_theme:
		if get_node("RichTextLabel").get_theme() != get_node("/root/themes").asimov_theme:
			get_node("RichTextLabel").set_theme(get_node("/root/themes").asimov_theme)
			set_process(true)
		else:
			g_setting_theme = false
			set_process(false)
