extends Sprite

const c_default_scale = Vector2(0.5,0.95)
const c_default_bloat_amount = Vector2(0,0.015)

onready var g_default_bloat_amount = c_default_bloat_amount * get_node("/root/viewport_data").g_viewport_scaler
onready var g_scale_final_value = Vector2(1,1) * get_node("/root/viewport_data").g_viewport_scaler

func _ready():
	set_process(false)

func setup(is_animated_spawn, l_scale_initial_value, l_scale_final_value):
	if is_animated_spawn:
		g_scale_final_value = l_scale_final_value
		set_scale(l_scale_initial_value)
		set_process(true)
	else:
		set_scale(get_node("/root/viewport_data").g_viewport_scaler * l_scale_final_value)

func _process(delta):
	if(get_scale().y < g_scale_final_value.y):
		set_scale(get_scale() + g_default_bloat_amount)
	else:
		set_process(false)
