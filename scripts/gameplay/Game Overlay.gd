extends Node2D

var score_timer = null

const c_menu_primary_button_scale = Vector2(0.35,0.35)
const c_menu_underlying_button_scale = Vector2(0.4,0.4)
const c_menubutton_boundary_overscale = Vector2(0.13, 0.13)

const c_dec_percentage = 0.01
var g_score_decrement = 1

const g_timer_slope_P0 = Vector2(1, 5)	# Level 1 -> 5 seconds before decrementing
const g_timer_slope_P1 = Vector2(100,0.08)
var g_score_line_equation_m = 0
var g_score_line_equation_b = 0

const c_seconds_before_next_decrement = 0.08
var g_seconds_before_decrementing = 1

var g_board_level = 0
var g_score_to_win = 0
var g_half_total_score

func _ready():
	add_to_group("controls")
	_calc_dec_slope()
	_config_icons()
	set_hidden(false)
	set_process(false)

func config_progressbar(l_pos):
	get_node("Progress Bars").set_pos(l_pos)

func set_board_level(l_level):
	g_board_level = l_level
	_calc_seconds_before_dec()
	_init_score_timer()

func _calc_dec_slope():
	g_score_line_equation_m = ( g_timer_slope_P1.y - g_timer_slope_P0.y ) / ( g_timer_slope_P1.x - g_timer_slope_P0.x )
	g_score_line_equation_b = g_timer_slope_P1.y - ( g_score_line_equation_m * g_timer_slope_P1.x )

func _calc_seconds_before_dec():
	var l_seconds = ( (g_score_line_equation_m * g_board_level ) + g_score_line_equation_b )
	
	if l_seconds < g_timer_slope_P1.y :
		l_seconds = g_timer_slope_P1.y
	
	g_seconds_before_decrementing = l_seconds

func set_score_colour(l_colour):
	get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_font_colour_override(l_colour)

func set_timer_colour(l_colour):
	get_node("Scaled Auxiliary").get_node("Transformed Node/Timer Value").set_font_colour_override(l_colour)

func play_timer_bloat_animation():
	get_node("Scaled Auxiliary").get_node("Transformed Node/Timer Value").play_scale_up_animation()

func set_timer_value(l_value):
	get_node("Scaled Auxiliary").get_node("Transformed Node/Timer Value").set_text(l_value)

func set_score_to_win(l_score):
	g_score_to_win = String(l_score).to_int()
	_calc_dec_amount()
	g_half_total_score = String(l_score / 2).to_int()

func found_heart():
	disable_overlay_controls()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Hearts Found").set_text(get_node("/root/global").g_hearts_found)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Hearts Found").play_scale_up_animation()
	set_process(true)

func _process(delta):
	increment_user_score(g_score_decrement)

func progress_bar_score_value_0_to_100(l_score_value):
	return ( ( l_score_value * 100 ) / g_score_to_win )

func set_scorevalue(l_value):
	var l_progress_bar_value = progress_bar_score_value_0_to_100(l_value)
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_text(l_value)
	get_node("Progress Bars/ProgressBar").set_value(l_progress_bar_value)
	get_node("Progress Bars/ProgressBar1").set_value(l_progress_bar_value)

func _calc_dec_amount():
	var l_max_score = g_score_to_win
	
	if l_max_score != null:
		g_score_decrement = String(l_max_score * c_dec_percentage).to_int()

func play_overlay_click_animation():
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").play_click_animation()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").play_click_animation()

func _config_icons():
	get_node("Scaled Auxiliary").set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	get_node("Progress Bars").set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").load_PID_for_scale_0_625()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").setup(Vector2(0,0), get_node("/root/circular_button").g_button_scale, get_node("/root/colours").dot_colours[randi() % get_node("/root/colours").dot_colours.size()], 0.00001, false)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").set_button_overlay_texture(get_node("/root/circular_button").t_gameoverlay_description, 0, get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").get_modulate().inverted(), "t_gameoverlay_description")
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").load_PID_for_scale_0_4()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").setup(Vector2(0,0), get_node("/root/circular_button").g_button_scale, Color(0,0,0,0), 0.00001, false)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").set_button_overlay_texture(get_node("/root/circular_button").t_menu_button_circle, -1, get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").get_modulate().inverted(), "t_menu_button_circle")
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").set_child_overscale(c_menubutton_boundary_overscale, "t_menu_button_circle")
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").connect("pressed", self, "_on_Open_Menu_pressed")
	
	if (get_node("/root/global").g_theme == 0):
		get_node("Scaled Auxiliary").get_node("Transformed Node/Timer Label").set_modulate(Color(0,0,0,1))
		get_node("Scaled Auxiliary").get_node("Transformed Node/Score Label").set_modulate(Color(0,0,0,1))
	else:
		get_node("Scaled Auxiliary").get_node("Transformed Node/Timer Label").set_modulate(Color(255,255,255,1))
		get_node("Scaled Auxiliary").get_node("Transformed Node/Score Label").set_modulate(Color(255,255,255,1))
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Lower Overlay Frame").set_modulate(get_node("/root/colours").get_colour("violet"))
	get_node("Scaled Auxiliary").get_node("Transformed Node/Score Label").set_modulate(get_node("/root/colours").get_colour("violet"))
	get_node("Scaled Auxiliary/Transformed Node/Heart Sprite").set_modulate(get_node("/root/colours").get_colour("red"))
	get_node("Scaled Auxiliary").get_node("Transformed Node/Timer Label").set_modulate(get_node("/root/colours").get_colour("orange"))
	
	set_score_colour("blue")
	get_node("Scaled Auxiliary/Transformed Node/Hearts Found").set_font_colour_override("red")
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Hearts Found").set_text(get_node("/root/global").g_hearts_found)
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_text(0)
	get_node("Progress Bars/ProgressBar").set_value(0)
	get_node("Progress Bars/ProgressBar1").set_value(0)

func _init_score_timer():
	score_timer = Timer.new()
	score_timer.set_one_shot(false)	# Timer will stop after the first, single trigger activation
	score_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	score_timer.set_wait_time(g_seconds_before_decrementing)		# 1 second
	score_timer.connect("timeout", self, "on_score_timeout")
	add_child(score_timer)

func on_score_timeout():
	if get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").get_text().to_int() > 0:
		var l_temp_score = get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").get_text().to_int() - g_score_decrement
		var l_progress_bar_value = progress_bar_score_value_0_to_100(l_temp_score)
		
		if l_temp_score >= 0:
			get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_text( l_temp_score )
			get_node("Progress Bars/ProgressBar").set_value(l_progress_bar_value)
			get_node("Progress Bars/ProgressBar1").set_value(l_progress_bar_value)
			score_timer.stop()
			score_timer.set_wait_time(c_seconds_before_next_decrement)
			score_timer.start()
		else:
			get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_text(0)
			get_node("Progress Bars/ProgressBar").set_value(0)
			get_node("Progress Bars/ProgressBar1").set_value(0)
			score_timer.stop()
	
	if get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").get_text().to_int() < 0:
		get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_text(0)
		get_node("Progress Bars/ProgressBar").set_value(0)
		get_node("Progress Bars/ProgressBar1").set_value(0)

func enable_overlay_controls():
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").set_ignore_mouse(false)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").set_ignore_mouse(false)
	get_node("/root/Soundboard").lower_music_volume(false)
	score_timer.start()

func disable_overlay_controls():
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").set_ignore_mouse(true)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").set_ignore_mouse(true)
	get_node("/root/Soundboard").lower_music_volume(true)
	score_timer.stop()

func _on_Open_Menu_pressed():
	get_node("/root/Soundboard").play_click()
	
	disable_overlay_controls()
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").play_click_animation()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").set_modulate(get_node("/root/colours").dot_colours[randi() % get_node("/root/colours").dot_colours.size()])
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").set_child_modulate(get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").get_modulate().inverted(), "t_gameoverlay_description")
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").play_click_animation()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/OpenMenu Underlying Button").set_child_modulate(get_node("Scaled Auxiliary").get_node("Transformed Node/Buttons/Open Menu").get_modulate().inverted(), "t_menu_button_circle")
	
	get_tree().call_group(0, "controls", "game_overlay_menu_button_pressed")

func increment_user_score(l_score):
	var l_score_incremented = get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").get_text().to_int() + String(l_score).to_int()
	var l_progress_bar_value = progress_bar_score_value_0_to_100(l_score_incremented)
	
	get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").set_text( l_score_incremented )
	get_node("Progress Bars/ProgressBar").set_value(l_progress_bar_value)
	get_node("Progress Bars/ProgressBar1").set_value(l_progress_bar_value)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").play_scale_up_animation()
	
	if get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").get_text().to_int() >= g_score_to_win:
		set_process(false)
		get_tree().call_group(0, "controls", "on_player_sublevel_cleared", g_score_to_win)
	
	score_timer.stop()
	score_timer.set_wait_time(g_seconds_before_decrementing)
	score_timer.start()

func _on_Clear_Line_pressed():
	var l_tempscore = get_node("Scaled Auxiliary").get_node("Transformed Node/Score Value").get_text().to_int()
	var l_progress_bar_value
	
	if l_tempscore > 0:
		l_tempscore -= g_half_total_score
		if l_tempscore < 0:
			l_tempscore = 0
		
		l_progress_bar_value = progress_bar_score_value_0_to_100(l_tempscore)
		get_node("Scaled Auxiliary/Transformed Node/Score Value").set_text(l_tempscore)
		get_node("Progress Bars/ProgressBar").set_value(l_progress_bar_value)
		get_node("Progress Bars/ProgressBar1").set_value(l_progress_bar_value)
		get_node("Scaled Auxiliary/Transformed Node/Score Value").play_scale_up_animation()
	
	get_node("/root/Soundboard").play_dot_hover()
	get_node("Scaled Auxiliary").get_node("Transformed Node/Clear Line").set_disabled(true)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Clear Line").set_ignore_mouse(true)
	get_tree().call_group(0, "controls", "clear_a_board_line")

func action_on_board():
	get_node("Scaled Auxiliary").get_node("Transformed Node/Clear Line").set_disabled(true)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Clear Line").set_ignore_mouse(true)

func no_action_on_board():
	get_node("Scaled Auxiliary").get_node("Transformed Node/Clear Line").set_disabled(false)
	get_node("Scaled Auxiliary").get_node("Transformed Node/Clear Line").set_ignore_mouse(false)
