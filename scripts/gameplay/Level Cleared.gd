extends Node2D

const c_completed_initial_scale = Vector2(0.4, 0.4)
const c_completed_final_scale = Vector2(1,1)

const c_timed_out_initial_scale = Vector2(0.1, 0.1)
const c_timed_out_final_scale = Vector2(0.5, 0.5)

const c_scale_increment = Vector2(0.04,0.04)
const c_scale_final_increment = Vector2(0.004,0.004)
const c_alpha_decrement = 0.005

var g_state_of_completion

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	
	set_hidden(true)
	_setup_icons()
	set_process(false)

func _setup_icons():
	var l_completed_colour = get_node("/root/colours").get_colour("green")
	var l_completed_ghastle_colour = get_node("/root/colours").get_colour("green").inverted()
	
	var l_timed_out_colour = get_node("/root/colours").get_colour("red")
	var l_timed_out_ghastle_colour = get_node("/root/colours").get_colour("red").inverted()
	
	l_completed_ghastle_colour.a = 0.4
	l_timed_out_ghastle_colour.a = 0.4
	
	get_node("Node Transformed/Background").set_modulate( get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour) )
	
	get_node("Node Transformed/Completed").set_modulate(l_completed_colour)
	get_node("Node Transformed/Completed Ghastle").set_modulate(l_completed_ghastle_colour)
	get_node("Node Transformed/Completed").set_scale(c_completed_initial_scale)
	get_node("Node Transformed/Completed Ghastle").set_scale(c_completed_initial_scale)
	
	get_node("Node Transformed/Timed Out").set_modulate(l_timed_out_colour)
	get_node("Node Transformed/Timed Out Ghastle").set_modulate(l_timed_out_ghastle_colour)
	get_node("Node Transformed/Timed Out").set_scale(c_timed_out_initial_scale)
	get_node("Node Transformed/Timed Out Ghastle").set_scale(c_timed_out_initial_scale)

# If state of completion == false then player did not complete level display timed out, else if true display completed
func display_level_completed_notification(l_state_of_completion):
	set_hidden(false)
	get_node("Node Transformed/Background").set_hidden(false)
	set_process(true)
	
	if l_state_of_completion:
		get_node("Node Transformed/Completed").set_hidden(false)
		get_node("Node Transformed/Completed Ghastle").set_hidden(false)
	else:
		get_node("Node Transformed/Timed Out").set_hidden(false)
		get_node("Node Transformed/Timed Out Ghastle").set_hidden(false)
	
	g_state_of_completion = l_state_of_completion

func _process(delta):
	var l_colour
	
	if g_state_of_completion:
		if get_node("Node Transformed/Completed").get_scale().x < c_completed_final_scale.x:
			get_node("Node Transformed/Completed").set_scale(get_node("Node Transformed/Completed").get_scale() + c_scale_increment)
			get_node("Node Transformed/Completed Ghastle").set_scale(get_node("Node Transformed/Completed Ghastle").get_scale() + c_scale_increment)
		else:
			l_colour = get_node("Node Transformed/Completed Ghastle").get_modulate()
			l_colour.a -=c_alpha_decrement
			get_node("Node Transformed/Completed Ghastle").set_scale(get_node("Node Transformed/Completed Ghastle").get_scale() + c_scale_final_increment)
			get_node("Node Transformed/Completed Ghastle").set_modulate(l_colour)
	else:
		if get_node("Node Transformed/Timed Out").get_scale().x < c_timed_out_final_scale.x:
			get_node("Node Transformed/Timed Out").set_scale(get_node("Node Transformed/Timed Out").get_scale() + c_scale_increment)
			get_node("Node Transformed/Timed Out Ghastle").set_scale(get_node("Node Transformed/Timed Out Ghastle").get_scale() + c_scale_increment)
		else:
			l_colour = get_node("Node Transformed/Timed Out Ghastle").get_modulate()
			l_colour.a -=c_alpha_decrement
			get_node("Node Transformed/Timed Out Ghastle").set_scale(get_node("Node Transformed/Timed Out Ghastle").get_scale() + c_scale_final_increment)
			get_node("Node Transformed/Timed Out Ghastle").set_modulate(l_colour)
