extends Sprite

const c_default_bloat_amount = Vector2(0.0025,0.0025)

var g_scale_final_value = Vector2(1,1)

func _ready():
	set_process(false)

func setup(is_animated_spawn, l_scale_initial_value, l_scale_final_value):
	if is_animated_spawn:
		set_scale(l_scale_final_value)
		g_scale_final_value = l_scale_final_value
		set_process(true)
	else:
		set_scale(Vector2(1,1))

func _process(delta):
	if(get_scale().x < g_scale_final_value.x):
		print("1")
		g_scale_final_value += c_default_bloat_amount
		set_scale(g_scale_final_value)
	else:
		print("2")
		set_process(false)