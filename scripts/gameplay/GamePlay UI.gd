extends Node2D

const c_max_round_minutes = 1
const c_round_max_time = ( 60 * c_max_round_minutes )

const g_score_slope_P0 = Vector2(0.3, 3)	# User finds shape within 0.3 secs of the last one, a multiplier of 5 is applied to the score of the found shape
const g_score_slope_P1 = Vector2(4,1)		# user find shape within 4 seconds or longer since the last found one, a multiplier of 1 is applied to the score of the found shape
var g_score_line_equation_m = 0
var g_score_line_equation_b = 0

var g_previous_score_time

var game_timer = null

const c_time_wait_on_victory_animation = 2.5	# Time to wait to display victory overlay and switch scene to pre-board
var victory_timer = null

var g_userlevel = 1
var g_user_found_shapes = 0
var g_user_avg_shapes_found = 0
var g_time_since_beggining_of_round_in_secs = 0

var g_player_won

func _ready():
	add_to_group("controls")
	_calc_score_slope()
	_init_victory_timer()
	
	get_tree().call_group(0, "controls", "action_on_board")
	
	get_node("Level Cleared").set_z(10)
	get_node("Board").set_z(4)
	get_node("Game Overlay").set_z(5)
	get_node("Game Pause Overlay").set_z(6)
	get_node("Game Pause Overlay").set_hidden(true)

func _calc_score_slope():
	g_score_line_equation_m = ( g_score_slope_P1.y - g_score_slope_P0.y ) / ( g_score_slope_P1.x - g_score_slope_P0.x )
	g_score_line_equation_b = g_score_slope_P1.y - ( g_score_line_equation_m * g_score_slope_P1.x )

func _calc_user_score(l_score):
	var l_time = OS.get_ticks_msec()
	var l_time_between_shapes_found = ( float(l_time) - float(g_previous_score_time) ) / 1000
	g_previous_score_time = l_time
	
	var l_points_multiplier = ( g_score_line_equation_m * l_time_between_shapes_found ) + g_score_line_equation_b
	
	if l_points_multiplier < g_score_slope_P1.y:
		l_points_multiplier = g_score_slope_P1.y
	elif l_points_multiplier > g_score_slope_P0.y:
		l_points_multiplier = g_score_slope_P0.y
	
	return ( l_points_multiplier * l_score )

func start_board_game(l_level, l_board_type):
	g_userlevel = l_level
	g_user_found_shapes = 0
	
	get_node("Game Overlay").set_hidden(false)
	get_node("Game Overlay").set_board_level(l_level)
	get_node("Board").board(g_userlevel, l_board_type)
	get_node("Board").set_scale( get_node("/root/viewport_data").c_board_scale )
	get_node("Board").set_pos( ( get_node("/root/viewport_data").g_viewport_rect_area / 2 ) - get_node("/root/viewport_data").g_board_center_scaled )
	get_node("Game Overlay").set_score_to_win( _calc_user_score_for_next_level( g_userlevel ) )
	get_node("Game Overlay").set_timer_colour("green")
	set_timer_on_game_overlay(c_round_max_time)
	
	if get_node("/root/global").g_toggle_timer:
		_init_game_timer()
	else:
		get_node("Game Overlay").set_timer_value("Disabled")
	
	g_previous_score_time = OS.get_ticks_msec()
	
	if(get_node("/root/global").g_toggle_music):
		get_node("/root/Soundboard").lower_music_volume(false)

func retry_continue():
	get_node("Board").set_hidden(true)
	get_node("/root/global").board_retry()

func save_score():
	get_node("/root/global").save_user_score(g_userlevel)

func _init_game_timer():
	game_timer = Timer.new()
	game_timer.set_one_shot(false)	# Timer will stop after the first, single trigger activation
	game_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	game_timer.set_wait_time(1)		# 1 second
	game_timer.connect("timeout", self, "on_game_timer_timeout")
	add_child(game_timer)

func on_game_timer_timeout():
	g_time_since_beggining_of_round_in_secs += 1
	
	# Check if the round has ended
	if g_time_since_beggining_of_round_in_secs >= c_round_max_time:
		get_node("/root/Soundboard").play_beep()
		if get_node("/root/global").g_toggle_music:
			get_node("/root/Soundboard").lower_music_volume(true)
		game_timer_ran_out()
	
	var l_time_left = c_round_max_time - g_time_since_beggining_of_round_in_secs
	set_timer_on_game_overlay(l_time_left)
	
	if l_time_left <= 30 && l_time_left > 10:
		get_node("Game Overlay").set_timer_colour("yellow")
	elif l_time_left <= 10:
		get_node("Game Overlay").set_timer_colour("red")
	
	if l_time_left == 60 || l_time_left == 30 || l_time_left == 10 || l_time_left == 5 || l_time_left == 3 || l_time_left == 2 || l_time_left == 1:
		get_node("/root/Soundboard").play_beep()

func enable_game_overlay_timer():
	if get_node("/root/global").g_toggle_timer:
		game_timer.start()

func disable_game_overlay_timer():
	if get_node("/root/global").g_toggle_timer:
		game_timer.stop()

func set_timer_on_game_overlay(l_value):
	var seconds = l_value % 60
	var minutes = l_value % 3600 / 60
	var str_elapsed = "%02d : %02d" % [minutes, seconds]
	get_node("Game Overlay").set_timer_value(str_elapsed)

func game_overlay_menu_button_pressed():
	if get_node("/root/global").g_toggle_timer:
		game_timer.stop()
	
	get_tree().call_group(0, "controls", "disable_board")
	get_node("Game Pause Overlay").set_hidden(false)
	get_node("Game Pause Overlay").play_enter_animation()

func game_overlay_menu_closed_to_return_to_gameplay():
	if get_node("/root/global").g_toggle_timer:
		game_timer.start()
	
	get_tree().call_group(0, "controls", "enable_board")
	get_node("Game Overlay").enable_overlay_controls()
	get_node("Game Pause Overlay").set_hidden(true)

func close_board():
	if get_node("/root/global").g_toggle_timer:
		game_timer.stop()
	
	get_node("Board").set_hidden(true)
	get_node("/root/global").board_lost()

func game_timer_ran_out():
	if get_node("/root/global").g_toggle_timer:
		game_timer.stop()
	
	if c_max_round_minutes != 0:
		g_user_avg_shapes_found = g_user_found_shapes / c_max_round_minutes
	
	get_tree().call_group(0, "controls", "disable_board")
	get_node("Game Pause Overlay").set_hidden(true)
	get_node("Game Overlay").disable_overlay_controls()
	get_node("Level Cleared").display_level_completed_notification(false)
	
	g_player_won = false
	
	game_timer.stop()
	victory_timer.start()

func _init_victory_timer():
	victory_timer = Timer.new()
	victory_timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	victory_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	victory_timer.set_wait_time(c_time_wait_on_victory_animation)		# 1 second
	victory_timer.connect("timeout", self, "on_victory_timer_timeout")
	add_child(victory_timer)

func on_victory_timer_timeout():
	victory_timer.stop()
	if g_player_won:
		get_node("/root/global").board_won()
	else:
		get_node("/root/global").board_lost()

func on_player_sublevel_cleared(l_score_offset):
	get_node("/root/Soundboard").play_levelup()
	
	g_userlevel += 1
	get_node("Board").set_current_userlevel( g_userlevel )
	get_node("Game Overlay").set_scorevalue( l_score_offset )
	get_node("Game Overlay").set_score_to_win( _calc_user_score_for_next_level( g_userlevel ) )
	
	get_node("Game Overlay").disable_overlay_controls()
	get_node("Level Cleared").display_level_completed_notification(true)
	g_player_won = true
	game_timer.stop()
	victory_timer.start()

func user_found_heart():
	get_node("Game Overlay").found_heart()

func add_user_score(l_found_shape):
	g_user_found_shapes += 1
	var l_user_score = 0
	var l_distance_between_coords
	var l_distance = 0
	
	for i in range(l_found_shape.size() - 1):
		l_distance_between_coords = l_found_shape[i] - l_found_shape[i + 1]
		l_distance = sqrt( ( l_distance_between_coords.x * l_distance_between_coords.x ) + ( l_distance_between_coords.y * l_distance_between_coords.y ) )
		l_user_score += ( l_distance * 100 )
	
	get_tree().call_group(0, "controls", "increment_user_score", _calc_user_score(l_user_score))

func _calc_user_score_for_next_level(l_level):
	return ( String(sqrt(120 * l_level) * 150 ).to_int() + 3200 )		# Will cast the float to an integer
