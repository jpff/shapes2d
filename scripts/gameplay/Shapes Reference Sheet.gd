# Shapes reference is an auxiliary node that is part of Shapes Engine.
# Constains all the valid shape references that are to be compared against the user's input.

extends Node2D

const DEBUG = false

# Types of shapes are defined by their vertices herein, to be later transformed into a Matrix32 array
const c_triangle1 = [Vector2(1,0), Vector2(2,2), Vector2(0,2)]
const c_triangle2 = [Vector2(0,0), Vector2(2,0), Vector2(1,2)]
const c_triangle3 = [Vector2(0,1), Vector2(2,0), Vector2(2,2)]
const c_triangle4 = [Vector2(0,0), Vector2(2,1), Vector2(0,2)]
const c_triangle = [c_triangle1, c_triangle2, c_triangle3, c_triangle4]
const c_triangle_reference = {name = "triangle", shape = c_triangle}

const c_triangle_scalene1 = [Vector2(0,0), Vector2(0,1), Vector2(1,1)]
const c_triangle_scalene2 = [Vector2(1,0), Vector2(1,1), Vector2(0,1)]
const c_triangle_scalene3 = [Vector2(0,0), Vector2(1,0), Vector2(1,1)]
const c_triangle_scalene4 = [Vector2(0,0), Vector2(1,0), Vector2(0,1)]
const c_triangle_scalene = [c_triangle_scalene1, c_triangle_scalene2, c_triangle_scalene3, c_triangle_scalene4]
const c_triangle_scalene_reference = {name = "scalene", shape = c_triangle_scalene}

const c_square = [Vector2(0,0), Vector2(1,0), Vector2(1,1), Vector2(0,1)]
const c_square_reference = {name = "square", shape = c_square}

const c_parallelogram1 = [Vector2(1,0), Vector2(2,0), Vector2(1,1), Vector2(0,1)]
const c_parallelogram2 = [Vector2(0,0), Vector2(1,1), Vector2(1,2), Vector2(0,1)]
const c_parallelogram3 = [Vector2(0,0), Vector2(1,0), Vector2(2,1), Vector2(1,1)]
const c_parallelogram4 = [Vector2(0,1), Vector2(1,0), Vector2(1,1), Vector2(0,2)]
const c_parallelogram = [c_parallelogram1, c_parallelogram2, c_parallelogram3, c_parallelogram4]
const c_parallelogram_reference = {name = "parallelogram", shape = c_parallelogram}

const c_diamond = [Vector2(1,0), Vector2(2,1), Vector2(1,2), Vector2(0,1)]
const c_diamond_reference = {name = "diamond", shape = c_diamond}

const c_trapezium1 = [Vector2(0,1), Vector2(1,0), Vector2(2,0), Vector2(3,1)]
const c_trapezium2 = [Vector2(0,1), Vector2(1,0), Vector2(1,3), Vector2(0,2)]
const c_trapezium3 = [Vector2(0,0), Vector2(3,0), Vector2(2,1), Vector2(1,1)]
const c_trapezium4 = [Vector2(0,0), Vector2(1,1), Vector2(1,2), Vector2(0,3)]
const c_trapezium = [c_trapezium1, c_trapezium2, c_trapezium3, c_trapezium4]
const c_trapezium_reference = {name = "trapezium", shape = c_trapezium}

const c_diagonal_trapezium1 = [Vector2(0,0), Vector2(1,0), Vector2(2,1), Vector2(2,2)]
const c_diagonal_trapezium2 = [Vector2(1,0), Vector2(2,0), Vector2(0,2), Vector2(0,1)]
const c_diagonal_trapezium3 = [Vector2(0,0), Vector2(2,2), Vector2(1,2), Vector2(0,1)]
const c_diagonal_trapezium4 = [Vector2(2,0), Vector2(2,1), Vector2(1,2), Vector2(0,2)]
const c_diagonal_trapezium = [c_diagonal_trapezium1, c_diagonal_trapezium2, c_diagonal_trapezium3, c_diagonal_trapezium4]
const c_diagonal_trapezium_reference = {name = "diagonal trapezium", shape = c_diagonal_trapezium}

const c_hedron1 = [Vector2(1,0), Vector2(2,1), Vector2(2,2), Vector2(1,3), Vector2(0,2), Vector2(0,1)]
const c_hedron2 = [Vector2(0,1), Vector2(1,0), Vector2(2,0), Vector2(3,1), Vector2(2,2), Vector2(1,2)]
const c_hedron = [c_hedron1, c_hedron2]
const c_hedron_reference = {name = "hedron", shape = c_hedron}

const c_octagon = [Vector2(1,0), Vector2(2,0), Vector2(3,1), Vector2(3,2), Vector2(2,3), Vector2(1,3), Vector2(0,2), Vector2(0,1)]
const c_octagon_reference = {name = "octagon", shape = c_octagon}

const c_heart1 = [Vector2(1,0), Vector2(2,1), Vector2(3,0), Vector2(4,1), Vector2(4,2), Vector2(2,4), Vector2(0,2), Vector2(0,1)]
const c_heart2 = [Vector2(1,4), Vector2(2,3), Vector2(3,4), Vector2(4,3), Vector2(4,2), Vector2(2,0), Vector2(0,2), Vector2(0,3)]
const c_heart3 = [Vector2(3,4), Vector2(2,4), Vector2(0,2), Vector2(2,0), Vector2(3,0), Vector2(4,1), Vector2(3,2), Vector2(4,3)]
const c_heart4 = [Vector2(1,0), Vector2(2,0), Vector2(4,2), Vector2(2,4), Vector2(1,4), Vector2(0,3), Vector2(1,2), Vector2(0,1)]
const c_heart = [c_heart1, c_heart2, c_heart3, c_heart4]
const c_heart_reference = {name = "heart", shape = c_heart}

const c_shape_array = [c_triangle_reference, c_triangle_scalene_reference, c_square_reference, c_parallelogram_reference, c_diamond_reference, c_trapezium_reference, c_diagonal_trapezium_reference, c_hedron_reference, c_octagon_reference, c_heart_reference]

# Global variables
var g_shapes = []			# Holds the shape output format for a single shape

# Global variables for view/debugging
var g_shape_debug = []		# Holds the debugging shapes format for user viewing
var g_shape_index = 0
const c_resolution_scaler = 100

var g_debouncer = false

func _ready():
	set_hidden(true)
	set_process(false)
	setup_references()
	
	if DEBUG:
		_view_shapes()

func get_shapes():
	return g_shapes

func setup_references():
	var l_shape_offset
	
	g_shapes = c_shape_array
	
	for i in range(c_shape_array.size()):
		if typeof(c_shape_array[i].shape[0]) == TYPE_ARRAY:
			for j in range(c_shape_array[i].shape.size()):
				l_shape_offset = calc_shape_offset(c_shape_array[i].shape[j])
				
				for index in range(c_shape_array[i].shape[j].size()):
					g_shapes[i].shape[j][index] -= l_shape_offset
		else:
			l_shape_offset = calc_shape_offset(c_shape_array[i].shape)
			
			for index in range(c_shape_array[i].shape.size()):
				g_shapes[i].shape[index] -= l_shape_offset

func calc_shape_offset(l_shape):
	var l_shape_epicentre_offset = Vector2(0,0)
	
	for i in range(l_shape.size()):
		l_shape_epicentre_offset += l_shape[i]
	
	l_shape_epicentre_offset /= l_shape.size()
	
	return l_shape_epicentre_offset



# -----------------------/////////////////--------------------------
# 					For debugging purposes only					
# -----------------------/////////////////--------------------------

func _view_shapes():
	set_hidden(false)
	setup_debug()		# Take the value of the first shape index
	set_process(true)

func setup_debug():
	for i in range(c_shape_array.size()):
		if typeof(c_shape_array[i].shape[0]) == TYPE_ARRAY:
			for j in range(c_shape_array[i].shape.size()):
				g_shape_debug.append(c_shape_array[i].shape[j])
		else:
			g_shape_debug.append(c_shape_array[i].shape)

func _process(delta):
	if Input.is_action_pressed("BUTTON_LEFT"):
		if g_debouncer == false:
			g_debouncer = true
			
			g_shape_index += 1
			if g_shape_index == g_shape_debug.size():
				g_shape_index = 0
	else:
		g_debouncer = false
	
	update()

func _draw():
	# Draw interconnecting lines between all the vertices in the shape
	if ( g_shape_debug.size() != 0 ):
		for i in range(g_shape_debug[g_shape_index].size() - 1):
			draw_line( ( g_shape_debug[g_shape_index][i] * c_resolution_scaler ), ( g_shape_debug[g_shape_index][i + 1] * c_resolution_scaler ), Color(255,255,255,1), 1)
		draw_line( ( g_shape_debug[g_shape_index][g_shape_debug[g_shape_index].size() - 1] * c_resolution_scaler ), ( g_shape_debug[g_shape_index][0] * c_resolution_scaler ), Color(255,255,255,1), 1)
