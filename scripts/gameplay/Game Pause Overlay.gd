extends Node2D

var timer = null
var g_button_timeout = 0
var g_option = 0
var ReturnButton = null
var SaveAndExitButton = null

func _ready():
	add_to_group("controls")
	_configure_icons()
	_init_timer()

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

# Used to calculate the timeout for the button spawn delay timers
func _get_button_timeout_value():
	g_button_timeout += get_node("/root/global").menu_button_timeout_secs
	return g_button_timeout

func _configure_icons():
	var l_colour_aux
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	
	if(!get_node("/root/global").g_theme):
		get_node("Static Menu Icons/Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Static Menu Icons/Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Static Menu Icons/Background").set_hidden(false)
	
	get_node("Static Menu Icons/Game Paused Header").set_modulate(get_node("/root/colours").get_colour("orange"))
	l_colour_aux = get_node("Static Menu Icons/Game Paused Header").get_modulate().inverted()
	l_colour_aux.a = 0.2
	get_node("Static Menu Icons/Header Background Filling").set_modulate(l_colour_aux)
	get_node("Static Menu Icons/Header Background Boundary").set_modulate(get_node("Static Menu Icons/Game Paused Header").get_modulate())

func on_timeout():
	get_node("/root/admob_global").hide_banner()
	
	if g_option == 1:
		timer.stop()
		get_tree().call_group(0, "controls", "game_overlay_menu_closed_to_return_to_gameplay")
	elif g_option == 2:
		ReturnButton.queue_free()
		SaveAndExitButton.queue_free()
		get_tree().call_group(0, "controls", "close_board")
	
	g_option = 0

func _on_Return_Button_pressed():
	get_node("/root/Soundboard").play_click()
	
	ReturnButton.play_click_animation()
	ReturnButton.invert_colours()
	
	g_option = 1
	timer.start()

func _on_Save__Exit_Button_pressed():
	get_node("/root/Soundboard").play_click()
	
	SaveAndExitButton.play_click_animation()
	SaveAndExitButton.invert_colours()
	
	g_option = 2
	timer.start()

func play_enter_animation():
	g_button_timeout = 0
	
	if ReturnButton != null:
		ReturnButton.queue_free()
	if SaveAndExitButton != null:
		SaveAndExitButton.queue_free()
	
	ReturnButton = get_node("/root/circular_button").menu_button.instance()
	SaveAndExitButton = get_node("/root/circular_button").menu_button.instance()
	
	ReturnButton.load_PID_for_scale_0_625()
	ReturnButton.setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("violet"), _get_button_timeout_value(), true)
	ReturnButton.set_button_overlay_texture(get_node("/root/circular_button").t_pausemenu_return_description, 0, get_node("/root/colours").get_colour("violet").inverted(), "t_pausemenu_return_description")
	
	SaveAndExitButton.load_PID_for_scale_0_625()
	SaveAndExitButton.setup(get_node("/root/viewport_data").g_dot_pos[6], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("blue"), _get_button_timeout_value(), true)
	SaveAndExitButton.set_button_overlay_texture(get_node("/root/circular_button").t_pausemenu_save_and_exit_description, 0, get_node("/root/colours").get_colour("blue").inverted(), "t_pausemenu_save_and_exit_description")
	
	ReturnButton.connect("pressed", self, "_on_Return_Button_pressed")
	SaveAndExitButton.connect("pressed", self, "_on_Save__Exit_Button_pressed")
	
	add_child(ReturnButton)
	add_child(SaveAndExitButton)
	
	get_node("/root/admob_global").display_banner()
