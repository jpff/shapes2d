extends Node2D

var selection_trigger_timer = null
var g_selection_timer_is_active = false
const c_selection_polling_speed = 0.06	# Lower is more sensitive
const c_trigger_threshold = 1			# If a dot is verified to be hovered for more than n interations, set it as valid user selection
var g_threshold_counter

var g_play_dot_hover_sound_latch	# A play sound latch so that only 

# Global array to host generated dot info
var g_dots = []
var g_dot_radius

# Global cvar
var g_is_drawing_line				# Retains state to draw line or not
var g_mouse_target					# Current active cursor target
var g_current_dot_under_analysis	# Current dot under analysis
var g_first_dot						# First dot off which a line started being drawn off
var g_draw_line_colour				# Colour of the drawn line
var g_line_width					# Width of the line being drawn

var g_dot_casper_indexes		# Array that holds dot indexes off which a current line is being drawn off
var g_dot_casper_pos	# Array that holds dot positions off which a current line is being drawn off

# Control vars 
var g_dot_is_hovering
var g_mouse_button_is_being_pressed
var g_current_target_colour
var g_dot_under_analysis_acception_trigger

# For delta_theta calculation
const delta_theta_threshold = 20	# Angle threshold to spawn a new line

onready var g_board_offset = Vector2(0,0)

func _ready():
	set_process(false)
	set_z(0)
	add_to_group("board")
	set_hidden(true)
	
	_init_selection_trigger_timer()
	
	g_play_dot_hover_sound_latch = false
	
	g_dot_casper_indexes = []
	g_dot_casper_pos = []
	
	g_is_drawing_line = false
	g_mouse_target = null
	g_first_dot = null
	g_current_dot_under_analysis = null
	g_draw_line_colour = Color(0,0,0,1)
	g_line_width = 0
	g_dot_under_analysis_acception_trigger = false
	g_threshold_counter = 0
	
	g_board_offset = ( get_node("/root/viewport_data").g_viewport_rect_area / 2 ) - get_node("/root/viewport_data").g_board_center_scaled
	g_dots.clear()

func _init_selection_trigger_timer():
	selection_trigger_timer = Timer.new()
	selection_trigger_timer.set_one_shot(false)	# Timer will stop after the first, single trigger activation
	selection_trigger_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	selection_trigger_timer.set_wait_time( c_selection_polling_speed )
	selection_trigger_timer.connect("timeout", self, "on_selection_trigger_timeout")
	add_child( selection_trigger_timer )

func _set_timer_active(l_state):
	if l_state:
		selection_trigger_timer.start()
		g_selection_timer_is_active = true
	else:
		selection_trigger_timer.stop()
		g_selection_timer_is_active = false

func _process(delta):
	_line_canvas_process()
	update()

func clear_canvas_line():
	g_is_drawing_line = false
	
	g_dot_casper_indexes = []
	g_dot_casper_pos = []
	update()
	
	g_is_drawing_line = true

# Configures the node with all the necessary information
func init(l_dot_array, l_dot_radius):
	set_hidden(false)
	
	g_dots = l_dot_array
	g_dot_radius = l_dot_radius
	g_line_width = ( ( g_dot_radius.x * 4 ) / 3 )
	g_line_width /= 2

# Updates the data on the node
func update_data(l_dot_array):
	g_is_drawing_line = false
	
	g_dots = l_dot_array
	_clear_casper_array()
	
	update()		# Update the screen once to clear all lines
	
	g_is_drawing_line = true



# -------------------------/////////////---------------------------
#						Auxialiary Functions						
# -------------------------/////////////---------------------------

func _clear_casper_array():
	g_dot_casper_indexes = []
	g_dot_casper_pos = []

func _search_casper_array_index(l_index):
	for i in range(g_dot_casper_indexes.size()):
		if g_dot_casper_indexes[i] == l_index:
			return true
	return false

func _search_casper_array_pos(l_pos):
	for i in range(g_dot_casper_pos.size()):
		if g_dot_casper_pos[i] == l_pos:
			return true
	return false

func _remove_casper_null_values():
	for i in range(g_dot_casper_pos.size()):
		if g_dot_casper_pos[i] == null:
			g_dot_casper_pos.remove(i)
			g_dot_casper_indexes.remove(i)

func _clear_casper_duplicates():
	var l_index = 0
	var l_index_aux = 0
	
	if ( g_dot_casper_indexes.size() >= 2 ):
		while ( l_index < g_dot_casper_indexes.size() ):
			l_index_aux = l_index + 1
			
			if l_index_aux >= g_dot_casper_indexes.size():
				break
			
			while( l_index_aux < g_dot_casper_indexes.size() ):
				if g_dot_casper_indexes[l_index] == g_dot_casper_indexes[l_index_aux]:
					g_dot_casper_indexes.remove(l_index_aux)
					g_dot_casper_pos.remove(l_index_aux)
				l_index_aux += 1
			l_index += 1

func _get_dot_colour(l_dot_pos):
	for l_no_of_dots_counter in range(g_dots.size()):
		if g_dots[l_no_of_dots_counter].dot_pos == l_dot_pos:
			return g_dots[l_no_of_dots_counter].dot_colour
	return null

func _search_dot_index_from_pos(l_dot_pos):
	for l_no_of_dots_counter in range(g_dots.size()):
		if g_dots[l_no_of_dots_counter].dot_pos == l_dot_pos:
			return g_dots[l_no_of_dots_counter].dot_index
	return null

func _get_dot_pos_from_index(l_dot_index):
	for l_no_of_dots_counter in range(g_dots.size()):
		if g_dots[l_no_of_dots_counter].dot_index == l_dot_index:
			return g_dots[l_no_of_dots_counter].dot_pos
	return null

# Sets the target for the cursor to draw towards at, return true if drawing towards a dot and false if drawing towards the real cursor pos
func _cursor_target():
	var l_distance
	var l_cursor_pos = self.get_local_mouse_pos()
	
	for l_no_of_dots_counter in range(g_dots.size()):
		l_distance = sqrt( (l_cursor_pos.x - g_dots[l_no_of_dots_counter].dot_pos.x) * (l_cursor_pos.x - g_dots[l_no_of_dots_counter].dot_pos.x) + (l_cursor_pos.y - g_dots[l_no_of_dots_counter].dot_pos.y) * (l_cursor_pos.y - g_dots[l_no_of_dots_counter].dot_pos.y) )
		
		if(l_distance <= g_dot_radius.x):
			g_mouse_target = g_dots[l_no_of_dots_counter].dot_pos
			return true
	g_mouse_target = l_cursor_pos
	return false



# -------------------------/////////////---------------------------
#				Auxiliary Line Functions related					
# -------------------------/////////////---------------------------

func _calc_poly_line(l_line_start, l_line_end):
	var l_points = Vector2Array()
	var l_angle = _calc_angle_between_points(l_line_start, l_line_end)
	
	l_points.append( _process_poly_vertex_with_angle(l_line_start, l_angle, -g_line_width ) )
	l_points.append( _process_poly_vertex_with_angle(l_line_start, l_angle, g_line_width ) )
	l_points.append( _process_poly_vertex_with_angle(l_line_end, l_angle, g_line_width ) )
	l_points.append( _process_poly_vertex_with_angle(l_line_end, l_angle, -g_line_width ) )
	
	return l_points

func _process_poly_vertex_with_angle(point, angle, length):
	return Vector2( point.x + ( length * cos(angle) ), point.y + ( length * sin(angle) ) )

func _calc_angle_between_points(l_point1, l_point2):
	var length = l_point2 - l_point1
	var alpha = rad2deg( atan2( length.x, length.y ) )
	
	return _filter_angle( _normalize_angle( _digest_angle( length, alpha ) ) )

func _filter_angle(l_angle):
	return ( l_angle / ( (-1)*(14.5 * 4) ) )

func _digest_angle(l_vector, l_angle):		# Transform the relative angle between two dots into an absolute angle in relation to the cartesian axis
	var l_angle_calc
	
	if ( l_vector.x >= 0 && l_vector.y >= 0 ):		# 1º Quadrant
		l_angle_calc = 360 + l_angle
	elif ( l_vector.x < 0 && l_vector.y >= 0 ):		# 2º Quadrant
		l_angle_calc = l_angle
	elif ( l_vector.x < 0 && l_vector.y < 0 ):		# 3º Quadrant
		l_angle_calc = 360 + l_angle
	elif ( l_vector.x >= 0 && l_vector.y < 0 ):		# 4º Quadrant
		l_angle_calc = l_angle
	
	return l_angle_calc

func _normalize_angle(l_angle_to_normalize):
	var l_normalized_angle = l_angle_to_normalize
	
	if( l_normalized_angle < 0 ):
		while ( l_normalized_angle < 0 ):
			l_normalized_angle += 360
	elif ( l_normalized_angle >= 360 ):
		while ( l_normalized_angle >= 360 ):
			l_normalized_angle -= 360
	
	return l_normalized_angle



# -------------------------/////////////---------------------------
#							Line related							
# -------------------------/////////////---------------------------

func _line_canvas_process():
	g_dot_is_hovering = _cursor_target()
	g_mouse_button_is_being_pressed = Input.is_mouse_button_pressed( BUTTON_LEFT )
	g_current_target_colour = _get_dot_colour( g_mouse_target )
	
	# Check if a dot's being hovered and bloat it if so, set to unbloat otherwise
	if (g_dot_is_hovering):
		get_tree().call_group(0, "board", "bloat_dot", _search_dot_index_from_pos( g_mouse_target ))
		if(!g_play_dot_hover_sound_latch):
			get_node("/root/Soundboard").play_dot_hover()
			g_play_dot_hover_sound_latch = true
	else:
		get_tree().call_group(0, "board", "unbloat_dots")
		g_play_dot_hover_sound_latch = false
	
	# Process dot selection
	if g_mouse_button_is_being_pressed:
		if g_dot_is_hovering:
			if !g_is_drawing_line:
				g_first_dot = g_mouse_target
				g_draw_line_colour = _get_dot_colour(g_mouse_target)
				g_is_drawing_line = true
				g_dot_casper_pos.append( g_mouse_target )
				g_dot_casper_indexes.append( _search_dot_index_from_pos( g_mouse_target ) )
			else:
				if g_current_target_colour == g_draw_line_colour:
					if g_mouse_target != g_first_dot:
						if g_current_dot_under_analysis == null:
							g_current_dot_under_analysis = g_mouse_target
							g_dot_under_analysis_acception_trigger = false
							g_threshold_counter = 0
							_set_timer_active(true)
						
						if g_dot_under_analysis_acception_trigger:
							g_dot_casper_pos.append( g_current_dot_under_analysis )
							g_dot_casper_indexes.append( _search_dot_index_from_pos( g_current_dot_under_analysis ) )
							get_tree().call_group(0, "board", "_generate_user_dot_selection_reference", g_dot_casper_pos[g_dot_casper_pos.size() - 1], g_dot_casper_indexes[g_dot_casper_indexes.size() - 1], g_draw_line_colour)
							g_dot_under_analysis_acception_trigger = false
							g_threshold_counter = 0
					else:
						g_dot_under_analysis_acception_trigger = false
						g_threshold_counter = 0
						_set_timer_active(false)
						_clear_casper_duplicates()
						_remove_casper_null_values()
						get_tree().call_group(0, "board", "user_shape_selection", g_dot_casper_indexes)
		else:	# If no dot is currently being hovered
			_set_timer_active(false)
			g_current_dot_under_analysis = null
			g_dot_under_analysis_acception_trigger = false
			g_threshold_counter = 0
	else:	# If user stopped clicking
		g_mouse_target = null
		g_first_dot = null
		g_is_drawing_line = false
		g_current_dot_under_analysis = null
		g_dot_under_analysis_acception_trigger = false
		g_threshold_counter = 0
		_set_timer_active(false)
		
		if(g_dot_casper_pos.size() != 0):
			_clear_casper_array()

func on_selection_trigger_timeout():
	if g_mouse_target == g_current_dot_under_analysis:
		g_threshold_counter += 1
	
	if g_threshold_counter >= c_trigger_threshold:
		g_dot_under_analysis_acception_trigger = true
		_set_timer_active(false)



# -------------------------/////////////---------------------------
# 						Process draw function						
# -------------------------/////////////---------------------------

func _draw():
	if g_is_drawing_line && g_dot_casper_pos.size() > 0:
		_clear_casper_duplicates()
		_remove_casper_null_values()
		
		if g_dot_casper_pos.size() > 1:
			for l_dot_casper_counter in range(g_dot_casper_indexes.size() - 1):
				draw_colored_polygon( _calc_poly_line( g_dot_casper_pos[l_dot_casper_counter], g_dot_casper_pos[l_dot_casper_counter + 1] ), g_draw_line_colour )
		
		if ( g_mouse_target != g_dot_casper_pos[g_dot_casper_pos.size() - 1] ):
			draw_circle( g_mouse_target, g_line_width, g_draw_line_colour )
			draw_colored_polygon( _calc_poly_line( g_dot_casper_pos[g_dot_casper_pos.size() - 1], g_mouse_target ), g_draw_line_colour )
	else:
		pass		# Clear the screen
