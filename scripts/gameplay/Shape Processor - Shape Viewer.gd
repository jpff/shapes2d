extends Popup

var g_shape_debug = []		# Holds the debugging shapes format for user viewing
const c_resolution_scaler = 100
var g_shape_index = 0
var is_clearing = false

func _ready():
	set_process(false)
	popup_centered_minsize( get_node("/root/viewport_data").c_default_window_size )

func set_shape(l_shape):
	is_clearing = true
	update()
	set_process(false)
	is_clearing = false
	g_shape_debug = []
	g_shape_debug = l_shape
	set_process(false)

func show_shape_window():
	set_hidden(false)
	popup()
	show()

func _draw():
	if !is_clearing:
		# Draw interconnecting lines between all the vertices in the shape
		if ( g_shape_debug.size() != 0 ):
			for i in range(g_shape_debug[g_shape_index].size() - 1):
				draw_line( ( g_shape_debug[g_shape_index][i] * c_resolution_scaler ), ( g_shape_debug[g_shape_index][i + 1] * c_resolution_scaler ), Color(255,255,255,1), 1)
			draw_line( ( g_shape_debug[g_shape_index][g_shape_debug[g_shape_index].size() - 1] * c_resolution_scaler ), ( g_shape_debug[g_shape_index][0] * c_resolution_scaler ), Color(255,255,255,1), 1)
		else:
			pass
