extends Node2D

onready var DEBUG

const c_default_board_dimension = [4,5,6,7,8,9]

const c_grid_index_reference_4x4 = [[Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4), Vector2(0, 5), Vector2(0, 6), Vector2(0, 7)], [Vector2(1, 0), Vector2(1, 1), Vector2(1, 2), Vector2(1, 3), Vector2(1, 4), Vector2(1, 5), Vector2(1, 6), Vector2(1, 7)], [Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(2, 3), Vector2(2, 4), Vector2(2, 5), Vector2(2, 6), Vector2(2, 7)], [Vector2(3, 0), Vector2(3, 1), Vector2(3, 2), Vector2(3, 3), Vector2(3, 4), Vector2(3, 5), Vector2(3, 6), Vector2(3, 7)]]
const c_grid_index_reference_5x5 = [[Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4), Vector2(0, 5), Vector2(0, 6), Vector2(0, 7), Vector2(0, 8), Vector2(0, 9)], [Vector2(1, 0), Vector2(1, 1), Vector2(1, 2), Vector2(1, 3), Vector2(1, 4), Vector2(1, 5), Vector2(1, 6), Vector2(1, 7), Vector2(1, 8), Vector2(1, 9)], [Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(2, 3), Vector2(2, 4), Vector2(2, 5), Vector2(2, 6), Vector2(2, 7), Vector2(2, 8), Vector2(2, 9)], [Vector2(3, 0), Vector2(3, 1), Vector2(3, 2), Vector2(3, 3), Vector2(3, 4), Vector2(3, 5), Vector2(3, 6), Vector2(3, 7), Vector2(3, 8), Vector2(3, 9)], [Vector2(4, 0), Vector2(4, 1), Vector2(4, 2), Vector2(4, 3), Vector2(4, 4), Vector2(4, 5), Vector2(4, 6), Vector2(4, 7), Vector2(4, 8), Vector2(4, 9)]]
const c_grid_index_reference_6x6 = [[Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4), Vector2(0, 5), Vector2(0, 6), Vector2(0, 7), Vector2(0, 8), Vector2(0, 9), Vector2(0, 10), Vector2(0, 11)], [Vector2(1, 0), Vector2(1, 1), Vector2(1, 2), Vector2(1, 3), Vector2(1, 4), Vector2(1, 5), Vector2(1, 6), Vector2(1, 7), Vector2(1, 8), Vector2(1, 9), Vector2(1, 10), Vector2(1, 11)], [Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(2, 3), Vector2(2, 4), Vector2(2, 5), Vector2(2, 6), Vector2(2, 7), Vector2(2, 8), Vector2(2, 9), Vector2(2, 10), Vector2(2, 11)], [Vector2(3, 0), Vector2(3, 1), Vector2(3, 2), Vector2(3, 3), Vector2(3, 4), Vector2(3, 5), Vector2(3, 6), Vector2(3, 7), Vector2(3, 8), Vector2(3, 9), Vector2(3, 10), Vector2(3, 11)], [Vector2(4, 0), Vector2(4, 1), Vector2(4, 2), Vector2(4, 3), Vector2(4, 4), Vector2(4, 5), Vector2(4, 6), Vector2(4, 7), Vector2(4, 8), Vector2(4, 9), Vector2(4, 10), Vector2(4, 11)], [Vector2(5, 0), Vector2(5, 1), Vector2(5, 2), Vector2(5, 3), Vector2(5, 4), Vector2(5, 5), Vector2(5, 6), Vector2(5, 7), Vector2(5, 8), Vector2(5, 9), Vector2(5, 10), Vector2(5, 11)]]
const c_grid_index_reference_7x7 = [[Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4), Vector2(0, 5), Vector2(0, 6), Vector2(0, 7), Vector2(0, 8), Vector2(0, 9), Vector2(0, 10), Vector2(0, 11), Vector2(0, 12), Vector2(0, 13)], [Vector2(1, 0), Vector2(1, 1), Vector2(1, 2), Vector2(1, 3), Vector2(1, 4), Vector2(1, 5), Vector2(1, 6), Vector2(1, 7), Vector2(1, 8), Vector2(1, 9), Vector2(1, 10), Vector2(1, 11), Vector2(1, 12), Vector2(1, 13)], [Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(2, 3), Vector2(2, 4), Vector2(2, 5), Vector2(2, 6), Vector2(2, 7), Vector2(2, 8), Vector2(2, 9), Vector2(2, 10), Vector2(2, 11), Vector2(2, 12), Vector2(2, 13)], [Vector2(3, 0), Vector2(3, 1), Vector2(3, 2), Vector2(3, 3), Vector2(3, 4), Vector2(3, 5), Vector2(3, 6), Vector2(3, 7), Vector2(3, 8), Vector2(3, 9), Vector2(3, 10), Vector2(3, 11), Vector2(3, 12), Vector2(3, 13)], [Vector2(4, 0), Vector2(4, 1), Vector2(4, 2), Vector2(4, 3), Vector2(4, 4), Vector2(4, 5), Vector2(4, 6), Vector2(4, 7), Vector2(4, 8), Vector2(4, 9), Vector2(4, 10), Vector2(4, 11), Vector2(4, 12), Vector2(4, 13)], [Vector2(5, 0), Vector2(5, 1), Vector2(5, 2), Vector2(5, 3), Vector2(5, 4), Vector2(5, 5), Vector2(5, 6), Vector2(5, 7), Vector2(5, 8), Vector2(5, 9), Vector2(5, 10), Vector2(5, 11), Vector2(5, 12), Vector2(5, 13)], [Vector2(6, 0), Vector2(6, 1), Vector2(6, 2), Vector2(6, 3), Vector2(6, 4), Vector2(6, 5), Vector2(6, 6), Vector2(6, 7), Vector2(6, 8), Vector2(6, 9), Vector2(6, 10), Vector2(6, 11), Vector2(6, 12), Vector2(6, 13)]]
const c_grid_index_reference_8x8 = [[Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4), Vector2(0, 5), Vector2(0, 6), Vector2(0, 7), Vector2(0, 8), Vector2(0, 9), Vector2(0, 10), Vector2(0, 11), Vector2(0, 12), Vector2(0, 13), Vector2(0, 14), Vector2(0, 15)], [Vector2(1, 0), Vector2(1, 1), Vector2(1, 2), Vector2(1, 3), Vector2(1, 4), Vector2(1, 5), Vector2(1, 6), Vector2(1, 7), Vector2(1, 8), Vector2(1, 9), Vector2(1, 10), Vector2(1, 11), Vector2(1, 12), Vector2(1, 13), Vector2(1, 14), Vector2(1, 15)], [Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(2, 3), Vector2(2, 4), Vector2(2, 5), Vector2(2, 6), Vector2(2, 7), Vector2(2, 8), Vector2(2, 9), Vector2(2, 10), Vector2(2, 11), Vector2(2, 12), Vector2(2, 13), Vector2(2, 14), Vector2(2, 15)], [Vector2(3, 0), Vector2(3, 1), Vector2(3, 2), Vector2(3, 3), Vector2(3, 4), Vector2(3, 5), Vector2(3, 6), Vector2(3, 7), Vector2(3, 8), Vector2(3, 9), Vector2(3, 10), Vector2(3, 11), Vector2(3, 12), Vector2(3, 13), Vector2(3, 14), Vector2(3, 15)], [Vector2(4, 0), Vector2(4, 1), Vector2(4, 2), Vector2(4, 3), Vector2(4, 4), Vector2(4, 5), Vector2(4, 6), Vector2(4, 7), Vector2(4, 8), Vector2(4, 9), Vector2(4, 10), Vector2(4, 11), Vector2(4, 12), Vector2(4, 13), Vector2(4, 14), Vector2(4, 15)], [Vector2(5, 0), Vector2(5, 1), Vector2(5, 2), Vector2(5, 3), Vector2(5, 4), Vector2(5, 5), Vector2(5, 6), Vector2(5, 7), Vector2(5, 8), Vector2(5, 9), Vector2(5, 10), Vector2(5, 11), Vector2(5, 12), Vector2(5, 13), Vector2(5, 14), Vector2(5, 15)], [Vector2(6, 0), Vector2(6, 1), Vector2(6, 2), Vector2(6, 3), Vector2(6, 4), Vector2(6, 5), Vector2(6, 6), Vector2(6, 7), Vector2(6, 8), Vector2(6, 9), Vector2(6, 10), Vector2(6, 11), Vector2(6, 12), Vector2(6, 13), Vector2(6, 14), Vector2(6, 15)], [Vector2(7, 0), Vector2(7, 1), Vector2(7, 2), Vector2(7, 3), Vector2(7, 4), Vector2(7, 5), Vector2(7, 6), Vector2(7, 7), Vector2(7, 8), Vector2(7, 9), Vector2(7, 10), Vector2(7, 11), Vector2(7, 12), Vector2(7, 13), Vector2(7, 14), Vector2(7, 15)]]
const c_grid_index_reference_9x9 = [[Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4), Vector2(0, 5), Vector2(0, 6), Vector2(0, 7), Vector2(0, 8), Vector2(0, 9), Vector2(0, 10), Vector2(0, 11), Vector2(0, 12), Vector2(0, 13), Vector2(0, 14), Vector2(0, 15), Vector2(0, 16), Vector2(0, 17)], [Vector2(1, 0), Vector2(1, 1), Vector2(1, 2), Vector2(1, 3), Vector2(1, 4), Vector2(1, 5), Vector2(1, 6), Vector2(1, 7), Vector2(1, 8), Vector2(1, 9), Vector2(1, 10), Vector2(1, 11), Vector2(1, 12), Vector2(1, 13), Vector2(1, 14), Vector2(1, 15), Vector2(1, 16), Vector2(1, 17)], [Vector2(2, 0), Vector2(2, 1), Vector2(2, 2), Vector2(2, 3), Vector2(2, 4), Vector2(2, 5), Vector2(2, 6), Vector2(2, 7), Vector2(2, 8), Vector2(2, 9), Vector2(2, 10), Vector2(2, 11), Vector2(2, 12), Vector2(2, 13), Vector2(2, 14), Vector2(2, 15), Vector2(2, 16), Vector2(2, 17)], [Vector2(3, 0), Vector2(3, 1), Vector2(3, 2), Vector2(3, 3), Vector2(3, 4), Vector2(3, 5), Vector2(3, 6), Vector2(3, 7), Vector2(3, 8), Vector2(3, 9), Vector2(3, 10), Vector2(3, 11), Vector2(3, 12), Vector2(3, 13), Vector2(3, 14), Vector2(3, 15), Vector2(3, 16), Vector2(3, 17)], [Vector2(4, 0), Vector2(4, 1), Vector2(4, 2), Vector2(4, 3), Vector2(4, 4), Vector2(4, 5), Vector2(4, 6), Vector2(4, 7), Vector2(4, 8), Vector2(4, 9), Vector2(4, 10), Vector2(4, 11), Vector2(4, 12), Vector2(4, 13), Vector2(4, 14), Vector2(4, 15), Vector2(4, 16), Vector2(4, 17)], [Vector2(5, 0), Vector2(5, 1), Vector2(5, 2), Vector2(5, 3), Vector2(5, 4), Vector2(5, 5), Vector2(5, 6), Vector2(5, 7), Vector2(5, 8), Vector2(5, 9), Vector2(5, 10), Vector2(5, 11), Vector2(5, 12), Vector2(5, 13), Vector2(5, 14), Vector2(5, 15), Vector2(5, 16), Vector2(5, 17)], [Vector2(6, 0), Vector2(6, 1), Vector2(6, 2), Vector2(6, 3), Vector2(6, 4), Vector2(6, 5), Vector2(6, 6), Vector2(6, 7), Vector2(6, 8), Vector2(6, 9), Vector2(6, 10), Vector2(6, 11), Vector2(6, 12), Vector2(6, 13), Vector2(6, 14), Vector2(6, 15), Vector2(6, 16), Vector2(6, 17)], [Vector2(7, 0), Vector2(7, 1), Vector2(7, 2), Vector2(7, 3), Vector2(7, 4), Vector2(7, 5), Vector2(7, 6), Vector2(7, 7), Vector2(7, 8), Vector2(7, 9), Vector2(7, 10), Vector2(7, 11), Vector2(7, 12), Vector2(7, 13), Vector2(7, 14), Vector2(7, 15), Vector2(7, 16), Vector2(7, 17)], [Vector2(8, 0), Vector2(8, 1), Vector2(8, 2), Vector2(8, 3), Vector2(8, 4), Vector2(8, 5), Vector2(8, 6), Vector2(8, 7), Vector2(8, 8), Vector2(8, 9), Vector2(8, 10), Vector2(8, 11), Vector2(8, 12), Vector2(8, 13), Vector2(8, 14), Vector2(8, 15), Vector2(8, 16), Vector2(8, 17)]]

const c_grid_index_reference = [c_grid_index_reference_4x4, c_grid_index_reference_5x5, c_grid_index_reference_6x6, c_grid_index_reference_7x7, c_grid_index_reference_8x8, c_grid_index_reference_9x9]

var g_board_type

const c_dot_z_axis = 1

var timer = null
const c_timer_countdown_value = 6		# For 3 seconds
var g_timer_counter

var g_number_of_dots
var g_columns			# Number of columns
var g_rows				# number of rows

# Holds an array of the generated dots, with their repective serial number and position
# Format: <dot> = {dot_pos = <dot_position>, dot_index = <dot_index>, dot_colour = <dot_colour>}
var g_dots = []			# Dot tracking auxiliary array
var g_dot_radius		# Calculate dot radius in accordance to the screen's width
var g_dot_to_bloat		# Current hovered dot by the user to be bloated

var g_is_moving_dots

var g_user_shape_selection = []		# Global array that hosts user's dot selections

var g_dot_scaler

# Gameplay grid-related global variables
var g_rearranged_dots_indexes = []		# A matrix of dot indexes with their previous positions
var g_grid_empty_indexes = []					# Holds the indexes of the removed dots with every shape found
var g_no_of_vacated_indexes_per_column = []		# Holds the amount of removed indexes per column

# Default grid sizes for predefined difficulty settings
var grid_width
var grid_height
var grid_centre

var grid_pos = Vector2()
var grid_dimension_px = Vector2()
var grid_index_size = Vector2()		# Holds the amount of coordinates held in the x and y axis
var grid_pos_reference = []

var corner_spacing_width
var internal_spacing_width
var corner_spacing_height
var internal_spacing_height

var dot = preload("res://scenes/data/Dot.tscn")

var g_userlevel

var g_dot_random_colour_coef

func _ready():
	DEBUG = get_node("/root/global").DEBUG_BOARD
	
	add_to_group("board")
	add_to_group("controls")
	
	randomize()
	
	grid_width = self.get_viewport().get_rect().size.width
	grid_height = self.get_viewport().get_rect().size.width
	grid_centre = (self.get_viewport().get_rect().size.height / 2)
	
	g_dot_to_bloat = null
	g_is_moving_dots = false

func user_found_heart():
	get_tree().call_group(0, "controls", "user_found_heart")

func board(l_userlevel, l_board_type):
	g_userlevel = l_userlevel		# Used to increment the amount of different colours the dots spawn with
	g_board_type = l_board_type
	g_dot_random_colour_coef = String(sqrt( 2.32 * g_userlevel )).to_int()		# Rounds down to integer values
	g_dot_scaler = get_node("/root/viewport_data").g_viewport_scaler.x * ( 9 / ( 1.1 * c_default_board_dimension[g_board_type] ) )
	g_rows = c_default_board_dimension[g_board_type] * 2
	g_columns = c_default_board_dimension[g_board_type]
	g_number_of_dots = g_rows * g_columns
	
	# Steps of board generation
	_set_base_UI()
	_config_grid()
	_config_progressbar_seperator()
	_generate_first_batch_of_dots()
	_arrange_first_batch_of_dots_to_grid()
	_start_board_game()
	set_process(true)

func set_current_userlevel(l_userlevel):
	g_userlevel = l_userlevel
	g_dot_random_colour_coef = String(sqrt( 2.32 * g_userlevel )).to_int()		# Rounds down to integer values

func _start_board_game():
	_dot_colour_randomizer()	# Immediately randomize dots's colours
	g_timer_counter = 0
	
	get_node("Display Countdown Numbers").set_digit(3)
	get_node("/root/Soundboard").play_beep()
	get_tree().call_group(0, "controls", "play_overlay_click_animation")
	
	timer = Timer.new()
	timer.set_one_shot(false)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(0.5)	# 1/2 second
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)
	timer.start()

func on_timeout():
	g_timer_counter += 1
	_dot_colour_randomizer()
	
	if (g_timer_counter == 2):
		get_node("Display Countdown Numbers").set_digit(2)
		get_node("/root/Soundboard").play_beep()
		get_tree().call_group(0, "controls", "play_overlay_click_animation")
	elif g_timer_counter == 4:
		get_node("Display Countdown Numbers").set_digit(1)
		get_node("/root/Soundboard").play_beep()
		get_tree().call_group(0, "controls", "play_overlay_click_animation")
	
	if g_timer_counter >= c_timer_countdown_value:
		get_node("Display Countdown Numbers").set_process(false)
		get_node("Display Countdown Numbers").set_hidden(true)
		
		timer.stop()
		_sync_g_dots_to_gridcontainer()					# Sync the randomized dots to g_dots
		get_node("Line Canvas").update_data(g_dots)		# Update their values in line canvas
		_setup_line_canvas()
		get_node("Line Canvas").set_process(true)		# Set line canvas process on
		get_tree().call_group(0, "controls", "enable_game_overlay_timer")
		get_tree().call_group(0, "controls", "no_action_on_board")

func enable_board():
	set_process(true)
	get_node("Shapes Engine").set_process(true)
	get_node("Line Canvas").set_process(true)
	get_node("Display Countdown Numbers").set_process(true)
	if g_timer_counter < c_timer_countdown_value:
		timer.start()

func disable_board():
	set_process(false)
	get_node("Shapes Engine").set_process(false)
	get_node("Line Canvas").set_process(false)
	get_node("Display Countdown Numbers").set_process(false)
	timer.stop()



# -----------------------/////////////////--------------------------
# 					Private auxiliary functions						
# -----------------------/////////////////--------------------------

func _set_base_UI():
	# Resize background to viewport
	get_node("Background").set_region_rect(get_node("/root/viewport_data").g_viewport_rect)
	
	# Set background colours
	if (get_node("/root/global").g_theme == 0):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
		get_node("Shade").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if (get_node("/root/global").g_theme == 1):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
		
		var l_shade_colour = get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour)
		l_shade_colour.a = 0.95
		get_node("Shade").set_modulate(l_shade_colour)
	
	get_node("Shade").set_hidden(false)
	get_node("Shade").set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	get_node("Background").set_hidden(false)
	# Set numbers colours
	get_node("Display Countdown Numbers").set_pos( self.get_viewport().get_rect().end / 2 )

# Configure board/grid size according to the difficulty setting
func _config_grid():
	var shredder_pos
	
	grid_dimension_px = Vector2(grid_width, grid_height)	# (x,y)
	get_node("ActiveDots").set_size(grid_dimension_px)
	get_node("Shredder").set_size(grid_dimension_px)
	
	grid_pos = Vector2( 0, ( grid_centre - ( 3 * ( get_node("ActiveDots").get_rect( ).size.width / 2 ) ) ) )
	grid_index_size = Vector2(c_default_board_dimension[g_board_type], c_default_board_dimension[g_board_type] * 2)
	
	# Set abstract grid container to previously calculated position
	get_node("ActiveDots").set_pos(grid_pos)
	get_node("Shredder").set_pos(grid_pos)
	
	internal_spacing_width = grid_width / g_columns
	corner_spacing_width = internal_spacing_width / 2
	internal_spacing_height = internal_spacing_width
	corner_spacing_height = corner_spacing_width

func _config_progressbar_seperator():
	var l_seperator_pos = Vector2(0,0)
	var l_active_dots_pos = get_node("ActiveDots").get_pos()
	var l_active_dots_size = get_node("ActiveDots").get_size()
	
	l_seperator_pos.x = l_active_dots_size.x / 2
	l_seperator_pos.y = l_active_dots_pos.y + l_active_dots_size.y
	get_tree().call_group(0, "controls", "config_progressbar", l_seperator_pos)

# Generates the dot objects and populates an abstract grid container with them
func _generate_first_batch_of_dots():
	var l_temp_dot
	
	# Instance all necessary dot objects and assign them to an array
	for i in range(g_number_of_dots):
		l_temp_dot = dot.instance()
		l_temp_dot.set_dot_z(c_dot_z_axis)
		l_temp_dot.set_dot_scale(g_dot_scaler)
		get_node("ActiveDots").add_child(l_temp_dot)
		l_temp_dot = {dot_pos = null, dot_index = null, dot_colour = null}
		g_dots.append(l_temp_dot)
	g_dot_radius = ( get_node("ActiveDots").get_child( get_node("ActiveDots").get_child_count() - 1 ).get_dot_size() / 2 )

# Organize dots in a grid-like arrangement
func _arrange_first_batch_of_dots_to_grid():
	var l_child_counter = 0
	var l_width_incrememt
	var l_height_increment
	
	l_width_incrememt = corner_spacing_width
	for c in range(g_columns):
		grid_pos_reference.append([])
		l_height_increment = corner_spacing_height
		
		for r in range(g_rows):
			get_node("ActiveDots").get_child(l_child_counter).set_dot_pos( Vector2(l_width_incrememt, l_height_increment) )
			get_node("ActiveDots").get_child(l_child_counter).set_dot_index(Vector2(c, r))
			grid_pos_reference[c].append(Vector2(l_width_incrememt, l_height_increment))
			l_height_increment += internal_spacing_height
			l_child_counter += 1
		l_width_incrememt += internal_spacing_width

func _get_random_dot_colour_index():
	var aux = g_dot_random_colour_coef
	
	if aux < 3:
		aux = 3
	elif aux > 10:
		aux = 10
	
	var l_dot_index = randi() % aux
	return l_dot_index

func _dot_colour_randomizer():
	var l_random_picked_colour
	
	for l_child_counter in range(get_node("ActiveDots").get_child_count()):
		if DEBUG:
			get_node("ActiveDots").get_child(l_child_counter).set_dot_colour(Color(0,0,0,1))
		else:
			l_random_picked_colour = get_node("/root/colours").dot_colours[_get_random_dot_colour_index()]
			get_node("ActiveDots").get_child(l_child_counter).set_dot_colour(l_random_picked_colour)

func _setup_line_canvas():
	get_node("Line Canvas").init(g_dots, g_dot_radius)



# -----------------------/////////////////--------------------------
# 						Process user input							
# -----------------------/////////////////--------------------------

func bloat_dot(l_dot_index_to_bloat):
	g_dot_to_bloat = l_dot_index_to_bloat

func unbloat_dots():
	g_dot_to_bloat = null

func _remove_user_selected_dots():
	var l_child_counter = 0
	var l_aux_dot
	
	# Finally, remove the dots from the tree
	while l_child_counter <  get_node("ActiveDots").get_child_count():
		if(get_node("ActiveDots").get_child(l_child_counter) != null):
			if ( g_user_shape_selection.has( get_node("ActiveDots").get_child( l_child_counter ).get_dot_index() ) ):
				# First, clone the dot
				l_aux_dot = dot.instance()
				l_aux_dot.set_dot_z(c_dot_z_axis)
				l_aux_dot.set_dot_pos( get_node("ActiveDots").get_child( l_child_counter ).get_dot_pos() )
				l_aux_dot.set_dot_colour( get_node("ActiveDots").get_child( l_child_counter ).get_dot_colour() )
				l_aux_dot.set_dot_scale( g_dot_scaler )
				
				get_node("Shredder").add_child(l_aux_dot)		# Duplicate dot to the shredder
				get_node("ActiveDots").get_child(l_child_counter).queue_free_dot()
				get_node("ActiveDots").remove_child(get_node("ActiveDots").get_child(l_child_counter))
				
				l_child_counter -= 1
		l_child_counter += 1
	get_node("Shredder").start_shredder()

func _sync_g_dots_to_gridcontainer():
	var l_child_counter = 0
	var l_temp_data
	var l_temp_data_dot_pos
	var l_temp_data_dot_index
	var l_temp_data_dot_colour
	
	g_dots = []
	
	while l_child_counter < get_node("ActiveDots").get_child_count():
		if(get_node("ActiveDots").get_child(l_child_counter) != null):
			if (get_node("ActiveDots").get_child(l_child_counter).get_dot_index().y >= c_default_board_dimension[g_board_type]):
				l_temp_data_dot_pos = get_node("ActiveDots").get_child(l_child_counter).get_dot_pos() + get_node("ActiveDots").get_pos()
				l_temp_data_dot_index = get_node("ActiveDots").get_child(l_child_counter).get_dot_index()
				l_temp_data_dot_colour = get_node("ActiveDots").get_child(l_child_counter).get_dot_colour()
				l_temp_data = {dot_pos = l_temp_data_dot_pos, dot_index = l_temp_data_dot_index, dot_colour = l_temp_data_dot_colour}
				g_dots.append(l_temp_data)
		l_child_counter += 1

# Determines which indexes will be free after a grid clean
func _calculate_empty_grid_indexes():
	g_no_of_vacated_indexes_per_column = []
	g_grid_empty_indexes = []
	
	if (g_user_shape_selection.size() > 0):
		# Initiate the tracking array with 0s
		
		for i in range(grid_index_size.x):
			g_no_of_vacated_indexes_per_column.append(0)
		
		# Count all the vacated indexes in each column
		for i in range(grid_index_size.x):
			for j in range(g_user_shape_selection.size()):
				if(g_user_shape_selection[j] != null):
					if g_user_shape_selection[j].x == i:
						g_no_of_vacated_indexes_per_column[i] += 1
		
		# Determine which indexes will be empty after the grid has been cleaned
		for i in range(g_no_of_vacated_indexes_per_column.size()):
			if(g_no_of_vacated_indexes_per_column[i] > 0):
				for j in range(g_no_of_vacated_indexes_per_column[i]):
					g_grid_empty_indexes.append(Vector2(i, j))
			else:
				g_grid_empty_indexes.append(null)

# Rearranges the dots indexes in the grid, moving them downwards to fill in any gaps left by the found shape dot's indexes
func _rearrange_dot_indexes():
	var l_auxiliary_array = []
	var l_aux_data
	
	# Reset g_rearranged_dots_indexes indexes
	g_rearranged_dots_indexes = []
	g_rearranged_dots_indexes = c_grid_index_reference[g_board_type]
	
	# Browse current existing alive children and fill in their current indexes
	for i in range(g_rearranged_dots_indexes.size()):
		for j in range(g_user_shape_selection.size()):
			l_aux_data = g_rearranged_dots_indexes[i].find(g_user_shape_selection[j])
			
			if l_aux_data != -1:
				g_rearranged_dots_indexes[i][l_aux_data] = null
	
	for i in range(grid_index_size.x):
		if(g_no_of_vacated_indexes_per_column[i] > 0):
			l_auxiliary_array = []
			
			for j in range(grid_index_size.y - 1, -1, -1):
				if(g_rearranged_dots_indexes[i][j] != null):
					l_auxiliary_array.append(g_rearranged_dots_indexes[i][j])
			
			l_auxiliary_array.resize(grid_index_size.y)
			l_auxiliary_array.invert()
			
			g_rearranged_dots_indexes[i] = []
			g_rearranged_dots_indexes[i] = l_auxiliary_array

func _rearrange_dot_positions_according_to_their_new_indexes():
	var l_index_aux
	
	for l_child_counter in range( get_node("ActiveDots").get_child_count() ):
		if (get_node("ActiveDots").get_child(l_child_counter) != null):
			for i in range(grid_index_size.x):
				if(g_no_of_vacated_indexes_per_column[i] > 0):
					l_index_aux = g_rearranged_dots_indexes[i].find(get_node("ActiveDots").get_child(l_child_counter).get_dot_index())
					
					if l_index_aux != -1:		# If index was found in column
						if Vector2(i, l_index_aux) != g_rearranged_dots_indexes[i][l_index_aux]:	# And its index is different from the rearranged one
							get_node("ActiveDots").get_child(l_child_counter).set_dot_next_position(grid_pos_reference[i][l_index_aux], Vector2(i, l_index_aux))

func _regenerate_dots():
	var l_temp_dot
	
	for i in range(g_grid_empty_indexes.size()):
		if g_grid_empty_indexes[i] != null:
			l_temp_dot = dot.instance()
			l_temp_dot.set_dot_z(c_dot_z_axis)
			l_temp_dot.set_dot_scale(g_dot_scaler)
			l_temp_dot.set_dot_pos( grid_pos_reference[g_grid_empty_indexes[i].x][g_grid_empty_indexes[i].y] )
			l_temp_dot.set_dot_index( g_grid_empty_indexes[i] )
			if DEBUG:
				l_temp_dot.set_dot_colour(Color(0,0,0,1))
			else:
				l_temp_dot.set_dot_colour( get_node("/root/colours").dot_colours[_get_random_dot_colour_index()] )
			get_node("ActiveDots").add_child(l_temp_dot)
	g_grid_empty_indexes = []

func user_shape_selection(l_user_shape_selection):
	if (get_node("Shapes Engine").validate_shape(l_user_shape_selection)):
		get_tree().call_group(0, "controls", "action_on_board")
		get_node("Line Canvas").clear_canvas_line()		# Immediately clear the canvas line
		get_node("Line Canvas").set_process(false)
		get_node("/root/Soundboard").play_found_object()		# Play a sound
		
		while(g_user_shape_selection.size() > 0):
			g_user_shape_selection = []
		
		g_user_shape_selection = l_user_shape_selection
		_remove_user_selected_dots()
		_calculate_empty_grid_indexes()
		_rearrange_dot_indexes()
		_rearrange_dot_positions_according_to_their_new_indexes()
		g_is_moving_dots = true
		_regenerate_dots()
		_sync_g_dots_to_gridcontainer()
		get_node("Line Canvas").update_data(g_dots)
		get_tree().call_group(0, "controls", "add_user_score", l_user_shape_selection)

func _generate_user_dot_selection_reference(l_dot_pos, l_dot_index, l_dot_colour):
	var l_aux_dot = dot.instance()
	l_aux_dot.set_dot_z(c_dot_z_axis)
	l_aux_dot.set_dot_pos( l_dot_pos - get_node("ActiveDots").get_pos() )
	l_aux_dot.set_dot_colour(l_dot_colour)
	l_aux_dot.set_dot_scale(g_dot_scaler)
	get_node("Shredder").add_child(l_aux_dot)		# Duplicate dot to the shredder
	get_node("Shredder").start_shredder()

func clear_a_board_line():
	var l_aux_dot
	var l_board_dimension = ( c_default_board_dimension[g_board_type] * 2 ) - 1
	
	get_tree().call_group(0, "controls", "action_on_board")
	g_user_shape_selection = []
	
	for i in range(get_node("ActiveDots").get_child_count()):
		if get_node("ActiveDots").get_child(i) != null:
			if get_node("ActiveDots").get_child(i).get_dot_index().y == l_board_dimension:
				g_user_shape_selection.append(get_node("ActiveDots").get_child(i).get_dot_index())
	
	_remove_user_selected_dots()
	_calculate_empty_grid_indexes()
	_rearrange_dot_indexes()
	_rearrange_dot_positions_according_to_their_new_indexes()
	g_is_moving_dots = true
	_regenerate_dots()
	_sync_g_dots_to_gridcontainer()
	get_node("Line Canvas").update_data(g_dots)



# -----------------------/////////////////--------------------------
# 						Board game process						
# -----------------------/////////////////--------------------------

func _process(delta):
	var l_child_counter = 0
	var l_all_dots_reached_destination_latch = 0
	var l_dot_colour
	
	while l_child_counter < get_node("ActiveDots").get_child_count():
		if(get_node("ActiveDots").get_child(l_child_counter) != null):
			l_dot_colour = get_node("ActiveDots").get_child(l_child_counter).get_dot_colour()
			get_node("ActiveDots").get_child(l_child_counter).set_dot_colour(l_dot_colour)
			
			# Check for dots to bloat
			if get_node("ActiveDots").get_child(l_child_counter).get_dot_index() == g_dot_to_bloat:		# Pin down dot within gridcontainer and bloat it
				get_node("ActiveDots").get_child(l_child_counter).bloat_dot()
			else:
				get_node("ActiveDots").get_child(l_child_counter).unbloat_dot()
			
			# Check for dots to move
			if g_is_moving_dots:
				if ( get_node("ActiveDots").get_child(l_child_counter).is_moving() ):
					
					l_all_dots_reached_destination_latch = 1
		
		l_child_counter += 1
	
	if l_all_dots_reached_destination_latch == 0:
		g_is_moving_dots = false
		get_node("Line Canvas").set_process(true)
