# Shapes processor is an auxialiary node that's part of Shapes Engine.
# It is used to manipulate user's input.

extends Node2D

var g_shape = []

var g_translation		# Holds the current shape translation value
var g_scale				# Holds the current shape scale value
var g_rotation			# Holds the current shape rotation value

var g_shape_matrix32_transfornation_reference_matrix = []

func _ready():
	pass



# -----------------------/////////////////--------------------------
# 					Module interface functions						
# -----------------------/////////////////--------------------------

func instance_shape(l_shape_indexes):
	var l_matrix
	var l_shape_epicentre_offset = Vector2(0,0)
	
	# Calculate offset and translate center of mass towards center of the cartesian grid
	for i in range(l_shape_indexes.size()):
		l_shape_epicentre_offset += l_shape_indexes[i]
	
	l_shape_epicentre_offset /= l_shape_indexes.size()
	
	for i in range(l_shape_indexes.size()):
		l_matrix = Matrix32()		# Construct new Matrix32() object and attach it to l_matrix
		
		l_matrix[0] = Vector2(1,0) * ( l_shape_indexes[i] - l_shape_epicentre_offset )		# X axis
		l_matrix[1] = Vector2(0,1) * ( l_shape_indexes[i] - l_shape_epicentre_offset )		# Y axis
		l_matrix[2] = Vector2(0,0)								# Default center at cartesian origin 
		
		g_shape_matrix32_transfornation_reference_matrix.append(l_matrix)
	
	_set_g_shape_array_from_matrix32_reference_array()

func get_shape():
	_set_g_shape_array_from_matrix32_reference_array()
	
	return g_shape

func rotate_shape(l_rotation):
	g_rotation = l_rotation
	_rotate_shape()

func translate_shape(l_translation):
	g_translation = l_translation
	_translate_shape()

func scale_shape(l_scale):
	g_scale = l_scale
	_scale_shape()



# -----------------------/////////////////--------------------------
# 					Auxialiary internal functions						
# -----------------------/////////////////--------------------------

func _set_g_shape_array_from_matrix32_reference_array():
	if(g_shape.size() != 0):
		g_shape.clear()
	
	if(g_shape_matrix32_transfornation_reference_matrix.size() != 0):
		for i in range(g_shape_matrix32_transfornation_reference_matrix.size()):
			g_shape.append(g_shape_matrix32_transfornation_reference_matrix[i][0] + g_shape_matrix32_transfornation_reference_matrix[i][1] + g_shape_matrix32_transfornation_reference_matrix[i][2])



# -----------------------/////////////////--------------------------
# 					General purpose functions						
# -----------------------/////////////////--------------------------

# Retrieve current size of the current shape held by this node
func get_shape_size():
	return calc_shape_size(g_shape)

# Determine the max index values in an array
func calc_shape_size(l_shape):
	var l_max_index_found = Vector2(-10000, -10000)
	
	if (l_shape.size() != 0):
		for i in range(l_shape.size()):
			if(l_shape[i].x > l_max_index_found.x):
				l_max_index_found.x = l_shape[i].x
			if(l_shape[i].y > l_max_index_found.y):
				l_max_index_found.y = l_shape[i].y
		
		return l_max_index_found
	else:
		return null

# Wipes and reset object
func clear_shape():
	if ( g_shape.size() != 0 ):
		g_shape = []
	
	if ( g_shape_matrix32_transfornation_reference_matrix.size() != 0 ) :
		g_shape_matrix32_transfornation_reference_matrix = []
	
	g_translation = Vector2(0,0)
	g_scale = Vector2(1,1)
	g_rotation = 0



# -----------------------/////////////////--------------------------
# 				Scale, rotate & translate functions					
# -----------------------/////////////////--------------------------

# Scale a shape to the desired value
func _scale_shape():
	if ( g_shape_matrix32_transfornation_reference_matrix.size() != 0 ):
		for i in range(g_shape_matrix32_transfornation_reference_matrix.size()):
			g_shape_matrix32_transfornation_reference_matrix[i] = g_shape_matrix32_transfornation_reference_matrix[i].scaled(g_scale)
		
		return true		# Return true if g_shape wasn't empty and operation was successful
	
	return false		# Return false if operation was unsucessful

# Rotate a shape to the desired value
func _rotate_shape():
	if ( g_shape_matrix32_transfornation_reference_matrix.size() != 0 ):
		for i in range(g_shape_matrix32_transfornation_reference_matrix.size()):
			g_shape_matrix32_transfornation_reference_matrix[i] = g_shape_matrix32_transfornation_reference_matrix[i].rotated(g_rotation)
		
		return true		# Return true if g_shape wasn't empty and operation was successful
	
	return false		# Return false if operation was unsucessful

# Translate a shape to the desired value
func _translate_shape():
	if ( g_shape_matrix32_transfornation_reference_matrix.size() != 0 ):
		for i in range(g_shape_matrix32_transfornation_reference_matrix.size()):
			g_shape_matrix32_transfornation_reference_matrix[i][0] += g_translation
			g_shape_matrix32_transfornation_reference_matrix[i][1] += g_translation
		
		return true		# Return true if g_shape wasn't empty and operation was successful
	
	return false		# Return false if operation was unsucessful

# Sets the shape at the beggining of the 4th quadrant
func _set_shape_reference_origin_at_zero():
	var l_offset = Vector2(10000, 10000)	# highest x, highest y, for initial comparison purposes
	
	for i in range(g_shape_matrix32_transfornation_reference_matrix.size()):
		if(g_shape_matrix32_transfornation_reference_matrix[i][0].x < l_offset.x):
			l_offset.x = g_shape_matrix32_transfornation_reference_matrix[i][0].x
		if(g_shape_matrix32_transfornation_reference_matrix[i][1].x < l_offset.x):
			l_offset.x = g_shape_matrix32_transfornation_reference_matrix[i][1].x
		if(g_shape_matrix32_transfornation_reference_matrix[i][0].y < l_offset.y):
			l_offset.y = g_shape_matrix32_transfornation_reference_matrix[i][0].y
		if(g_shape_matrix32_transfornation_reference_matrix[i][1].y < l_offset.y):
			l_offset.y = g_shape_matrix32_transfornation_reference_matrix[i][1].y
	
	for i in range(g_shape_matrix32_transfornation_reference_matrix.size()):
		g_shape_matrix32_transfornation_reference_matrix[i][2] = -l_offset
