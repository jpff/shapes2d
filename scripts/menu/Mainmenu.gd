extends Node2D

var timer_first_launch = null
var g_first_time_animation_sequence = 0

var timer = null
var g_option_switch = null
var g_button_timeout = 0

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	g_option_switch = 0
	set_background()
	_setup_menu_icons()
	_init_timer()
	
	if get_node("/root/global").g_toggle_music && get_node("/root/global").g_accepted_tos == true:
		get_node("/root/Soundboard").lower_music_volume(true)
		get_node("/root/Soundboard").enable_game_music()
	else:
		get_node("/root/Soundboard").disable_game_music()
	
	get_node("/root/global").reset_board_controls()
	set_process(true)

func _play_first_time_app_launch_animation():
	get_node("Play").disconnect("pressed", self, "_on_Play_pressed")
	get_node("Settings").disconnect("pressed", self, "_on_Settings_pressed")
	get_node("Like").disconnect("pressed", self, "_on_Like_pressed")
	get_node("About").disconnect("pressed", self, "_on_About_pressed")
	get_node("Exit").disconnect("pressed", self, "_on_Exit_pressed")
	get_node("Help").disconnect("pressed", self, "_on_Help_pressed")
	
	timer_first_launch = Timer.new()
	timer_first_launch.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer_first_launch.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer_first_launch.set_wait_time(0.3)
	timer_first_launch.connect("timeout", self, "_on_timer_first_launch_timeout")
	add_child(timer_first_launch)
	timer_first_launch.start()

func _on_timer_first_launch_timeout():
	timer_first_launch.stop()
	
	if g_first_time_animation_sequence >= 0 && g_first_time_animation_sequence < 9:
		timer_first_launch.stop()
		get_node("Help").invert_colours()
		g_first_time_animation_sequence += 1
		timer_first_launch.start()
	
	if g_first_time_animation_sequence == 9:
		get_node("Help").play_click_animation()
		g_first_time_animation_sequence += 1
		timer_first_launch.set_wait_time(1.5)
		timer_first_launch.start()
	elif g_first_time_animation_sequence == 10:
		get_node("/root/global").g_first_time_app_launch = false
		get_node("/root/global").save_user_data()
		get_node("/root/global").goto_scene(get_node("/root/global").Help)

func _process(delta):
	if get_node("/root/global").g_accepted_tos == true && get_node("/root/global").g_first_time_app_launch == false:	# If ToS accepted and app first time launching
		get_node("Play").set_ignore_mouse(false)
		get_node("Settings").set_ignore_mouse(false)
		get_node("Like").set_ignore_mouse(false)
		get_node("About").set_ignore_mouse(false)
		get_node("Help").set_ignore_mouse(false)
		get_node("Exit").set_ignore_mouse(false)
		set_process(false)
	else:
		if get_node("/root/global").g_accepted_tos == true && get_node("/root/global").g_first_time_app_launch == true:		# if accepted ToS and first time launching
			set_process(false)
			_play_first_time_app_launch_animation()
		else:
			get_node("Terms and Conditions").set_hidden(false)
			get_node("Play").set_ignore_mouse(true)
			get_node("Settings").set_ignore_mouse(true)
			get_node("Like").set_ignore_mouse(true)
			get_node("About").set_ignore_mouse(true)
			get_node("Help").set_ignore_mouse(true)
			get_node("Exit").set_ignore_mouse(true)

# Used to calculate the timeout for the button spawn delay timers
func _get_button_timeout_value():
	g_button_timeout += get_node("/root/global").menu_button_timeout_secs
	return g_button_timeout

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

func on_timeout():
	# Do something...
	if (g_option_switch == 1):
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").goto_scene(get_node("/root/global").Pre_Board_Menu)
	
	if (g_option_switch == 2):
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").goto_scene(get_node("/root/global").About)
	
	if (g_option_switch == 3):
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").goto_scene(get_node("/root/global").Settings)
	
	if (g_option_switch == 4):
		timer.stop()
		get_node("Like").invert_colours()
		OS.shell_open(get_node("/root/global").c_googleplay_app_page)
	
	if (g_option_switch == 6):
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").goto_scene(get_node("/root/global").Help)
	
	if (g_option_switch == 5):
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").save_user_data()
		get_node("/root/global").exit_game()

func _setup_menu_icons():
	get_node("Background Icon 1").load_PID_for_scale_0_625()
	get_node("Background Icon 2").load_PID_for_scale_0_625()
	get_node("Help").load_PID_for_scale_0_625()
	get_node("Play").load_PID_for_scale_0_625()
	get_node("Settings").load_PID_for_scale_0_625()
	get_node("Like").load_config_for_scale_0_625()
	get_node("About").load_PID_for_scale_0_625()
	get_node("Exit").load_PID_for_scale_0_625()
	
	if get_node("/root/global").g_first_time_app_launch == false:
		get_node("Background Icon 2").setup(get_node("/root/viewport_data").g_dot_pos[0], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("dark brown"), _get_button_timeout_value(), true)
		get_node("Background Icon 1").setup(get_node("/root/viewport_data").g_dot_pos[1], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("orange"), _get_button_timeout_value(), true)
		get_node("Settings").setup(get_node("/root/viewport_data").g_dot_pos[2], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("violet"), _get_button_timeout_value(), true)
		get_node("Play").setup(get_node("/root/viewport_data").g_dot_pos[3], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("green"), _get_button_timeout_value(), true)
		get_node("Like").setup(get_node("/root/viewport_data").g_dot_pos[4], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("orange"), _get_button_timeout_value(), true)
		get_node("About").setup(get_node("/root/viewport_data").g_dot_pos[5], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("yellow"), _get_button_timeout_value(), true)
		get_node("Exit").setup(get_node("/root/viewport_data").g_dot_pos[6], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("red"), _get_button_timeout_value(), true)
		get_node("Help").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("blue"), _get_button_timeout_value(), true)
		
		get_node("Play").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_play_description, 0, get_node("/root/colours").get_colour("green").inverted(), "t_mainmenu_play_description")
		get_node("Settings").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_settings_description, 0, get_node("/root/colours").get_colour("violet").inverted(), "t_mainmenu_settings_description")
		get_node("About").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_about_description, 0, get_node("/root/colours").get_colour("yellow").inverted(), "t_mainmenu_about_description")
		get_node("Exit").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_exit_description, 0, get_node("/root/colours").get_colour("red").inverted(), "t_mainmenu_exit_description")
		get_node("Like").set_button_overlay_texture(get_node("/root/circular_button").t_likemenu_star, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_likemenu_star")
		get_node("Like").set_button_overlay_texture(get_node("/root/circular_button").t_likemenu_rateapp_desc, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_likemenu_rateapp_desc")
		get_node("Help").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_help_texture, 0, get_node("/root/colours").get_colour("blue").inverted(), "t_mainmenu_help_texture")
	else:
		get_node("Background Icon 2").setup(get_node("/root/viewport_data").g_dot_pos[0], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("Background Icon 1").setup(get_node("/root/viewport_data").g_dot_pos[1], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("Settings").setup(get_node("/root/viewport_data").g_dot_pos[2], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("Play").setup(get_node("/root/viewport_data").g_dot_pos[3], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("Like").setup(get_node("/root/viewport_data").g_dot_pos[4], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("About").setup(get_node("/root/viewport_data").g_dot_pos[5], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("Exit").setup(get_node("/root/viewport_data").g_dot_pos[6], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		get_node("Help").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
		
		get_node("Play").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_play_description, 0, get_node("/root/colours").get_colour("violet"), "t_mainmenu_play_description")
		get_node("Settings").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_settings_description, 0, get_node("/root/colours").get_colour("violet"), "t_mainmenu_settings_description")
		get_node("About").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_about_description, 0, get_node("/root/colours").get_colour("violet"), "t_mainmenu_about_description")
		get_node("Exit").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_exit_description, 0, get_node("/root/colours").get_colour("violet"), "t_mainmenu_exit_description")
		get_node("Like").set_button_overlay_texture(get_node("/root/circular_button").t_likemenu_star, 0, get_node("/root/colours").get_colour("violet"), "t_likemenu_star")
		get_node("Like").set_button_overlay_texture(get_node("/root/circular_button").t_likemenu_rateapp_desc, 0, get_node("/root/colours").get_colour("violet"), "t_likemenu_rateapp_desc")
		get_node("Help").set_button_overlay_texture(get_node("/root/circular_button").t_mainmenu_help_texture, 0, get_node("/root/colours").get_colour("violet"), "t_mainmenu_help_texture")
	
	get_node("Static Menu Icons/Game Header").set_modulate(get_node("/root/colours").get_colour("blue"))
	var l_colour_aux = get_node("Static Menu Icons/Game Header").get_modulate().inverted()
	l_colour_aux.a = 0.2
	get_node("Static Menu Icons/Game Header Background Filling").set_modulate(l_colour_aux)
	get_node("Static Menu Icons/Game Header Background Boundary").set_modulate(get_node("Static Menu Icons/Game Header").get_modulate())

func set_background():
	if(!get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	get_node("Background").set_hidden(false)



# Signals

func _on_Play_pressed():
	get_node("Play").invert_colours()
	get_node("Play").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 1
	timer.start()

func _on_About_pressed():
	get_node("About").invert_colours()
	get_node("About").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 2
	timer.start()

func _on_Settings_pressed():
	get_node("Settings").invert_colours()
	get_node("Settings").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 3
	timer.start()

func _on_Like_pressed():
	get_node("Like").invert_colours()
	get_node("Like").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 4
	timer.start()

func _on_Exit_pressed():
	get_node("Exit").invert_colours()
	get_node("Exit").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 5
	timer.start()

func _on_Help_pressed():
	get_node("Help").invert_colours()
	get_node("Help").play_click_animation()
	g_option_switch = 6
	get_node("/root/Soundboard").play_click()
	timer.start()
