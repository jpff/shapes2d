extends Node2D

var timer = null
var g_option_switch

# Settings variables
var g_toggle_music
var g_toggle_timer
var g_theme

var g_button_timeout = 0

var g_free_to_play_version
var lock_timer = null
const lock_timeout = 1	# 1 second

const c_icon_boundary_overscale = Vector2(0.04, 0.04)

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	
	get_settings()
	g_option_switch = 0
	g_free_to_play_version = get_node("/root/global").free_to_play_version
	
	# If this is the f2p version then start the intermitent lock icon timer
	if(g_free_to_play_version):
		init_lock_timer()
	
	_setup_menu_icons()
	_init_timer()

# Used to calculate the timeout for the button spawn delay timers
func _get_button_timeout_value():
	g_button_timeout += get_node("/root/global").menu_button_timeout_secs
	return g_button_timeout

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

func on_timeout():
	# Do something...
	if (g_option_switch == 1):	# Save action
		timer.stop()
		get_node("Save").invert_colours()
		
		# Reset global control variable
		g_option_switch = 0
	
	if (g_option_switch == 2):	# Back action
		remove_child(timer)
		timer.queue_free()
		
		get_node("/root/global").goto_scene(get_node("/root/global").Mainmenu)

func init_lock_timer():
	lock_timer = Timer.new()
	lock_timer.set_one_shot(false)	# Timer continues to overflow until scene is purged off the tree
	lock_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	lock_timer.set_wait_time(lock_timeout)
	lock_timer.connect("timeout", self, "on_lock_timeout")
	add_child(lock_timer)
	lock_timer.start()

func on_lock_timeout():
	get_node("Toggle Timer").set_child_hidden(!get_node("Toggle Timer").get_child_hidden_status("t_settingsmenu_lock"), "t_settingsmenu_lock")

func _setup_menu_icons():
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	
	get_node("Back").load_PID_for_scale_0_625()
	get_node("Save").load_config_for_scale_0_625()
	get_node("Theme Clear").load_config_for_scale_0_625()
	get_node("Theme Dark").load_config_for_scale_0_625()
	get_node("Toggle Music").load_config_for_scale_0_625()
	get_node("Toggle Timer").load_config_for_scale_0_625()
	get_node("Background Icon 1").load_config_for_scale_0_625()
	get_node("Background Icon 2").load_config_for_scale_0_625()
	
	get_node("Background Icon 1").setup(get_node("/root/viewport_data").g_dot_pos[0], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("blue"), _get_button_timeout_value(), true)
	get_node("Background Icon 2").setup(get_node("/root/viewport_data").g_dot_pos[1], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("violet"), _get_button_timeout_value(), true)
	get_node("Toggle Music").setup(get_node("/root/viewport_data").g_dot_pos[2], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("yellow"), _get_button_timeout_value(), true)
	get_node("Toggle Timer").setup(get_node("/root/viewport_data").g_dot_pos[3], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("green"), _get_button_timeout_value(), true)
	get_node("Theme Dark").setup(get_node("/root/viewport_data").g_dot_pos[4], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("black"), _get_button_timeout_value(), true)
	get_node("Theme Clear").setup(get_node("/root/viewport_data").g_dot_pos[5], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("white"), _get_button_timeout_value(), true)
	get_node("Save").setup(get_node("/root/viewport_data").g_dot_pos[6], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("orange"), _get_button_timeout_value(), true)
	get_node("Back").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("blue"), _get_button_timeout_value(), true)
	
	get_node("Back").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_back_description, 0, get_node("/root/colours").get_colour("blue").inverted(), "t_settingsmenu_back_description")
	get_node("Save").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_save_description, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_settingsmenu_save_description")
	get_node("Theme Clear").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_themeclear_description, 0, get_node("/root/colours").get_colour("black"), "t_settingsmenu_themeclear_description")
	get_node("Theme Clear").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_themeclear_boundary, 1, get_node("/root/colours").get_colour("black"), "t_settingsmenu_themeclear_boundary")
	get_node("Theme Clear").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_themeclear_disabled, 0, get_node("/root/colours").get_colour("red"), "t_settingsmenu_themeclear_disabled")
	get_node("Theme Dark").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_themedark_description, 0, get_node("/root/colours").get_colour("white"), "t_settingsmenu_themedark_description")
	get_node("Theme Dark").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_themedark_boundary, 1, get_node("/root/colours").get_colour("white"), "t_settingsmenu_themedark_boundary")
	get_node("Theme Dark").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_themedark_disabled, 0, get_node("/root/colours").get_colour("red"), "t_settingsmenu_themedark_disabled")
	get_node("Toggle Music").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_music_boundary, 1, get_node("/root/colours").get_colour("yellow").inverted(), "t_settingsmenu_music_boundary")
	get_node("Toggle Music").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_musical_note, 0, get_node("/root/colours").get_colour("yellow").inverted(), "t_settingsmenu_musical_note")
	get_node("Toggle Music").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_music_disabled, 0, get_node("/root/colours").get_colour("red"), "t_settingsmenu_music_disabled")
	get_node("Toggle Timer").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_timer_boundary, 1, get_node("/root/colours").get_colour("green").inverted(), "t_settingsmenu_timer_boundary")
	get_node("Toggle Timer").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_lock, 0, get_node("/root/colours").get_colour("green").inverted(), "t_settingsmenu_lock")
	get_node("Toggle Timer").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_pocket_clock, 0, get_node("/root/colours").get_colour("green").inverted(), "t_settingsmenu_pocket_clock")
	get_node("Toggle Timer").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_timer_disabled, 0, get_node("/root/colours").get_colour("red"), "t_settingsmenu_timer_disabled")
	
	get_node("Theme Clear").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_themeclear_boundary")
	get_node("Theme Dark").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_themedark_boundary")
	get_node("Toggle Music").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_music_boundary")
	get_node("Toggle Timer").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_timer_boundary")
	
	get_node("Theme Clear").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_themeclear_disabled")
	get_node("Theme Dark").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_themedark_disabled")
	get_node("Toggle Music").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_music_disabled")
	get_node("Toggle Timer").set_child_overscale(c_icon_boundary_overscale, "t_settingsmenu_timer_disabled")
	
	get_node("Static Menu Icons/Settings Header").set_modulate(get_node("/root/colours").get_colour("red"))
	get_node("Static Menu Icons/Settings Header").set_z(2)
	var l_colour_aux = get_node("Static Menu Icons/Settings Header").get_modulate()
	l_colour_aux.a = 0.2
	get_node("Static Menu Icons/Settings Header Background Filling").set_modulate(l_colour_aux)
	get_node("Static Menu Icons/Settings Header background Boundary").set_modulate(get_node("Static Menu Icons/Settings Header").get_modulate())
	
	get_node("Timer Warning").set_hidden(true)
	get_node("Background").set_z(-5)
	#get_node("Timer Warning").set_z(10)
	get_node("Timer Warning").add_color_override("font_color", get_node("/root/colours").get_colour("red"))
	
	if(g_theme == 0):
		get_node("Theme Clear").set_child_hidden(true, "t_settingsmenu_themeclear_disabled")
		get_node("Theme Dark").set_child_hidden(false, "t_settingsmenu_themedark_disabled")
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(g_theme == 1):
		get_node("Theme Clear").set_child_hidden(false, "t_settingsmenu_themeclear_disabled")
		get_node("Theme Dark").set_child_hidden(true, "t_settingsmenu_themedark_disabled")
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Toggle Music").set_child_hidden(g_toggle_music, "t_settingsmenu_music_disabled")
	get_node("Toggle Timer").set_child_hidden(g_toggle_timer, "t_settingsmenu_timer_disabled")
	get_node("Toggle Timer").set_child_hidden(!get_node("/root/global").free_to_play_version, "t_settingsmenu_lock")
	
	get_node("Background").set_hidden(false)

func get_settings():
	# Fetch settings off the main node...
	if get_node("/root/global").free_to_play_version:
		g_toggle_timer = true
	else:
		g_toggle_timer = get_node("/root/global").g_toggle_timer
	g_toggle_music = get_node("/root/global").g_toggle_music
	g_theme = get_node("/root/global").g_theme



# Signals

func _on_Theme_Clear_pressed():
	g_theme = 0
	
	get_node("Theme Clear").play_click_animation()
	get_node("Background").set_modulate(get_node("/root/colours").get_colour("white"))
	get_node("Theme Clear").set_child_hidden(true, "t_settingsmenu_themeclear_disabled")
	get_node("Theme Dark").set_child_hidden(false, "t_settingsmenu_themedark_disabled")
	get_node("Timer Warning").set_hidden(true)
	
	get_node("/root/Soundboard").play_click()

func _on_Theme_Dark_pressed():
	g_theme = 1
	
	get_node("Theme Dark").play_click_animation()
	get_node("Background").set_modulate(get_node("/root/colours").get_colour("black"))
	get_node("Theme Clear").set_child_hidden(false, "t_settingsmenu_themeclear_disabled")
	get_node("Theme Dark").set_child_hidden(true, "t_settingsmenu_themedark_disabled")
	get_node("Timer Warning").set_hidden(true)
	
	get_node("/root/Soundboard").play_click()

func _on_Toggle_Music_pressed():
	get_node("Toggle Music").play_click_animation()
	g_toggle_music = !g_toggle_music
	
	if g_toggle_music:
		get_node("/root/Soundboard").enable_game_music()
		get_node("/root/Soundboard").lower_music_volume(true)
	else:
		get_node("/root/Soundboard").disable_game_music()
	
	get_node("Toggle Music").set_child_hidden(g_toggle_music, "t_settingsmenu_music_disabled")
	get_node("Toggle Music").play_click_animation()
	get_node("Timer Warning").set_hidden(true)
	
	get_node("/root/Soundboard").play_click()

func _on_Toggle_Timer_pressed():
	get_node("Toggle Timer").play_click_animation()
	
	if(g_free_to_play_version):		# If this is the paid version then user cannot disable timer
		# Display message telling the user they cannot disable timer
		get_node("Timer Warning").set_hidden(false)
	else:
		g_toggle_timer = !g_toggle_timer
		get_node("Toggle Timer").set_child_hidden(g_toggle_timer, "t_settingsmenu_timer_disabled")
	
	get_node("/root/Soundboard").play_click()

func _on_Save_pressed():
	get_node("Save").invert_colours()
	get_node("Save").play_click_animation()
	
	get_node("/root/Soundboard").play_click()
	
	get_node("/root/global").new_settings(g_toggle_music, g_toggle_timer, g_theme)
	g_option_switch = 1
	timer.start()

func _on_Back_pressed():
	get_node("Back").invert_colours()
	get_node("Back").play_click_animation()
	
	get_node("/root/Soundboard").play_click()
	
	g_option_switch = 2
	timer.start()
