extends Node2D

const c_alpha_increment = 0.08
var g_show_background = false

func _ready():
	var l_background_colour
	
	get_node("Grid 4x4 Selected").set_ignore_mouse(true)
	get_node("Grid 4x4 Selected").set_disabled(true)
	get_node("Grid 5x5 Selected").set_ignore_mouse(true)
	get_node("Grid 5x5 Selected").set_disabled(true)
	get_node("Grid 6x6 Selected").set_ignore_mouse(true)
	get_node("Grid 6x6 Selected").set_disabled(true)
	get_node("Grid 7x7 Selected").set_ignore_mouse(true)
	get_node("Grid 7x7 Selected").set_disabled(true)
	get_node("Grid 8x8 Selected").set_ignore_mouse(true)
	get_node("Grid 8x8 Selected").set_disabled(true)
	get_node("Grid 9x9 Selected").set_ignore_mouse(true)
	get_node("Grid 9x9 Selected").set_disabled(true)
	
	if(!get_node("/root/global").g_theme):
		l_background_colour = get_node("/root/colours").get_colour("white")
	if(get_node("/root/global").g_theme):
		l_background_colour = get_node("/root/colours").get_colour("black")
	l_background_colour.a = 0
	get_node("Selection Background").set_modulate(l_background_colour)
	get_node("Selection Background").set_hidden(false)

func set_grid_4x4_selected():
	get_node("Grid 4x4 Selected").set_hidden(false)
	get_node("Grid 5x5 Selected").set_hidden(true)
	get_node("Grid 6x6 Selected").set_hidden(true)
	get_node("Grid 7x7 Selected").set_hidden(true)
	get_node("Grid 8x8 Selected").set_hidden(true)
	get_node("Grid 9x9 Selected").set_hidden(true)
	g_show_background = true
	set_process(true)

func set_grid_5x5_selected():
	get_node("Grid 4x4 Selected").set_hidden(true)
	get_node("Grid 5x5 Selected").set_hidden(false)
	get_node("Grid 6x6 Selected").set_hidden(true)
	get_node("Grid 7x7 Selected").set_hidden(true)
	get_node("Grid 8x8 Selected").set_hidden(true)
	get_node("Grid 9x9 Selected").set_hidden(true)
	g_show_background = true
	set_process(true)

func set_grid_6x6_selected():
	get_node("Grid 4x4 Selected").set_hidden(true)
	get_node("Grid 5x5 Selected").set_hidden(true)
	get_node("Grid 6x6 Selected").set_hidden(false)
	get_node("Grid 7x7 Selected").set_hidden(true)
	get_node("Grid 8x8 Selected").set_hidden(true)
	get_node("Grid 9x9 Selected").set_hidden(true)
	g_show_background = true
	set_process(true)

func set_grid_7x7_selected():
	get_node("Grid 4x4 Selected").set_hidden(true)
	get_node("Grid 5x5 Selected").set_hidden(true)
	get_node("Grid 6x6 Selected").set_hidden(true)
	get_node("Grid 7x7 Selected").set_hidden(false)
	get_node("Grid 8x8 Selected").set_hidden(true)
	get_node("Grid 9x9 Selected").set_hidden(true)
	g_show_background = true
	set_process(true)

func set_grid_8x8_selected():
	get_node("Grid 4x4 Selected").set_hidden(true)
	get_node("Grid 5x5 Selected").set_hidden(true)
	get_node("Grid 6x6 Selected").set_hidden(true)
	get_node("Grid 7x7 Selected").set_hidden(true)
	get_node("Grid 8x8 Selected").set_hidden(false)
	get_node("Grid 9x9 Selected").set_hidden(true)
	g_show_background = true
	set_process(true)

func set_grid_9x9_selected():
	get_node("Grid 4x4 Selected").set_hidden(true)
	get_node("Grid 5x5 Selected").set_hidden(true)
	get_node("Grid 6x6 Selected").set_hidden(true)
	get_node("Grid 7x7 Selected").set_hidden(true)
	get_node("Grid 8x8 Selected").set_hidden(true)
	get_node("Grid 9x9 Selected").set_hidden(false)
	g_show_background = true
	set_process(true)

func deselect_all():
	get_node("Grid 4x4 Selected").set_hidden(true)
	get_node("Grid 5x5 Selected").set_hidden(true)
	get_node("Grid 6x6 Selected").set_hidden(true)
	get_node("Grid 7x7 Selected").set_hidden(true)
	get_node("Grid 8x8 Selected").set_hidden(true)
	get_node("Grid 9x9 Selected").set_hidden(true)
	
	g_show_background = false
	set_process(true)

func _process(delta):
	var l_background_colour = get_node("Selection Background").get_modulate()
	
	if g_show_background:
		if get_node("Selection Background").get_modulate().a < 1:
			l_background_colour.a += c_alpha_increment
			if l_background_colour.a >= 1:
				l_background_colour.a = 1
				set_process(false)
		else:
			set_process(false)
	else:
		if get_node("Selection Background").get_modulate().a > 0:
			l_background_colour.a -= c_alpha_increment
			if l_background_colour.a <= 0:
				l_background_colour.a = 0
				set_process(false)
		else:
			set_process(false)
	
	get_node("Selection Background").set_modulate(l_background_colour)
