extends Node2D

var timer = null
var g_option_switch
var g_button_timeout = 0
onready var g_board_type

var click_play_animation_timer = null

var g_grid_userlevels

func _ready():
	disable_deselection_boundary_controls()
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	g_grid_userlevels = get_node("/root/global").g_grid_userlevels
	g_board_type = get_node("/root/global").g_board_type
	
	get_node("Static Menu Icons/Grids Selection").set_hidden(false)
	
	g_option_switch = 0
	_setup_menu_icons()
	_init_timer()
	_init_play_click_button_animation_timer()
	set_background()
	
	if g_board_type != -1:
		get_node("Play").set_ignore_mouse(false)
		if g_board_type == 0:
			_on_Grid_4x4_pressed()
			if get_node("/root/global").g_board_newly_won_level != -1:
				get_node("Static Menu Icons/Grids").get_node("Grid 4x4").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("Static Menu Icons/Grids Selection").get_node("Grid 4x4 Selected").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("/root/global").g_board_newly_won_level = -1
		elif g_board_type == 1:
			_on_Grid_5x5_pressed()
			if get_node("/root/global").g_board_newly_won_level != -1:
				get_node("Static Menu Icons/Grids").get_node("Grid 5x5").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("Static Menu Icons/Grids Selection").get_node("Grid 5x5 Selected").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("/root/global").g_board_newly_won_level = -1
		elif g_board_type == 2:
			_on_Grid_6x6_pressed()
			if get_node("/root/global").g_board_newly_won_level != -1:
				get_node("Static Menu Icons/Grids").get_node("Grid 6x6").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("Static Menu Icons/Grids Selection").get_node("Grid 6x6 Selected").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("/root/global").g_board_newly_won_level = -1
		elif g_board_type == 3:
			_on_Grid_7x7_pressed()
			if get_node("/root/global").g_board_newly_won_level != -1:
				get_node("Static Menu Icons/Grids").get_node("Grid 7x7").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("Static Menu Icons/Grids Selection").get_node("Grid 7x7 Selected").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("/root/global").g_board_newly_won_level = -1
		elif g_board_type == 4:
			_on_Grid_8x8_pressed()
			if get_node("/root/global").g_board_newly_won_level != -1:
				get_node("Static Menu Icons/Grids").get_node("Grid 8x8").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("Static Menu Icons/Grids Selection").get_node("Grid 8x8 Selected").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("/root/global").g_board_newly_won_level = -1
		elif g_board_type == 5:
			_on_Grid_9x9_pressed()
			if get_node("/root/global").g_board_newly_won_level != -1:
				get_node("Static Menu Icons/Grids").get_node("Grid 9x9").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("Static Menu Icons/Grids Selection").get_node("Grid 9x9 Selected").grid_play_won_level_animation(get_node("/root/global").g_board_newly_won_level)
				get_node("/root/global").g_board_newly_won_level = -1

# Used to calculate the timeout for the button spawn delay timers
func _get_button_timeout_value():
	g_button_timeout += get_node("/root/global").menu_button_timeout_secs
	return g_button_timeout

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

func _init_play_click_button_animation_timer():
	click_play_animation_timer = Timer.new()
	click_play_animation_timer.set_one_shot(false)	# Timer will stop after the first, single trigger activation
	click_play_animation_timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	click_play_animation_timer.set_wait_time(2.5)		# 1 second
	click_play_animation_timer.connect("timeout", self, "on_play_click_button_animation_timeout")
	add_child(click_play_animation_timer)

func on_play_click_button_animation_timeout():
	get_node("Play").play_click_animation()

func on_timeout():
	# Do something...
	if (g_option_switch == 1):
		if g_board_type != -1:
			# Only when a game option is selected the timer should be purged off the tree
			remove_child(timer)
			timer.queue_free()
			get_node("/root/global").set_board(g_board_type)
			get_node("/root/global").start_board_game()
	
	if (g_option_switch == 2):
		remove_child(timer)
		timer.queue_free()
		
		get_node("/root/global").goto_scene(get_node("/root/global").Mainmenu)
	
	g_option_switch = 0

func _get_colour_with_alpha(l_colour):
	var l_colour_aux = l_colour
	l_colour_aux.a = 0.7
	return l_colour_aux

func _setup_menu_icons():
	get_node("Static Menu Icons/Grids").get_node("Grid 4x4").set_grid_user_level(g_grid_userlevels[0].grid_user_level)
	get_node("Static Menu Icons/Grids").get_node("Grid 5x5").set_grid_user_level(g_grid_userlevels[1].grid_user_level)
	get_node("Static Menu Icons/Grids").get_node("Grid 6x6").set_grid_user_level(g_grid_userlevels[2].grid_user_level)
	get_node("Static Menu Icons/Grids").get_node("Grid 7x7").set_grid_user_level(g_grid_userlevels[3].grid_user_level)
	get_node("Static Menu Icons/Grids").get_node("Grid 8x8").set_grid_user_level(g_grid_userlevels[4].grid_user_level)
	get_node("Static Menu Icons/Grids").get_node("Grid 9x9").set_grid_user_level(g_grid_userlevels[5].grid_user_level)
	
	get_node("Static Menu Icons/Grids Selection").get_node("Grid 4x4 Selected").set_grid_user_level(g_grid_userlevels[0].grid_user_level)
	get_node("Static Menu Icons/Grids Selection").get_node("Grid 5x5 Selected").set_grid_user_level(g_grid_userlevels[1].grid_user_level)
	get_node("Static Menu Icons/Grids Selection").get_node("Grid 6x6 Selected").set_grid_user_level(g_grid_userlevels[2].grid_user_level)
	get_node("Static Menu Icons/Grids Selection").get_node("Grid 7x7 Selected").set_grid_user_level(g_grid_userlevels[3].grid_user_level)
	get_node("Static Menu Icons/Grids Selection").get_node("Grid 8x8 Selected").set_grid_user_level(g_grid_userlevels[4].grid_user_level)
	get_node("Static Menu Icons/Grids Selection").get_node("Grid 9x9 Selected").set_grid_user_level(g_grid_userlevels[5].grid_user_level)
	
	# Active icons
	get_node("Play").load_PID_for_scale_0_625()
	get_node("Play").setup(get_node("/root/viewport_data").g_dot_pos[6], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("violet"), _get_button_timeout_value(), true)
	get_node("Play").set_button_overlay_texture(get_node("/root/circular_button").t_preboardmenu_play_description, 0, get_node("/root/colours").get_colour("violet").inverted(), "t_preboardmenu_play_description")
	get_node("Play").set_ignore_mouse(true)
	
	get_node("Back").load_PID_for_scale_0_625()
	get_node("Back").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("orange"), _get_button_timeout_value(), true)
	get_node("Back").set_button_overlay_texture(get_node("/root/circular_button").t_preboardmenu_back_description, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_preboardmenu_play_description")

func set_background():
	if(!get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	get_node("Background").set_hidden(false)

func boards_ignore_mouse(l_state):
	get_node("Static Menu Icons/Grids/Grid 4x4").set_ignore_mouse(l_state)
	get_node("Static Menu Icons/Grids/Grid 5x5").set_ignore_mouse(l_state)
	get_node("Static Menu Icons/Grids/Grid 6x6").set_ignore_mouse(l_state)
	get_node("Static Menu Icons/Grids/Grid 7x7").set_ignore_mouse(l_state)
	get_node("Static Menu Icons/Grids/Grid 8x8").set_ignore_mouse(l_state)
	get_node("Static Menu Icons/Grids/Grid 9x9").set_ignore_mouse(l_state)



# Signals

func _on_Play_pressed():
	get_node("Play").invert_colours()
	get_node("Play").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 1
	timer.start()

func _on_Back_pressed():
	get_node("Back").invert_colours()
	get_node("Back").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 2
	timer.start()

func _on_Grid_4x4_pressed():
	g_board_type = 0
	
	get_node("/root/Soundboard").play_click()
	get_node("Play").set_ignore_mouse(false)
	click_play_animation_timer.start()
	
	boards_ignore_mouse(true)
	get_node("Static Menu Icons/Grids Selection").set_grid_4x4_selected()
	enable_deselection_boundary_controls()

func _on_Grid_5x5_pressed():
	g_board_type = 1
	
	get_node("/root/Soundboard").play_click()
	get_node("Play").set_ignore_mouse(false)
	click_play_animation_timer.start()
	
	boards_ignore_mouse(true)
	get_node("Static Menu Icons/Grids Selection").set_grid_5x5_selected()
	enable_deselection_boundary_controls()

func _on_Grid_6x6_pressed():
	g_board_type = 2
	
	get_node("/root/Soundboard").play_click()
	get_node("Play").set_ignore_mouse(false)
	click_play_animation_timer.start()
	
	boards_ignore_mouse(true)
	get_node("Static Menu Icons/Grids Selection").set_grid_6x6_selected()
	enable_deselection_boundary_controls()

func _on_Grid_7x7_pressed():
	g_board_type = 3
	
	get_node("/root/Soundboard").play_click()
	get_node("Play").set_ignore_mouse(false)
	click_play_animation_timer.start()
	
	boards_ignore_mouse(true)
	get_node("Static Menu Icons/Grids Selection").set_grid_7x7_selected()
	enable_deselection_boundary_controls()

func _on_Grid_8x8_pressed():
	g_board_type = 4
	
	get_node("/root/Soundboard").play_click()
	get_node("Play").set_ignore_mouse(false)
	click_play_animation_timer.start()
	
	boards_ignore_mouse(true)
	get_node("Static Menu Icons/Grids Selection").set_grid_8x8_selected()
	enable_deselection_boundary_controls()

func _on_Grid_9x9_pressed():
	g_board_type = 5
	
	get_node("/root/Soundboard").play_click()
	get_node("Play").set_ignore_mouse(false)
	click_play_animation_timer.start()
	
	boards_ignore_mouse(true)
	get_node("Static Menu Icons/Grids Selection").set_grid_9x9_selected()
	enable_deselection_boundary_controls()

func deselect_board():
	g_board_type = -1
	boards_ignore_mouse(false)
	disable_deselection_boundary_controls()
	get_node("Play").set_ignore_mouse(true)
	click_play_animation_timer.stop()



# Static Menu Icons/Grids selection

func enable_deselection_boundary_controls():
	get_node("Static Menu Icons/Leave Selection").set_ignore_mouse(false)
	get_node("Static Menu Icons/Leave Selection1").set_ignore_mouse(false)
	get_node("Static Menu Icons/Leave Selection2").set_ignore_mouse(false)
	get_node("Static Menu Icons/Leave Selection3").set_ignore_mouse(false)

func disable_deselection_boundary_controls():
	get_node("Static Menu Icons/Leave Selection").set_ignore_mouse(true)
	get_node("Static Menu Icons/Leave Selection1").set_ignore_mouse(true)
	get_node("Static Menu Icons/Leave Selection2").set_ignore_mouse(true)
	get_node("Static Menu Icons/Leave Selection3").set_ignore_mouse(true)

func _on_Leave_Selection_pressed():
	get_node("Static Menu Icons/Grids Selection").deselect_all()
	deselect_board()

func _on_Leave_Selection1_pressed():
	get_node("Static Menu Icons/Grids Selection").deselect_all()
	deselect_board()

func _on_Leave_Selection2_pressed():
	get_node("Static Menu Icons/Grids Selection").deselect_all()
	deselect_board()

func _on_Leave_Selection3_pressed():
	get_node("Static Menu Icons/Grids Selection").deselect_all()
	deselect_board()
