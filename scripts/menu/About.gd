extends Node2D

const c_credits_1 = "This game includes the following soundtracks, which are licensed under the [url=https://creativecommons.org/licenses/by/4.0/]Creative Commons Attribution[/url], listed below:"
const c_newline = "\n"
const c_credits_2 = "Kevin MacLeod - Blip Stream - incompetech.com"

var timer = null
var g_option_switch

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	g_option_switch = 0
	_setup_menu_icons()
	_init_timer()
	set_background()

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

func on_timeout():
	# Do something...
	if (g_option_switch == 1):
		remove_child(timer)
		timer.queue_free()
		
		get_node("/root/global").goto_scene(get_node("/root/global").Mainmenu)

func _setup_menu_icons():
	get_node("Back").load_PID_for_scale_0_625()
	get_node("Back").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("violet"), get_node("/root/global").menu_button_timeout_secs, true)
	get_node("Back").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_back_description, 0, get_node("/root/colours").get_colour("violet").inverted(), "t_settingsmenu_back_description")
	
	if(!get_node("/root/global").g_theme):
		get_node("Static Menu Icons/Description").add_color_override("font_color", get_node("/root/colours").get_colour("black"))
		get_node("Static Menu Icons/Credits Header").add_color_override("font_color", get_node("/root/colours").get_colour("black"))
		get_node("Static Menu Icons/Credits").append_bbcode("[color=black]")
		get_node("Static Menu Icons/Seperator 1").set_modulate(get_node("/root/colours").get_colour("black"))
		get_node("Static Menu Icons/Seperator 2").set_modulate(get_node("/root/colours").get_colour("black"))
		get_node("Static Menu Icons/Seperator3").set_modulate(get_node("/root/colours").get_colour("black"))
	if(get_node("/root/global").g_theme):
		get_node("Static Menu Icons/Description").add_color_override("font_color", get_node("/root/colours").get_colour("white"))
		get_node("Static Menu Icons/Credits Header").add_color_override("font_color", get_node("/root/colours").get_colour("white"))
		get_node("Static Menu Icons/Credits").append_bbcode("[color=white]")
		get_node("Static Menu Icons/Seperator 1").set_modulate(get_node("/root/colours").get_colour("white"))
		get_node("Static Menu Icons/Seperator 2").set_modulate(get_node("/root/colours").get_colour("white"))
		get_node("Static Menu Icons/Seperator3").set_modulate(get_node("/root/colours").get_colour("white"))
	
	get_node("Static Menu Icons/Credits").append_bbcode(c_credits_1)
	get_node("Static Menu Icons/Credits").append_bbcode(c_newline)
	get_node("Static Menu Icons/Credits").append_bbcode(c_newline)
	get_node("Static Menu Icons/Credits").append_bbcode(c_credits_2)

func set_background():
	if(!get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	get_node("Background").set_hidden(false)



# Signals

func _on_Back_pressed():
	get_node("Back").invert_colours()
	get_node("Back").play_click_animation()
	
	get_node("/root/Soundboard").play_click()
	
	g_option_switch = 1
	
	timer.start()

func _on_Credits_meta_clicked( meta ):
	OS.shell_open(meta)
