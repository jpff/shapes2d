extends Node2D

const c_description_1 = "Do you like Shapes? Feel free to rate it at its app store's page."
const c_description_tab = "\n"
const c_description_2_1 = "Has new variants of Shapes2D are released, a list will be added here."
const c_description_2_2 = "Work in progress! :)"
const c_description_2_3 = ""
const c_description_2_5 = ""

var timer = null
var g_option_switch

var g_button_timeout = 0

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	g_option_switch = 0
	_setup_menu_icons()
	_init_timer()
	set_background()

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

# Used to calculate the timeout for the button spawn delay timers
func _get_button_timeout_value():
	g_button_timeout += get_node("/root/global").menu_button_timeout_secs
	return g_button_timeout

func on_timeout():
	# Do something...
	if (g_option_switch == 1):
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").goto_scene(get_node("/root/global").Mainmenu)
	if g_option_switch == 2:
		timer.stop()
		get_node("Rate App").invert_colours()
		OS.shell_open(get_node("/root/global").c_googleplay_app_page)
	if g_option_switch == 3:
		timer.stop()
		get_node("Show Rewarded").invert_colours()
		if get_node("/root/admob_global").g_rewardedLoaded:
			get_node("/root/admob_global").display_rewardedvideo()
		else:
			get_node("/root/global").g_return_point = 3
			get_node("/root/global").goto_scene(get_node("/root/global").StockAdvert)

func _setup_menu_icons():
	get_node("Rate App").load_config_for_scale_0_625()
	get_node("Rate App").setup(get_node("/root/viewport_data").g_dot_pos[1], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("orange"), _get_button_timeout_value(), true)
	get_node("Rate App").set_button_overlay_texture(get_node("/root/circular_button").t_likemenu_star, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_likemenu_star")
	get_node("Rate App").set_button_overlay_texture(get_node("/root/circular_button").t_likemenu_rateapp_desc, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_likemenu_rateapp_desc")
	
	get_node("Back").load_PID_for_scale_0_625()
	get_node("Back").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("green"), _get_button_timeout_value(), true)
	get_node("Back").set_button_overlay_texture(get_node("/root/circular_button").t_settingsmenu_back_description, 0, get_node("/root/colours").get_colour("green").inverted(), "t_settingsmenu_back_description")
	
	if(!get_node("/root/global").g_theme):
		get_node("Static Menu Icons/Description 1").append_bbcode("[color=black]")
		get_node("Static Menu Icons/Description 2").append_bbcode("[color=black]")
		
		get_node("Static Menu Icons/Seperator 1").set_modulate(Color(0,0,0,1))
		get_node("Static Menu Icons/Seperator 2").set_modulate(Color(0,0,0,1))
	if(get_node("/root/global").g_theme):
		get_node("Static Menu Icons/Description 1").append_bbcode("[color=white]")
		get_node("Static Menu Icons/Description 2").append_bbcode("[color=white]")
		
		get_node("Static Menu Icons/Seperator 1").set_modulate(Color(255,255,255,1))
		get_node("Static Menu Icons/Seperator 2").set_modulate(Color(255,255,255,1))
	
	get_node("Static Menu Icons/Description 1").append_bbcode(c_description_1)
	
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_2_1)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_tab)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_tab)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_2_2)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_tab)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_tab)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_2_3)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_tab)
	get_node("Static Menu Icons/Description 2").append_bbcode(c_description_2_5)

func set_background():
	if(!get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	get_node("Background").set_hidden(false)



# Signals

func _on_Back_pressed():
	get_node("Back").invert_colours()
	get_node("Back").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 1
	timer.start()

func _on_Rate_App_pressed():
	get_node("Rate App").invert_colours()
	get_node("Rate App").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 2
	timer.start()

func _on_Show_Rewarded_pressed():
	get_node("Show Rewarded").invert_colours()
	get_node("Show Rewarded").play_click_animation()
	get_node("/root/Soundboard").play_click()
	g_option_switch = 3
	timer.start()
