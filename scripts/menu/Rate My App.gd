extends Node2D

const c_description = "Hey there! Do you like this app? Feel free to rate it at the store!"

const c_mov_amount = Vector2(0,8)
onready var g_notice_display_pos

func _ready():
	set_hidden(true)
	set_process(false)
	
	get_node("Transformed Node/Decline").set_ignore_mouse(true)
	get_node("Transformed Node/Accept").set_ignore_mouse(true)
	get_node("Transformed Node/Later").set_ignore_mouse(true)
	
	if(!get_node("/root/global").g_theme):
		get_node("Transformed Node/Background").set_modulate(get_node("/root/colours").get_colour("cornsilk"))
		get_node("Transformed Node/Description").append_bbcode("[center]")
		get_node("Transformed Node/Description").append_bbcode("[color=black]")
	elif get_node("/root/global").g_theme:
		get_node("Transformed Node/Background").set_modulate(get_node("/root/colours").get_colour("gray"))
		get_node("Transformed Node/Description").append_bbcode("[center]")
		get_node("Transformed Node/Description").append_bbcode("[color=white]")
	
	get_node("Transformed Node/Description").append_bbcode(c_description)
	
	g_notice_display_pos = Vector2(0, get_node("/root/viewport_data").c_default_window_size.y - ( get_node("Transformed Node/Background").get_texture().get_size() * get_node("Transformed Node/Background").get_scale() ).x )
	set_pos(Vector2(0, get_node("/root/viewport_data").c_default_window_size.y))
	
	if get_node("/root/global").g_display_notice_now:
		display_notice()

func display_notice():
	set_hidden(false)
	set_process(true)

func _process(delta):
	if get_pos().y > g_notice_display_pos.y:
		set_pos(get_pos() - c_mov_amount )
	else:
		set_process(false)
		get_node("Transformed Node/Decline").set_ignore_mouse(false)
		get_node("Transformed Node/Accept").set_ignore_mouse(false)
		get_node("Transformed Node/Later").set_ignore_mouse(false)

func _on_Accept_pressed():
	set_hidden(true)
	get_node("Transformed Node/Decline").set_ignore_mouse(true)
	get_node("Transformed Node/Accept").set_ignore_mouse(true)
	get_node("Transformed Node/Later").set_ignore_mouse(true)
	
	# Open hyperlink to Google Play's page here...
	OS.shell_open(get_node("/root/global").c_google_play_app_page)
	
	# Set the global control variavles accordingly
	get_node("/root/global").g_time_before_displaying_rate_notice_in_seconds = 0
	get_node("/root/global").g_user_has_rated_app = true
	get_node("/root/global").g_display_notice_now = false
	
	# Then proceed to save them
	get_node("/root/global").disable_rate_my_app_timer()
	get_node("/root/global").save_user_data()

func _on_Decline_pressed():
	set_hidden(true)
	get_node("Transformed Node/Decline").set_ignore_mouse(true)
	get_node("Transformed Node/Accept").set_ignore_mouse(true)
	get_node("Transformed Node/Later").set_ignore_mouse(true)
	
	# Set the global control variavles accordingly
	get_node("/root/global").g_time_before_displaying_rate_notice_in_seconds = 0
	get_node("/root/global").g_user_has_rated_app = true
	get_node("/root/global").g_display_notice_now = false
	
	# Then proceed to save them
	get_node("/root/global").disable_rate_my_app_timer()
	get_node("/root/global").save_user_data()

func _on_Later_pressed():
	set_hidden(true)
	get_node("Transformed Node/Decline").set_ignore_mouse(true)
	get_node("Transformed Node/Accept").set_ignore_mouse(true)
	get_node("Transformed Node/Later").set_ignore_mouse(true)
	
	get_node("/root/global").g_time_before_displaying_rate_notice_in_seconds = 0
	get_node("/root/global").g_user_has_rated_app = false
	get_node("/root/global").g_display_notice_now = false
	get_node("/root/global").save_user_data()
