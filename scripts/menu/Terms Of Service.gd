extends Node2D

const bbcode = "By clicking ACCEPT, you accept this app's [url=http://shapes2d-game-privacy.hasherkeen.com/]Privacy Policy[/url]."

func _ready():
	get_node("ToS").set_bbcode(bbcode)
	_setup()

func _setup():
	var l_pos_x = 0
	var l_pos_y = ( get_node("/root/viewport_data").c_default_window_size.y / 2 ) - ( ( get_node("Background").get_texture().get_size().x * get_node("Background").get_scale().x ) / 2 )
	
	set_pos(Vector2( l_pos_x, l_pos_y ))
	
	var l_modulate = get_node("/root/colours").get_colour("black")
	l_modulate.a = 0.85
	get_node("Background").set_modulate(l_modulate)

func _on_ACCEPT_pressed():
	get_node("/root/global").g_accepted_tos = true
	get_node("/root/global").save_user_data()
	queue_free()

func _on_DECLINE_pressed():
	get_node("/root/global").g_accepted_tos = false
	get_node("/root/global").save_user_data()
	get_node("/root/global").exit_game()

func _on_ToS_meta_clicked( meta ):
	OS.shell_open( get_node("/root/global").c_privacy_policy_url )		# Test URL
