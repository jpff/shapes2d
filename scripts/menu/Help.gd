extends Node2D

var timer = null
var g_option_switch
var g_button_timeout = 0

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	g_option_switch = 0
	_setup_menu_icons()
	_init_timer()
	set_background()

# Used to calculate the timeout for the button spawn delay timers
func _get_button_timeout_value():
	g_button_timeout += get_node("/root/global").menu_button_timeout_secs
	return g_button_timeout

func _init_timer():
	timer = Timer.new()
	timer.set_one_shot(true)	# Timer will stop after the first, single trigger activation
	timer.set_timer_process_mode(Timer.TIMER_PROCESS_FIXED)
	timer.set_wait_time(get_node("/root/global").timer_timeout_secs)
	timer.connect("timeout", self, "on_timeout")
	add_child(timer)

func on_timeout():
	# Do something...
	if (g_option_switch == 1):
		# Only when a game option is selected the timer should be purged off the tree
		remove_child(timer)
		timer.queue_free()
		get_node("/root/global").start_board_game()
	
	if (g_option_switch == 2):
		remove_child(timer)
		timer.queue_free()
		
		get_node("/root/global").goto_scene(get_node("/root/global").Mainmenu)

func _setup_menu_icons():
	# Setup passive icons
	get_node("Static Menu Icons/Header").set_modulate(get_node("/root/colours").get_colour("blue"))
	get_node("Static Menu Icons/Seperator 1").set_modulate(get_node("/root/colours").get_colour("red"))
	get_node("Static Menu Icons/Seperator 2").set_modulate(get_node("/root/colours").get_colour("pink"))
	get_node("Static Menu Icons/Seperator 3").set_modulate(get_node("/root/colours").get_colour("red"))
	get_node("Static Menu Icons/Seperator 4").set_modulate(get_node("/root/colours").get_colour("pink"))
	get_node("Static Menu Icons/Seperator 5").set_modulate(get_node("/root/colours").get_colour("red"))
	get_node("Static Menu Icons/Shapes Reference Seperator 1").set_modulate(get_node("/root/colours").get_colour("pink"))
	get_node("Static Menu Icons/Shapes Reference Seperator 2").set_modulate(get_node("/root/colours").get_colour("violet"))
	get_node("Static Menu Icons/Shapes Reference Lines").set_modulate(get_node("/root/colours").get_colour("orange"))
	get_node("Static Menu Icons/Shapes Reference Dots").set_modulate(get_node("/root/colours").get_colour("blue"))
	get_node("Static Menu Icons/Find Shapes Description").set_modulate(get_node("/root/colours").get_colour("violet"))
	get_node("Static Menu Icons/More Points Description").set_modulate(get_node("/root/colours").get_colour("violet"))
	get_node("Static Menu Icons/Reset Score Description").set_modulate(get_node("/root/colours").get_colour("orange"))
	get_node("Static Menu Icons/Shapes Can be Stretched Description").set_modulate(get_node("/root/colours").get_colour("orange"))
	get_node("Static Menu Icons/Larger Shapes Description").set_modulate(get_node("/root/colours").get_colour("green"))
	get_node("Static Menu Icons/Find Heart").set_modulate(get_node("/root/colours").get_colour("red"))
	
	# Active icons
	get_node("Back").load_PID_for_scale_0_625()
	get_node("Back").setup(get_node("/root/viewport_data").g_dot_pos[7], get_node("/root/circular_button").g_button_scale, get_node("/root/colours").get_colour("orange"), _get_button_timeout_value(), true)
	get_node("Back").set_button_overlay_texture(get_node("/root/circular_button").t_preboardmenu_back_description, 0, get_node("/root/colours").get_colour("orange").inverted(), "t_preboardmenu_play_description")

func set_background():
	if(!get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	get_node("Background").set_hidden(false)



# Signals

func _on_Play_pressed():
	get_node("Play").invert_colours()
	get_node("Play").play_click_animation()
	
	get_node("/root/Soundboard").play_click()
	
	g_option_switch = 1
	
	timer.start()

func _on_Back_pressed():
	get_node("Back").invert_colours()
	get_node("Back").play_click_animation()
	
	get_node("/root/Soundboard").play_click()
	
	g_option_switch = 2
	
	timer.start()

func _on_Reset_pressed():
	get_node("Static Menu Icons/Reset").set_hidden(true)
	get_node("Static Menu Icons/Reset").set_ignore_mouse(true)
	
	get_node("Static Menu Icons/Confirm Reset").set_hidden(false)
	get_node("Static Menu Icons/Confirm Reset").set_ignore_mouse(false)
	get_node("Static Menu Icons/Cancel Reset").set_hidden(false)
	get_node("Static Menu Icons/Cancel Reset").set_ignore_mouse(false)

func _on_Confirm_Reset_pressed():
	get_node("/root/global").reset_levels()
	
	get_node("Static Menu Icons/Confirm Reset").set_hidden(true)
	get_node("Static Menu Icons/Confirm Reset").set_ignore_mouse(true)
	get_node("Static Menu Icons/Cancel Reset").set_hidden(true)
	get_node("Static Menu Icons/Cancel Reset").set_ignore_mouse(true)
	
	get_node("Static Menu Icons/Reset").set_hidden(false)
	get_node("Static Menu Icons/Reset").set_ignore_mouse(false)

func _on_Cancel_Reset_pressed():
	get_node("Static Menu Icons/Confirm Reset").set_hidden(true)
	get_node("Static Menu Icons/Confirm Reset").set_ignore_mouse(true)
	get_node("Static Menu Icons/Cancel Reset").set_hidden(true)
	get_node("Static Menu Icons/Cancel Reset").set_ignore_mouse(true)
	
	get_node("Static Menu Icons/Reset").set_hidden(false)
	get_node("Static Menu Icons/Reset").set_ignore_mouse(false)
