extends Node2D

func _ready():
	set_scale(get_node("/root/viewport_data").g_viewport_scaler)
	get_node("Background").set_region(true)
	get_node("Background").set_region_rect(get_node("/root/viewport_data").c_default_rect_area)
	
	if(!get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
		get_node("Advert Continue").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
		get_node("Advert Description").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
	if(get_node("/root/global").g_theme):
		get_node("Background").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_darktheme_colour))
		get_node("Advert Continue").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))
		get_node("Advert Description").set_modulate(get_node("/root/colours").get_colour(get_node("/root/global").c_cleartheme_colour))

func _on_Advert_Continue_pressed():
	get_node("/root/global").goto_scene(get_node("/root/global").Pre_Board_Menu)
